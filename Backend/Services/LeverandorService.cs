﻿using AutoMapper;
using Backend.DTOs.Requests;
using Backend.DTOs.Responses;
using Backend.Models;
using Microsoft.EntityFrameworkCore;

namespace Backend.Services
{
    /// <summary>
    /// Interface of the Leverandør Service
    /// </summary>
    public interface ILeverandorService
    {
        public Task<IReadOnlyList<LeverandorResponse>> GetAllLeverandorerAsync();
        public Task<IReadOnlyList<LeverandorResponse>> GetAktiveLeverandorerAsync();
        public Task<LeverandorResponse> AddAsync(RegisterLeverandorRequest request);
        public Task<LeverandorResponse> UpdateAsync(int leverandorId, RegisterLeverandorRequest request);
        public Task DeleteAsync(int leverandorId);
    }

    public class LeverandorService : ILeverandorService
    {
        private readonly ILogger<LeverandorService> _logger;
        private readonly DatabaseContext _database;
        private readonly IMapper _mapper;
        private readonly IValidator _validator;

        public LeverandorService(ILogger<LeverandorService> logger, DatabaseContext database, IMapper mapper, IValidator validator)
        {
            _logger = logger;
            _database = database;
            _mapper = mapper;
            _validator = validator;
        }

        /// <summary>
        /// Get all Leverandører
        /// </summary>
        /// <returns>A list of Leverandører</returns>
        public async Task<IReadOnlyList<LeverandorResponse>> GetAllLeverandorerAsync()
        {
            _logger.LogInformation("Henter alle leverandørerne");
            var leverandorer = await _database.Leverandorer.ToListAsync();
            return _mapper.Map<List<LeverandorResponse>>(leverandorer);
        }

        /// <summary>
        /// Get all active Leverandører
        /// </summary>
        /// <returns>A list of Leverandører</returns>
        public async Task<IReadOnlyList<LeverandorResponse>> GetAktiveLeverandorerAsync()
        {
            _logger.LogInformation("Henter alle aktive leverandørerne");
            var leverandorer = await _database.Leverandorer.Where(x => x.Aktiv == true).ToListAsync();
            return _mapper.Map<List<LeverandorResponse>>(leverandorer);
        }

        /// <summary>
        /// Add a new Leverandør
        /// Validates the Selskap if not null
        /// </summary>
        /// <param name="request">Leverandør request</param>
        /// <returns>The new Leverandør</returns>
        public async Task<LeverandorResponse> AddAsync(RegisterLeverandorRequest request)
        {
            _logger.LogInformation("Legger til ny leverandør");
            if (request.SelskapId.HasValue)
            {
                await _validator.SelskapExistsAsync(request.SelskapId.Value);
            }

            var leverandor = _mapper.Map<Leverandor>(request);
            leverandor = await _database.AddSaveAndGetAsync(_database.Leverandorer, leverandor);
            _logger.LogInformation($"Leverandør lagt til {leverandor.Id}");
            return _mapper.Map<LeverandorResponse>(leverandor);
        }

        /// <summary>
        /// Update a Leverandør
        /// Validates Selskap if not null
        /// </summary>
        /// <param name="leverandorId">Id of Leverandør to be updated</param>
        /// <param name="request">Updated Leverandør request</param>
        /// <returns>The updated Leverandør</returns>
        public async Task<LeverandorResponse> UpdateAsync(int leverandorId, RegisterLeverandorRequest request)
        {
            _logger.LogInformation($"Oppdaterer leverandør {leverandorId}");
            await _validator.LeverandorExistsAsync(leverandorId);
            if (request.SelskapId.HasValue)
            {
                await _validator.SelskapExistsAsync(request.SelskapId.Value);
            }

            var leverandor = _mapper.Map<Leverandor>(request);
            leverandor.Id = leverandorId;

            leverandor = await _database.UpdateSaveAndGetAsync(_database.Leverandorer, leverandor);
            _logger.LogInformation("leverandør oppdatert");
            return _mapper.Map<LeverandorResponse>(leverandor);
        }

        /// <summary>
        /// Delete a Leverandør
        /// Checks if Leverandør exists
        /// </summary>
        /// <param name="leverandorId">Id of Leverandør to be deleted</param>
        /// <returns>Nothing</returns>
        /// <exception cref="ValidationException">Throws Valdiation exception if Leverandør does not exist</exception>
        public async Task DeleteAsync(int leverandorId)
        {
            _logger.LogInformation($"Sletter leverandør {leverandorId}");
            var leverandor = await _database.Leverandorer.FirstOrDefaultAsync(x => x.Id == leverandorId);
            if (leverandor == null)
            {
                throw new ValidationException($"Invalid leverandør {leverandorId}");
            }
            _database.Leverandorer.Remove(leverandor);
            await _database.TrySaveChangesAsync("Kunne ikke slette leverandøren.");
            _logger.LogInformation("Leverandør slettet");
        }
    }
}
