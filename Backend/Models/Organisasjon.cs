﻿namespace Backend.Models
{
    public class Organisasjon
    {
        public int Id { get; set; }
        public string Navn { get; set; }
    }
}
