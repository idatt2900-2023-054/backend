﻿namespace Backend.DTOs.Requests
{
    public class RegisterUtleveringRequest
    {
        [ValidDate]
        public DateOnly Dato { get; set; }
        [PositiveInteger]
        public int KasserInn { get; set; }
        public bool Primaer { get; set; }
        [PositiveInteger]
        public int OrganisasjonId { get; set; } = -1;
        [PositiveInteger]
        public int VektgruppeId { get; set; } = -1;
        [PositiveFloat]
        public float Bruttovekt { get; set; } = -1;
        [PositiveInteger]
        public int KasserUt { get; set; }
        [PositiveFloat]
        public float Kassevekt { get; set; }
        public string? Notat { get; set; }
    }
}
