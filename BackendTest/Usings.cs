global using Backend.Auth;
global using Backend.DTOs;
global using Backend.DTOs.Requests;
global using Backend.DTOs.Responses;
global using Backend.Models;
global using Backend.Services;
global using Xunit;
global using ValidationException = Backend.Services.ValidationException;