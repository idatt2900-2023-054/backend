﻿namespace Backend.Models
{
    public enum Rolle
    {
        Bruker,
        Admin,
        SuperAdmin
    }
}
