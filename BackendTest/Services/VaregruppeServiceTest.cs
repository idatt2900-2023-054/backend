﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Logging;
using Moq;
using Moq.EntityFrameworkCore;

namespace BackendTest.Services
{
    /// <summary>
    /// Unit tests for Varegruppe Service
    /// Test name format: "Method name" _ "What is expected to happen"
    /// Ex: Method_ThrowsException
    /// </summary>
    public class VaregruppeServiceTest
    {
        private readonly ILogger<VaregruppeService> _loggerMock;
        private readonly IMapper _mapper;
        private readonly IValidator _validator;

        public VaregruppeServiceTest()
        {
            _loggerMock = new Mock<ILogger<VaregruppeService>>().Object;

            _mapper = new Mapper(new MapperConfiguration(config =>
                config.AddProfile<AutoMapperProfile>()
            ));

            _validator = Utils.GetValidatorMock().Object;
        }

        [Fact]
        public async Task GetAllVaregrupperAsync_ReturnsVaregruppeResponses()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Varegrupper).ReturnsDbSet(
                new List<Varegruppe> {
                    new Varegruppe{ Id = 1 },
                    new Varegruppe{ Id = 2 },
                }
            );

            var varegruppeService = new VaregruppeService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var varegrupper = await varegruppeService.GetAllVaregrupperAsync();

            //Assert
            Assert.Equal(2, varegrupper.Count);
            Assert.True(varegrupper.Select(x => x.Id == 1).Any());
            Assert.True(varegrupper.Select(x => x.Id == 2).Any());
        }

        [Fact]
        public async Task GetAllVaregrupperAsync_ReturnsEmptyWhenEmpty()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Varegrupper).ReturnsDbSet(
                new List<Varegruppe>()
            );

            var varegruppeService = new VaregruppeService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var varegrupper = await varegruppeService.GetAllVaregrupperAsync();

            //Assert
            Assert.Empty(varegrupper);
        }
        [Fact]
        public async Task GetvaregrupperOnLocationAsync_ReturnsVaregruppeResponses()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();

            databaseMock.Setup(db => db.VaregruppeLokasjoner).ReturnsDbSet(new List<VaregruppeLokasjon>
            {
                new VaregruppeLokasjon { VaregruppeId = 1 , LokasjonId = 1},
                new VaregruppeLokasjon { VaregruppeId = 2, LokasjonId = 1},

            });

            var varegruppeService = new VaregruppeService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var varegrupper = await varegruppeService.GetVaregrupperOnLokasjonAsync(1);

            //Assert
            Assert.Equal(2, varegrupper.Count);
            Assert.True(varegrupper.Select(x => x.Id = 1).Any());
            Assert.True(varegrupper.Select(x => x.Id = 2).Any());
        }

        [Fact]
        public async Task GetVaregrupperOnLocationAsync_ReturnsEmptyWhenNoVaregruppeExists()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.VaregruppeLokasjoner).ReturnsDbSet(
                new List<VaregruppeLokasjon>()
            );

            var varegruppeService = new VaregruppeService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var varegrupper = await varegruppeService.GetVaregrupperOnLokasjonAsync(1);

            //Assert
            Assert.Empty(varegrupper);
        }

        [Fact]
        public async Task GetVaregrupperOnLocationAsync_ReturnsEmptyWhenNoVaregruppeWithSpecifiedLocationExists()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(db => db.VaregruppeLokasjoner).ReturnsDbSet(new List<VaregruppeLokasjon>
            {
                new VaregruppeLokasjon { VaregruppeId = 1 , LokasjonId = 2},
                new VaregruppeLokasjon { VaregruppeId = 2, LokasjonId = 2},

            });

            var varegruppeService = new VaregruppeService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var varegrupper = await varegruppeService.GetVaregrupperOnLokasjonAsync(1);

            //Assert
            Assert.Empty(varegrupper);
        }

        [Fact]
        public async Task GetHovedgruppeRecursiveAsync_ReturnsEmptyWhenNoVaregruppeExists()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(db => db.Varegrupper).ReturnsDbSet(new List<Varegruppe>
            {
            });

            var vektgruppeService = new VaregruppeService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var varegrupper = await vektgruppeService.GetHovedgrupperRecursiveAsync();

            //Assert
            Assert.Empty(varegrupper);
        }

        [Fact]
        public async Task GetHovedgruppeRecursiveAsync_ReturnsVaregruppeResponses()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();

            databaseMock.Setup(db => db.Varegrupper).ReturnsDbSet(new List<Varegruppe>
            {
                new Varegruppe { Id = 1 },
                new Varegruppe { Id = 2 },
                new Varegruppe { Id = 3, HovedgruppeId = 1 },
                new Varegruppe { Id = 4, HovedgruppeId = 1 },
            });

            var varegruppeService = new VaregruppeService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var varegrupper = await varegruppeService.GetHovedgrupperRecursiveAsync();

            //Assert
            Assert.Equal(2, varegrupper.Count);
            Assert.True(varegrupper.Select(x => x.Id = 1).Any());
            Assert.True(varegrupper.Select(x => x.Id = 2).Any());
            Assert.Null(varegrupper[1].Undergrupper);
            Assert.Equal(2, varegrupper[0].Undergrupper.Count);
            Assert.True(varegrupper[0].Undergrupper.Select(x => x.Id = 3).Any());
            Assert.True(varegrupper[0].Undergrupper.Select(x => x.Id = 4).Any());
        }

        [Fact]
        public async Task GetHovedgrupperRecursiveWithLokasjonAsync_ReturnsEmptyWhenNoVaregruppeExists()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(db => db.VaregruppeLokasjoner).ReturnsDbSet(new List<VaregruppeLokasjon>
            {
            });
            databaseMock.Setup(db => db.Varegrupper).ReturnsDbSet(new List<Varegruppe>
            {
            });

            var vektgruppeService = new VaregruppeService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var varegrupper = await vektgruppeService.GetHovedgrupperRecursiveWithLokasjonAsync(1);

            //Assert
            Assert.Empty(varegrupper);
        }

        [Fact]
        public async Task GetHovedgrupperRecursiveWithLokasjonAsync_ReturnsVaregruppeResponses()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();

            databaseMock.Setup(db => db.VaregruppeLokasjoner).ReturnsDbSet(new List<VaregruppeLokasjon>
            {
                new VaregruppeLokasjon { Varegruppe =  new Varegruppe{ Id = 1}, LokasjonId = 1 },
                new VaregruppeLokasjon { Varegruppe = new Varegruppe { Id = 3, HovedgruppeId = 1 }, LokasjonId = 1 },
            });

            databaseMock.Setup(db => db.Varegrupper).ReturnsDbSet(new List<Varegruppe>
            {
                new Varegruppe { Id = 1 },
                new Varegruppe { Id = 2 },
                new Varegruppe { Id = 3, HovedgruppeId = 1 },
                new Varegruppe { Id = 4, HovedgruppeId = 1 },
            });

            var varegruppeService = new VaregruppeService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var varegrupper = await varegruppeService.GetHovedgrupperRecursiveWithLokasjonAsync(1);

            //Assert
            Assert.Equal(2, varegrupper.Count);
            Assert.True(varegrupper.Select(x => x.Id = 1).Any());
            Assert.True(varegrupper.Select(x => x.Id = 2).Any());
            Assert.Null(varegrupper[1].Undergrupper);
            Assert.Equal(2, varegrupper[0].Undergrupper.Count);
            Assert.True(varegrupper[0].Aktiv);
            Assert.False(varegrupper[1].Aktiv);
            Assert.True(varegrupper[0].Undergrupper.Select(x => x.Id = 3).Any());
            Assert.True(varegrupper[0].Undergrupper.Select(x => x.Id = 4).Any());
            Assert.True(varegrupper[0].Undergrupper[0].Aktiv);
            Assert.False(varegrupper[0].Undergrupper[1].Aktiv);
        }

        [Fact]
        public async Task AddAsync_RegistersAndReturnsVaregruppeResponseWithoutHovedgruppe()
        {
            // Arrange
            var registerVaregruppeRequest = new RegisterVaregruppeRequest
            {
                Navn = "vektgruppe",
                DefaultKassevekt = 1.4f
            };
            var varegruppe = _mapper.Map<Varegruppe>(registerVaregruppeRequest);

            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.AddSaveAndGetAsync(It.IsAny<DbSet<Varegruppe>>(), It.IsAny<Varegruppe>()))
                .Callback(() => varegruppe.Id = 1)
                .ReturnsAsync(varegruppe);

            var varegruppeService = new VaregruppeService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var returnVaregruppe = await varegruppeService.AddAsync(registerVaregruppeRequest);

            //Assert
            Assert.NotNull(returnVaregruppe);
            Assert.Equal(varegruppe.Id, returnVaregruppe.Id);
            Assert.Equal(varegruppe.Navn, returnVaregruppe.Navn);
            Assert.Equal(varegruppe.DefaultKassevekt, returnVaregruppe.DefaultKassevekt);
        }

        [Fact]
        public async Task AddAsync_RegistersAndReturnsVaregruppeResponseWithHovedgruppe()
        {
            // Arrange
            var registerVaregruppeRequest = new RegisterVaregruppeRequest
            {
                Navn = "vektgruppe",
                HovedgruppeId = 2,
                DefaultKassevekt = 1.4f
            };
            var varegruppe = _mapper.Map<Varegruppe>(registerVaregruppeRequest);

            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.AddSaveAndGetAsync(It.IsAny<DbSet<Varegruppe>>(), It.IsAny<Varegruppe>()))
                .Callback(() => varegruppe.Id = 1)
                .ReturnsAsync(varegruppe);

            var varegruppeService = new VaregruppeService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var returnVaregruppe = await varegruppeService.AddAsync(registerVaregruppeRequest);

            //Assert
            Assert.NotNull(returnVaregruppe);
            Assert.Equal(varegruppe.Id, returnVaregruppe.Id);
            Assert.Equal(varegruppe.Navn, returnVaregruppe.Navn);
            Assert.Equal(varegruppe.DefaultKassevekt, returnVaregruppe.DefaultKassevekt);
        }

        [Fact]
        public async Task DeleteAsync_DeletesAndReturnsNothing()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            var varegrupper = new List<Varegruppe>
                {
                    new Varegruppe{ Id = 1},
                };
            databaseMock.Setup(x => x.Varegrupper).ReturnsDbSet(varegrupper);
            databaseMock.Setup(x => x.Varegrupper.Remove(It.IsAny<Varegruppe>())).Callback(() =>
            {
                varegrupper.Clear();
            });
            var varegruppeService = new VaregruppeService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            await varegruppeService.DeleteAsync(1);
            Assert.Empty(varegrupper);
        }

        [Fact]
        public async Task UpdateAsync_RegistersAndReturnsVaregruppeResponse()
        {
            // Arrange
            var updatedVaregruppe = new RegisterVaregruppeRequest
            {
                Navn = "Mat",
                DefaultKassevekt = 1.4f
            };
            var varegruppe = _mapper.Map<Varegruppe>(updatedVaregruppe);

            var entityEntryMock = new Mock<EntityEntry<Varegruppe>>(null);
            entityEntryMock.Setup(x => x.Entity).Returns(varegruppe);

            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.UpdateSaveAndGetAsync(It.IsAny<DbSet<Varegruppe>>(), It.IsAny<Varegruppe>()))
                .Callback(() => varegruppe.Id = 1)
                .ReturnsAsync(varegruppe);

            var varegruppeService = new VaregruppeService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var varegrupper = await varegruppeService.UpdateAsync(1, updatedVaregruppe);

            //Assert

            Assert.NotNull(varegrupper);
            Assert.Equal(1, varegrupper.Id);
            Assert.Equal(varegruppe.Navn, varegrupper.Navn);
            Assert.Equal(varegruppe.DefaultKassevekt, varegrupper.DefaultKassevekt);
        }

        [Fact]
        public async Task AddToLokasjonAsync_AddsAndReturnsNothing()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            var varegruppeLokasjon = new VaregruppeLokasjon { VaregruppeId = 1, LokasjonId = 1 };
            var varegrupper = new List<VaregruppeLokasjon> { };
            databaseMock.Setup(x => x.VaregruppeLokasjoner).ReturnsDbSet(varegrupper);
            databaseMock.Setup(x => x.VaregruppeLokasjoner.Add(It.IsAny<VaregruppeLokasjon>())).Callback(() =>
            {
                varegrupper.Add(varegruppeLokasjon);
            });
            var varegruppeService = new VaregruppeService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            await varegruppeService.AddVaregruppeToLokasjonAsync(1, 1);
            Assert.Single(varegrupper);
        }

        [Fact]
        public async Task DeleteFromLokasjonAsync_DeletesAndReturnsNothing()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            var varegrupper = new List<VaregruppeLokasjon>
                {
                    new VaregruppeLokasjon{ VaregruppeId = 1, LokasjonId = 1},
                };
            databaseMock.Setup(x => x.VaregruppeLokasjoner).ReturnsDbSet(varegrupper);
            databaseMock.Setup(x => x.VaregruppeLokasjoner.Remove(It.IsAny<VaregruppeLokasjon>())).Callback(() =>
            {
                varegrupper.Clear();
            });
            var varegruppeService = new VaregruppeService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            await varegruppeService.DeleteVaregruppeFromLokasjonAsync(1, 1);
            Assert.Empty(varegrupper);
        }
    }
}
