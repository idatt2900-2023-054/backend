﻿namespace Backend.DTOs.Requests
{
    public class LoginRequest
    {
        [NonEmptyString]
        public string Credential { get; set; }
    }
}
