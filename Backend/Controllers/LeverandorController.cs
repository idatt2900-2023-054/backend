﻿using Backend.Auth;
using Backend.DTOs.Requests;
using Backend.DTOs.Responses;
using Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers
{
    /// <summary>
    /// Controller class for the endpoints connected to Leverandører
    /// All of the endpoints have the Admin Policy, except get endpoints
    /// </summary>
    [ApiController]
    public class LeverandorController : ControllerBase
    {
        private readonly ILogger<LeverandorController> _logger;
        private readonly ILeverandorService _leverandorService;

        public LeverandorController(ILogger<LeverandorController> logger, ILeverandorService leverandorService)
        {
            _logger = logger;
            _leverandorService = leverandorService;
        }

        /// <summary>
        /// Get all Leverandører
        /// </summary>
        /// <returns>A list of all Leverandører</returns>
        [HttpGet("leverandorer")]
        public async Task<ActionResult<IEnumerable<LeverandorResponse>>> GetAll()
        {
            return Ok(await _leverandorService.GetAllLeverandorerAsync());
        }

        /// <summary>
        /// Get all active Leverandører
        /// </summary>
        /// <returns>A list of all active Leverandører</returns>
        [HttpGet("leverandorer/aktive")]
        public async Task<ActionResult<IEnumerable<LeverandorResponse>>> GetAktiveLeverandorer()
        {
            return Ok(await _leverandorService.GetAktiveLeverandorerAsync());
        }

        /// <summary>
        /// Add a new Leverandør
        /// </summary>
        /// <param name="registerLeverandorRequest">The Leverandør request</param>
        /// <returns>The new Leverandør</returns>
        [HttpPost("leverandorer")]
        [Authorize(Policy.Admin)]
        public async Task<ActionResult<IEnumerable<LeverandorResponse>>> Add
        (
            [FromBody] RegisterLeverandorRequest registerLeverandorRequest
        )
        {
            return Created(nameof(Add), await _leverandorService.AddAsync(registerLeverandorRequest));
        }

        /// <summary>
        /// Update a given Leverandør
        /// </summary>
        /// <param name="leverandorId">Id of the Leverandør to be updated</param>
        /// <param name="updateLeverandorRequest">The updated Leverandør request</param>
        /// <returns>The updated Leverandør</returns>
        [HttpPut("leverandorer/{leverandorId}")]
        [Authorize(Policy.Admin)]
        public async Task<ActionResult<LeverandorResponse>> Update
            (
                [FromRoute] int leverandorId,
                [FromBody] RegisterLeverandorRequest updateLeverandorRequest
            )
        {
            return Ok(await _leverandorService.UpdateAsync(leverandorId, updateLeverandorRequest));
        }

        /// <summary>
        /// Delete a Leverandør
        /// </summary>
        /// <param name="leverandorId">Id of the Leverandør to be deleted</param>
        /// <returns>Status 200 OK if deleted</returns>
        [HttpDelete("leverandorer/{leverandorId}")]
        [Authorize(Policy.Admin)]
        public async Task<ActionResult> Delete
            (
                [FromRoute] int leverandorId
            )
        {
            await _leverandorService.DeleteAsync(leverandorId);
            return Ok();
        }
    }
}