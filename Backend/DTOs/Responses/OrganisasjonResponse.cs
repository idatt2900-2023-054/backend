﻿namespace Backend.DTOs.Responses
{
    public class OrganisasjonResponse
    {
        public int Id { get; set; }
        public string Navn { get; set; }
    }
}
