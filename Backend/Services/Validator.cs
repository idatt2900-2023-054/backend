﻿using Backend.Models;
using Microsoft.EntityFrameworkCore;
using System.Runtime.Serialization;

namespace Backend.Services
{
    /// <summary>
    /// Custom Exception for validation errors
    /// </summary>
    public class ValidationException : Exception
    {
        public ValidationException() { }
        public ValidationException(string? message) : base(message) { }
        public ValidationException(string? message, Exception? innerException) : base(message, innerException) { }
        protected ValidationException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }

    /// <summary>
    /// Interface of the Validator
    /// The Validator is used to validate objects
    /// </summary>
    public interface IValidator
    {
        public Task<string> BrukerExistsAsync(int brukerId);
        public Task BrukerDoesNotExistAsync(string email);
        public Task LokasjonExistsAsync(int lokasjonId);
        public Task OrganisasjonExistsAsync(int organisasjonId);
        public Task OrganisasjonerExistsAsync(List<int> organisasjonIds);
        public Task LeverandorExistsAsync(int leverandorId);
        public Task LeverandorerExistsAsync(List<int> leverandorIds);
        public Task VektgrupperIsValidAsync(List<int> vektgrupperIds, int lokasjonId);
        public Task UtleveringExistsAsync(int utleveringId);
        public Task UtleveringExistsOnLokasjonAsync(int utleveringId, int lokasjonId);
        public Task InnleveringExistsAsync(int innleveringId);
        public Task InnleveringExistsOnLokasjonAsync(int innleveringId, int lokasjonId);
        public Task VaregruppeIsValidAsync(int varegruppeId, int lokasjonId);
        public Task VaregrupperIsValidAsync(List<int> varegrupperIds, int lokasjonId);
        public Task VektgruppeExistsOnLokasjonAsync(int vektgruppeId, int lokasjonId);
        public Task VaregruppeExistsAsync(int vektgruppeId);
        public Task SelskapExistsAsync(int selskapsId);
        public void FloatIsPositive(float number, string parameterName);
    }

    public class Validator : IValidator
    {
        private readonly ILogger<Validator> _logger;
        private readonly DatabaseContext _database;

        public Validator(ILogger<Validator> logger, DatabaseContext database)
        {
            _logger = logger;
            _database = database;
        }

        /// <summary>
        /// Check if a Bruker exists
        /// </summary>
        /// <param name="brukerId">Id to check</param>
        /// <returns>Nothing</returns>
        /// <exception cref="ValidationException">Throws validation exception if Bruker does not exist</exception>
        public async Task<string> BrukerExistsAsync(int brukerId)
        {
            var brukerExists = await _database.Brukere.FirstOrDefaultAsync(x => x.Id == brukerId);
            if (brukerExists == null)
            {
                throw new ValidationException($"Invalid bruker {brukerId}");
            }
            return brukerExists.Email;
        }

        /// <summary>
        /// Check if a Email is in use
        /// </summary>
        /// <param name="email">Email to check</param>
        /// <returns>Nothing</returns>
        /// <exception cref="ValidationException">Throws validation exception if Email is in use</exception>
        public async Task BrukerDoesNotExistAsync(string email)
        {
            var brukerExists = await _database.Brukere.AnyAsync(x => x.Email == email);
            if (brukerExists)
            {
                throw new ValidationException($"Bruker {email} er allerede opprettet");
            }
        }

        /// <summary>
        /// Check if a Lokasjon exists
        /// </summary>
        /// <param name="lokasjonId">Id to check</param>
        /// <returns>Nothing</returns>
        /// <exception cref="ValidationException">Throws validation exception if Lokasjon does not exist</exception>
        public async Task LokasjonExistsAsync(int lokasjonId)
        {
            var lokasjonExists = await _database.Lokasjoner.AnyAsync(x => x.Id == lokasjonId);
            if (!lokasjonExists)
            {
                throw new ValidationException($"Invalid lokasjon {lokasjonId}");
            }
        }

        /// <summary>
        /// Check if a Organisasjon exists
        /// </summary>
        /// <param name="organisasjonId">Id to check</param>
        /// <returns>Nothing</returns>
        /// <exception cref="ValidationException">Throws validation exception if Organisasjon does not exist</exception>
        public async Task OrganisasjonExistsAsync(int organisasjonId)
        {
            var organisasjonExists = await _database.Organisasjoner.AnyAsync(x => x.Id == organisasjonId);
            if (!organisasjonExists)
            {
                throw new ValidationException($"Invalid organisasjon {organisasjonId}");
            }
        }

        /// <summary>
        /// Check if Organisasjoner exists
        /// </summary>
        /// <param name="organisasjonIds">Id's to check</param>
        /// <returns>Nothing</returns>
        /// <exception cref="ValidationException">Throws validation exception if any Organisasjoner does not exist</exception>
        public async Task OrganisasjonerExistsAsync(List<int> organisasjonIds)
        {
            // Get all organisasjon ids that exists
            var existing = await _database.Organisasjoner.Where(x => organisasjonIds.Contains(x.Id)).Select(x => x.Id).ToListAsync();

            // Check that the two lists are equivalant
            var nonExistingOrganisasjoner = organisasjonIds.Where(x => !existing.Contains(x));
            if (nonExistingOrganisasjoner.Any())
            {
                throw new ValidationException($"Invalid organisasjon(er) {string.Join(", ", nonExistingOrganisasjoner)}");
            }
        }

        /// <summary>
        /// Check if a Leverandør exists
        /// </summary>
        /// <param name="leverandorId">Id to check</param>
        /// <returns>Nothing</returns>
        /// <exception cref="ValidationException">Throws validation exception if Leverandør does not exist</exception>
        public async Task LeverandorExistsAsync(int leverandorId)
        {
            var leverandorExists = await _database.Leverandorer.AnyAsync(x => x.Id == leverandorId);
            if (!leverandorExists)
            {
                throw new ValidationException($"Invalid leverandør {leverandorId}");
            }
        }

        /// <summary>
        /// Check if Leverandører exists
        /// </summary>
        /// <param name="brukerId">Id's to check</param>
        /// <returns>Nothing</returns>
        /// <exception cref="ValidationException">Throws validation exception if any Leverandør does not exist</exception>
        public async Task LeverandorerExistsAsync(List<int> leverandorIds)
        {
            var existing = await _database.Leverandorer.Where(x => leverandorIds.Contains(x.Id)).Select(x => x.Id).ToListAsync();

            // Check that the two lists are equivalant
            var nonExistingLeverandorer = leverandorIds.Where(x => !existing.Contains(x));
            if (nonExistingLeverandorer.Any())
            {
                throw new ValidationException($"Invalid leverandør(er) {string.Join(", ", nonExistingLeverandorer)}");
            }
        }

        /// <summary>
        /// Check if a Utlevering exists
        /// </summary>
        /// <param name="utleveringId">Id to check</param>
        /// <returns>Nothing</returns>
        /// <exception cref="ValidationException">Throws validation exception if Utlevering does not exist</exception>
        public async Task UtleveringExistsAsync(int utleveringId)
        {
            var utleveringExists = await _database.Utleveringer.AnyAsync(x => x.Id == utleveringId);
            if (!utleveringExists)
            {
                throw new ValidationException($"Invalid utlevering {utleveringId}");
            }
        }

        /// <summary>
        /// Check if a Utlevering exists at a Lokasjon
        /// </summary>
        /// <param name="utleveringId">Utlevering id to check</param>
        /// <param name="lokasjonId">Lokasjon id to check</param>
        /// <returns>Nothing</returns>
        /// <exception cref="ValidationException">Throws validation exception if Utlevering does not exist at Lokasjon</exception>
        public async Task UtleveringExistsOnLokasjonAsync(int utleveringId, int lokasjonId)
        {
            var utleveringExists = await _database.Utleveringer.AnyAsync(x => x.Id == utleveringId && x.LokasjonId == lokasjonId);
            if (!utleveringExists)
            {
                throw new ValidationException($"Invalid utlevering {utleveringId}");
            }
        }

        /// <summary>
        /// Check if a Innlevering exists
        /// </summary>
        /// <param name="innleveringId">Id to check</param>
        /// <returns>Nothing</returns>
        /// <exception cref="ValidationException">Throws validation exception if Innlevering does not exist</exception>
        public async Task InnleveringExistsAsync(int innleveringId)
        {
            var utleveringExists = await _database.Innleveringer.AnyAsync(x => x.Id == innleveringId);
            if (!utleveringExists)
            {
                throw new ValidationException($"Invalid innlevering {innleveringId}");
            }
        }

        /// <summary>
        /// Check if a Innlevering exists at a Lokasjon
        /// </summary>
        /// <param name="innleveringId">Innlevering id to check</param>
        /// <param name="lokasjonId">Lokasjon id to check</param>
        /// <returns>Nothing</returns>
        /// <exception cref="ValidationException">Throws validation exception if Innlevering does not exist at Lokasjon</exception>
        public async Task InnleveringExistsOnLokasjonAsync(int innleveringId, int lokasjonId)
        {
            var utleveringExists = await _database.Innleveringer.AnyAsync(x => x.Id == innleveringId && x.LokasjonId == lokasjonId);
            if (!utleveringExists)
            {
                throw new ValidationException($"Invalid innlevering {innleveringId}");
            }
        }

        /// <summary>
        /// Check if a Vektgruppe exists at a Lokasjon
        /// </summary>
        /// <param name="vektgruppeId">Vektgruppe id to check</param>
        /// <param name="lokasjonId">Lokasjon id to check</param>
        /// <returns>Nothing</returns>
        /// <exception cref="ValidationException">Throws validation exception if Vektgruppe does not exist at Lokasjon</exception>
        public async Task VektgruppeExistsOnLokasjonAsync(int vektgruppeId, int lokasjonId)
        {
            var vektgruppeExists = await _database.Vektgrupper.AnyAsync(x => x.Id == vektgruppeId && x.LokasjonId == lokasjonId);
            if (!vektgruppeExists)
            {
                throw new ValidationException($"Invalid vektgruppe {vektgruppeId}");
            }
        }

        /// <summary>
        /// Check if a Varegruppe exists
        /// </summary>
        /// <param name="varegruppeId">Id to check</param>
        /// <returns>Nothing</returns>
        /// <exception cref="ValidationException">Throws validation exception if Varegruppe does not exist</exception>
        public async Task VaregruppeExistsAsync(int varegruppeId)
        {
            var varegruppeExists = await _database.Varegrupper.AnyAsync(x => x.Id == varegruppeId);
            if (!varegruppeExists)
            {
                throw new ValidationException($"Invalid varegruppe {varegruppeId}");
            }
        }

        /// <summary>
        /// Check if a Selskap exists
        /// </summary>
        /// <param name="selskapId">Id to check</param>
        /// <returns>Nothing</returns>
        /// <exception cref="ValidationException">Throws validation exception if Selskap does not exist</exception>
        public async Task SelskapExistsAsync(int selskapId)
        {
            var selskapExists = await _database.Selskaper.AnyAsync(x => x.Id == selskapId);
            if (!selskapExists)
            {
                throw new ValidationException($"Invalid selskap {selskapId}");
            }
        }

        /// <summary>
        /// Check if Vektgrupper exists at a Lokasjon
        /// </summary>
        /// <param name="vektgrupperIds">Vektgrupper id's to check</param>
        /// <param name="lokasjonId">Lokasjon id to check</param>
        /// <returns>Nothing</returns>
        /// <exception cref="ValidationException">Throws validation exception if any Vektgruppe does not exist at Lokasjon</exception>
        public async Task VektgrupperIsValidAsync(List<int> vektgrupperIds, int lokasjonId)
        {
            var validVektgrupper = await _database.Vektgrupper
                .Where(x => x.LokasjonId == lokasjonId && vektgrupperIds.Contains(x.Id))
                .Select(x => x.Id)
                .ToListAsync();

            var invalidVektgrupper = vektgrupperIds.Except(validVektgrupper);
            if (invalidVektgrupper.Any())
            {
                throw new ValidationException($"Invalid vektgrupper {string.Join(", ", invalidVektgrupper)}");
            }
        }


        /// <summary>
        /// Check if a Varegruppe exists at a Lokasjon
        /// </summary>
        /// <param name="varegruppeId">Varegruppe id to check</param>
        /// <param name="lokasjonId">Lokasjon id to check</param>
        /// <returns>Nothing</returns>
        /// <exception cref="ValidationException">Throws validation exception if Varegruppe does not exist at Lokasjon</exception>
        public async Task VaregruppeIsValidAsync(int varegruppeId, int lokasjonId)
        {
            var validVaregruppe = await _database.VaregruppeLokasjoner.Where(x => x.LokasjonId == lokasjonId).AnyAsync(x => x.VaregruppeId == varegruppeId);

            if (!validVaregruppe)
            {
                throw new ValidationException($"Invalid varegruppe {varegruppeId}");
            }
        }


        /// <summary>
        /// Check if Varegrupper exists at a Lokasjon
        /// </summary>
        /// <param name="varegrupperIds">Varegrupper id's to check</param>
        /// <param name="lokasjonId">Lokasjon id to check</param>
        /// <returns>Nothing</returns>
        /// <exception cref="ValidationException">Throws validation exception if any Varegruppe does not exist at Lokasjon</exception>
        public async Task VaregrupperIsValidAsync(List<int> varegrupperIds, int lokasjonId)
        {
            var validVaregrupper = await _database.VaregruppeLokasjoner
                .Where(x => x.LokasjonId == lokasjonId && varegrupperIds.Contains(x.VaregruppeId))
                .Select(x => x.VaregruppeId)
                .ToListAsync();

            var invalidVaregrupper = varegrupperIds.Except(validVaregrupper);
            if (invalidVaregrupper.Any())
            {
                throw new ValidationException($"Invalid varegrupper {string.Join(", ", invalidVaregrupper)}");
            }
        }

        /// <summary>
        /// Check if a float is positive
        /// </summary>
        /// <param name="number">Float to check</param>
        /// <param name="parameterName">Name of the parameter the number is connected to</param>
        /// <exception cref="ValidationException">Throws validation exception if float is negative</exception>
        public void FloatIsPositive(float number, string parameterName)
        {
            if (number < 0)
            {
                throw new ValidationException($"{parameterName} kan ikke være negativ");
            }
        }
    }
}
