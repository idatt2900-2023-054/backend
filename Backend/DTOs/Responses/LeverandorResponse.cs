﻿namespace Backend.DTOs.Responses
{
    public class LeverandorResponse
    {
        public int Id { get; set; }
        public string Navn { get; set; }
        public bool Aktiv { get; set; }
        public string LeverandorType { get; set; }
        public string Adresse { get; set; }
        public SelskapResponse? Selskap { get; set; }
    }
}
