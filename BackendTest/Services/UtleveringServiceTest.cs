using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Moq;
using Moq.EntityFrameworkCore;

namespace BackendTest.Services
{
    /// <summary>
    /// Unit tests for Utlevering Service
    /// Test name format: "Method name" _ "What is expected to happen"
    /// Ex: Method_ThrowsException
    /// </summary>
    public class UtleveringServiceTests
    {
        private readonly ILogger<UtleveringService> _loggerMock;
        private readonly IMapper _mapper;
        private readonly IValidator _validator;

        public UtleveringServiceTests()
        {
            _loggerMock = Utils.GetLogger<UtleveringService>();
            _mapper = Utils.GetMapper();
            _validator = Utils.GetValidatorMock().Object;
        }

        [Fact]
        public async Task GetAllUtleveringerAsync_ReturnsUtleveringResponses()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Utleveringer).ReturnsDbSet(
                new List<Utlevering> {
                    new Utlevering
                    {
                        Id = 1,
                    },
                    new Utlevering
                    {
                        Id = 2,
                    }
                }
            );

            var utleveringService = new UtleveringService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var utleveringer = await utleveringService.GetAllUtleveringerAsync();

            //Assert
            Assert.Equal(2, utleveringer.Count);
        }

        [Fact]
        public async Task GetAllUtleveringerAsync_ReturnsEmptyWhenEmpty()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Utleveringer).ReturnsDbSet(
                new List<Utlevering>()
            );

            var utleveringService = new UtleveringService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var utleveringer = await utleveringService.GetAllUtleveringerAsync();

            //Assert
            Assert.Empty(utleveringer);
        }

        [Fact]
        public async Task GetUtleveringOnLokasjonAsync_ReturnsUtleveringResponsesWithSpecifiedLokasjonId()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Utleveringer).ReturnsDbSet(
                new List<Utlevering> {
                    new Utlevering
                    {
                        Id = 1,
                        LokasjonId = 1,
                        Lokasjon = new Lokasjon
                        {
                            Id = 1,
                        },
                        Dato = DateTime.Today
                    },
                    new Utlevering
                    {
                        Id = 2,
                        LokasjonId = 1,
                        Lokasjon = new Lokasjon
                        {
                            Id = 1,
                        },
                        Dato = DateTime.Today
                    },
                    new Utlevering
                    {
                        Id = 3,
                        LokasjonId = 2,
                        Lokasjon = new Lokasjon
                        {
                            Id = 2,
                        },
                        Dato = DateTime.Today
                    }
                }
            );

            var utleveringService = new UtleveringService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var utleveringer = await utleveringService.GetUtleveringOnLokasjonAsync(1, DateOnly.FromDateTime(DateTime.Today), DateOnly.FromDateTime(DateTime.Today));

            //Assert
            Assert.Equal(2, utleveringer.Count);
            Assert.All(utleveringer, utlevering => Assert.Equal(1, utlevering.Lokasjon.Id));
        }

        [Fact]
        public async Task GetUtleveringOnLokasjonAsync_ReturnsEmptyWhenEmpty()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Utleveringer).ReturnsDbSet(
                new List<Utlevering> {
                    new Utlevering
                    {
                        Lokasjon = new Lokasjon
                        {
                            Id = 2,
                        }
                    }
                }
            );

            var utleveringService = new UtleveringService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var utleveringer = await utleveringService.GetUtleveringOnLokasjonAsync(1, DateOnly.FromDateTime(DateTime.Today), DateOnly.FromDateTime(DateTime.Today));

            //Assert
            Assert.Empty(utleveringer);
        }

        [Fact]
        public async Task AddRangeAsync_AddsAndReturnsUtleveringResponse()
        {
            // Arrange
            var newUtleveringRequest = new RegisterUtleveringRequest
            {
                Dato = DateOnly.FromDateTime(DateTime.Today),
                KasserInn = 2,
                Primaer = false,
                OrganisasjonId = 1,
            };
            var utlevering = _mapper.Map<Utlevering>(newUtleveringRequest);

            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.AddRangeSaveAndGetAsync(It.IsAny<DbSet<Utlevering>>(), It.IsAny<List<Utlevering>>()))
                .Callback(() => utlevering.Id = 1)
                .ReturnsAsync(new List<Utlevering> { utlevering });

            var utleveringService = new UtleveringService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var utleveringer = await utleveringService.AddRangeAsync(1, new List<RegisterUtleveringRequest> { newUtleveringRequest });

            //Assert
            Assert.Single(utleveringer);
            Assert.NotNull(utleveringer[0]);
            Assert.Equal(1, utleveringer[0].Id);
        }

        [Fact]
        public async Task UpdateAsync_UpdateAndReturnUtleveringResponse()
        {
            // Arrange
            var updateUtleveringRequest = new RegisterUtleveringRequest
            {
                Dato = DateOnly.FromDateTime(DateTime.Today),
                KasserInn = 2,
                Primaer = false,
                OrganisasjonId = 1,
            };
            var utlevering = _mapper.Map<Utlevering>(updateUtleveringRequest);

            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.UpdateSaveAndGetAsync(It.IsAny<DbSet<Utlevering>>(), It.IsAny<Utlevering>()))
                .Callback(() => utlevering.Id = 1)
                .ReturnsAsync(utlevering);

            var utleveringService = new UtleveringService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var response = await utleveringService.UpdateAsync(1, 1, updateUtleveringRequest);

            //Assert
            Assert.NotNull(response);
            Assert.Equal(1, response.Id);
        }

        [Fact]
        public async Task DeleteAsync_DeletesAndReturnsNothing()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();

            var utleveringer = new List<Utlevering>
            {
                new Utlevering{ Id = 1, LokasjonId = 1},
            };

            databaseMock.Setup(x => x.Utleveringer).ReturnsDbSet(utleveringer);
            databaseMock.Setup(x => x.Utleveringer.Remove(It.IsAny<Utlevering>())).Callback(utleveringer.Clear);

            var utleveringService = new UtleveringService(_loggerMock, databaseMock.Object, _mapper, _validator);

            // Act
            await utleveringService.DeleteAsync(1, 1);

            // Assert
            Assert.Empty(utleveringer);
        }
    }
}