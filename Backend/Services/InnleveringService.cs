﻿using AutoMapper;
using Backend.DTOs.Requests;
using Backend.DTOs.Responses;
using Backend.Models;
using Microsoft.EntityFrameworkCore;

namespace Backend.Services
{
    /// <summary>
    /// Interface of the Innlevering Service
    /// </summary>
    public interface IInnleveringService
    {
        public Task<IReadOnlyList<InnleveringResponse>> AddRangeAsync(int lokasjonId, List<RegisterInnleveringRequest> requests);
        public Task<IReadOnlyList<InnleveringResponse>> GetAllInnleveringerAsync();
        public Task<IReadOnlyList<InnleveringResponse>> GetInnleveringerOnLokasjonAsync(int lokasjonId, DateOnly startDate, DateOnly endDate);
        public Task<InnleveringResponse> UpdateAsync(int innleveringId, int lokasjonId, RegisterInnleveringRequest request);
        public Task DeleteAsync(int utleveringId, int lokasjonId);
    }

    public class InnleveringService : IInnleveringService
    {
        private readonly ILogger<InnleveringService> _logger;
        private readonly DatabaseContext _database;
        private readonly IMapper _mapper;
        private readonly IValidator _validator;

        public InnleveringService(ILogger<InnleveringService> logger, DatabaseContext database, IMapper mapper, IValidator validator)
        {
            _logger = logger;
            _database = database;
            _mapper = mapper;
            _validator = validator;
        }

        /// <summary>
        /// Get all Innleveringer
        /// </summary>
        /// <returns>A list of all Innleveringer</returns>
        public async Task<IReadOnlyList<InnleveringResponse>> GetAllInnleveringerAsync()
        {
            _logger.LogInformation("Henter alle innleveringer");
            var innleveringer = await _database.Innleveringer.ToListAsync();
            return _mapper.Map<IReadOnlyList<InnleveringResponse>>(innleveringer);
        }

        /// <summary>
        /// Get all Innleveringer at a Lokasjon within a given timespan
        /// Validates the Lokasjon
        /// </summary>
        /// <param name="lokasjonId">Lokasjon to find from</param>
        /// <param name="startDate">Start search date</param>
        /// <param name="endDate">End search date</param>
        /// <returns>A list of Innleveringer which matches the criteria</returns>
        public async Task<IReadOnlyList<InnleveringResponse>> GetInnleveringerOnLokasjonAsync(int lokasjonId, DateOnly startDate, DateOnly endDate)
        {
            _logger.LogInformation($"Henter innleveringer fra lokasjon {lokasjonId}");
            await _validator.LokasjonExistsAsync(lokasjonId);

            var start = startDate.ToDateTime(TimeOnly.MinValue);
            var end = endDate.ToDateTime(TimeOnly.MinValue);
            var innleveringer = await _database.Innleveringer
                .Where(x => x.LokasjonId == lokasjonId && x.Dato >= start && x.Dato <= end).OrderByDescending(x => x.Dato)
                .ToListAsync();
            return _mapper.Map<IReadOnlyList<InnleveringResponse>>(innleveringer);
        }
        /// <summary>
        /// Add a list of Innleveringer
        /// Validates Lokasjon, Leverandører, Nettovekt and Varegrupper
        /// </summary>
        /// <param name="lokasjonId">Lokasjon to be registered at</param>
        /// <param name="requests">List of Innlevering request</param>
        /// <returns>A list of new Innleveringer</returns>

        public async Task<IReadOnlyList<InnleveringResponse>> AddRangeAsync(int lokasjonId, List<RegisterInnleveringRequest> requests)
        {
            _logger.LogInformation("Legger til nye innleveringer");
            await _validator.LokasjonExistsAsync(lokasjonId);
            await _validator.LeverandorerExistsAsync(requests.Select(x => x.LeverandorId).ToList());
            await _validator.VaregrupperIsValidAsync(requests.Select(x => x.VaregruppeId).ToList(), lokasjonId);
            requests.ForEach(x =>
            {
                if (x.Notat is string notat && notat.Trim().Length == 0)
                {
                    x.Notat = null;
                }
            });

            var innleveringer = _mapper.Map<List<Innlevering>>(requests);
            innleveringer.Select(x => x.Nettovekt).ToList().ForEach(x => _validator.FloatIsPositive(x, "Nettovekt"));
            innleveringer.ForEach(x => x.LokasjonId = lokasjonId);

            innleveringer = await _database.AddRangeSaveAndGetAsync(_database.Innleveringer, innleveringer);
            _logger.LogInformation("Innleveringer lagt til " + string.Join(",", innleveringer.Select(x => x.Id).ToList()));

            return _mapper.Map<List<InnleveringResponse>>(innleveringer);
        }

        /// <summary>
        /// Update a given Innlevering
        /// Validates Lokasjon, Innlevering, Leverandør, Nettovekt and Varegruppe
        /// </summary>
        /// <param name="innleveringId">Id of Innlevering to be updated</param>
        /// <param name="lokasjonId">Lokasjon of the Innlevering</param>
        /// <param name="request">Updated Innlevering request</param>
        /// <returns>Updated Innlevering</returns>
        public async Task<InnleveringResponse> UpdateAsync(int innleveringId, int lokasjonId, RegisterInnleveringRequest request)
        {
            _logger.LogInformation($"Oppdaterer innlevering {innleveringId}");
            await _validator.InnleveringExistsOnLokasjonAsync(innleveringId, lokasjonId);
            await _validator.LeverandorExistsAsync(request.LeverandorId);
            await _validator.VaregruppeIsValidAsync(request.VaregruppeId, lokasjonId);
            if (request.Notat is string notat && notat.Trim().Length == 0)
            {
                request.Notat = null;
            }

            var innlevering = _mapper.Map<Innlevering>(request);
            _validator.FloatIsPositive(innlevering.Nettovekt, "Nettovekt");
            innlevering.Id = innleveringId;
            innlevering.LokasjonId = lokasjonId;

            innlevering = await _database.UpdateSaveAndGetAsync(_database.Innleveringer, innlevering);
            _logger.LogInformation($"Innlevering oppdatert");

            return _mapper.Map<InnleveringResponse>(innlevering);
        }

        /// <summary>
        /// Delete a given Innlevering
        /// Checks if Innlevering exists and that Lokasjon matches
        /// </summary>
        /// <param name="innleveringId">Id of the Innlevering to be deleted</param>
        /// <param name="lokasjonId">Lokasjon of Innlevering</param>
        /// <returns>Nothing</returns>
        /// <exception cref="ValidationException">Throws Valdiation exception if Innlevering does not exist</exception>
        public async Task DeleteAsync(int innleveringId, int lokasjonId)
        {
            _logger.LogInformation($"Sletter innlevering {innleveringId}");
            var innlevering = await _database.Innleveringer.FirstOrDefaultAsync(x => x.Id == innleveringId);
            if (innlevering == null || innlevering.LokasjonId != lokasjonId)
            {
                throw new ValidationException($"Invalid innlevering {innleveringId}");
            }
            _database.Innleveringer.Remove(innlevering);
            _logger.LogInformation($"Innlevering slettet");
            await _database.TrySaveChangesAsync("Kunne ikke slette innlevering.");
        }
    }
}
