﻿using Google.Apis.Auth;

namespace Backend.Auth
{
    /// <summary>
    /// Google validator interface
    /// </summary>
    public interface IGoogleValidator
    {
        Task<GoogleJsonWebSignature.Payload> ValidateAsync(string credential);
    }

    public class GoogleValidator : IGoogleValidator
    {
        private readonly GoogleJsonWebSignature.ValidationSettings _googleValidationSettings;

        public GoogleValidator(GoogleJsonWebSignature.ValidationSettings googleValidationSettings)
        {
            _googleValidationSettings = googleValidationSettings;
        }

        /// <summary>
        /// Validate credentials through Google
        /// </summary>
        /// <param name="credential">Credential to validate</param>
        /// <returns>Payload</returns>
        public async Task<GoogleJsonWebSignature.Payload> ValidateAsync(string credential)
        {
            return await GoogleJsonWebSignature.ValidateAsync(credential);
        }
    }
}
