﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Moq;

namespace BackendTest
{
    public static class Utils
    {
        public static Mock<DatabaseContext> GetDatabaseContextMock()
        {
            return new Mock<DatabaseContext>(new DbContextOptionsBuilder<DatabaseContext>().Options);
        }

        public static ILogger<T> GetLogger<T>()
        {
            return new Mock<ILogger<T>>().Object;
        }

        public static IMapper GetMapper()
        {
            return new Mapper(new MapperConfiguration(config =>
                config.AddProfile<AutoMapperProfile>()
            ));
        }

        public static Mock<IValidator> GetValidatorMock()
        {
            var validator = new Mock<IValidator>();
            validator.SetReturnsDefault(Task.CompletedTask);
            return validator;
        }
    }
}
