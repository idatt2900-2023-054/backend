﻿namespace Backend.DTOs.Requests
{
    public class RegisterSelskapRequest
    {
        [NonEmptyString]
        public string Navn { get; set; }
    }
}
