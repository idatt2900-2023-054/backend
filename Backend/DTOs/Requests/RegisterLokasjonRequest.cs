﻿namespace Backend.DTOs.Requests
{
    public class RegisterLokasjonRequest
    {
        [NonEmptyString]
        public string Navn { get; set; }
    }
}
