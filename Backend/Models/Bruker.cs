﻿namespace Backend.Models
{
    public class Bruker
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public Lokasjon Lokasjon { get; set; }
        public int LokasjonId { get; set; }
        public Rolle Rolle { get; set; }
    }
}
