using Backend.Auth;
using Backend.DTOs.Requests;
using Backend.DTOs.Responses;
using Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers
{
    /// <summary>
    /// Controller class for the endpoints connected to Selskaper
    /// All of the endpoints have the SuperAdmin Policy, except the get endpoint
    /// </summary>
    [ApiController]
    public class SelskapController : ControllerBase
    {
        private readonly ILogger<SelskapController> _logger;
        private readonly ISelskapService _selskapService;

        public SelskapController(ILogger<SelskapController> logger, ISelskapService selskapService)
        {
            _logger = logger;
            _selskapService = selskapService;
        }
        /// <summary>
        /// Get all Selskaper
        /// </summary>
        /// <returns>A list of Selskaper</returns>
        [HttpGet("selskap")]
        public async Task<ActionResult<IEnumerable<SelskapResponse>>> GetAll()
        {
            return Ok(await _selskapService.GetAllSelskapAsync());
        }

        /// <summary>
        /// Add a new Selskap
        /// </summary>
        /// <param name="registerLeverandorSelskapRequest">Selskap request</param>
        /// <returns>The new Selskap</returns>
        [HttpPost("selskap")]
        [Authorize(Policy.SuperAdmin)]
        public async Task<ActionResult<IEnumerable<SelskapResponse>>> Add
        (
            [FromBody] RegisterSelskapRequest registerLeverandorSelskapRequest
        )
        {
            return Created(nameof(Add), await _selskapService.AddAsync(registerLeverandorSelskapRequest));
        }

        /// <summary>
        /// Update a given Selskap
        /// </summary>
        /// <param name="selskapId">Id of Selskap to be updated</param>
        /// <param name="updateLeverandorSelskapRequest">Updated Selskap request</param>
        /// <returns>The updated Selskap</returns>
        [HttpPut("selskap/{selskapId}")]
        [Authorize(Policy.SuperAdmin)]
        public async Task<ActionResult<SelskapResponse>> Update
            (
                [FromRoute] int selskapId,
                [FromBody] RegisterSelskapRequest updateLeverandorSelskapRequest
            )
        {
            return Ok(await _selskapService.UpdateAsync(selskapId, updateLeverandorSelskapRequest));
        }

        /// <summary>
        /// Delete a given Selskap
        /// </summary>
        /// <param name="selskapId">Id of Selskap to be deleted</param>
        /// <returns>Status 200 OK if deleted</returns>
        [HttpDelete("selskap/{selskapId}")]
        [Authorize(Policy.SuperAdmin)]
        public async Task<ActionResult> Delete
            (
                [FromRoute] int selskapId
            )
        {
            await _selskapService.DeleteAsync(selskapId);
            return Ok();
        }
    }
}
