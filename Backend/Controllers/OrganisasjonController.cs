﻿using Backend.DTOs.Responses;
using Backend.Services;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers
{
    /// <summary>
    /// Controller class for the endpoints connected to Organisasjoner
    /// All of the endpoints are open for users
    ///
    /// Important to note: the adding, updating and deleting of organisajoner is supposed to be done at another product, which will share a database
    /// </summary>
    [ApiController]
    public class OrganisasjonController : ControllerBase
    {
        private readonly ILogger<OrganisasjonController> _logger;
        private readonly IOrganisasjonService _utleveringService;

        public OrganisasjonController(ILogger<OrganisasjonController> logger, IOrganisasjonService utleveringService)
        {
            _logger = logger;
            _utleveringService = utleveringService;
        }

        /// <summary>
        /// Get all Organisasjoner
        /// </summary>
        /// <returns>A list of Organisasjoner</returns>
        [HttpGet("organisasjoner")]
        public async Task<IEnumerable<OrganisasjonResponse>> GetAll()
        {
            return await _utleveringService.GetAllOrganisasjoner();
        }
    }
}