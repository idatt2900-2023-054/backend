﻿using Microsoft.EntityFrameworkCore;

namespace Backend.Models
{
    [Index(nameof(LokasjonId))]
    public class Vektgruppe
    {
        public int Id { get; set; }
        public string Navn { get; set; }
        public float DefaultKassevekt { get; set; }
        public Lokasjon Lokasjon { get; set; }
        public int LokasjonId { get; set; }
    }
}
