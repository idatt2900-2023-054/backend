﻿using Backend.Services;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers
{
    /// <summary>
    /// Class to handle errors that occurs
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ErrorController : Controller
    {
        /// <summary>
        /// Catches exceptions and returns statuscode.
        /// If the exception is a validation exception it will return a bad request with an error message, other exceptions will return an internal server error
        /// </summary>
        /// <returns>Bad request or internal server error</returns>
        [Route("/error")]
        public IActionResult HandleError()
        {
            var error = HttpContext.Features.Get<IExceptionHandlerFeature>()!.Error;

            if (error is ValidationException)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { error.Message });
            }
            else
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
