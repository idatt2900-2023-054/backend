﻿namespace Backend.DTOs.Responses
{
    public class BrukerResponse
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public LokasjonResponse Lokasjon { get; set; }
        public string Rolle { get; set; }
    }
}
