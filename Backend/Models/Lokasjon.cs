﻿namespace Backend.Models
{
    public class Lokasjon
    {
        public int Id { get; set; }
        public string Navn { get; set; }
    }
}
