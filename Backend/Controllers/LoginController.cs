﻿using Backend.DTOs.Requests;
using Backend.DTOs.Responses;
using Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers
{
    [ApiController]
    [AllowAnonymous]
    public class LoginController : ControllerBase
    {
        /// <summary>
        /// Controller to handle login and logout requests
        /// </summary>
        private readonly ILogger<LoginController> _logger;
        private readonly ILoginService _loginService;

        public LoginController(ILogger<LoginController> logger, ILoginService loginService)
        {
            _logger = logger;
            _loginService = loginService;
        }

        /// <summary>
        /// Endpoint to login. 
        /// If the credentials are correct, a JWT token will be generated.
        /// </summary>
        /// <param name="loginRequest">The credentials which are used to try and login</param>
        /// <returns>Information of the user</returns>
        [HttpPost("login")]
        public async Task<ActionResult<BrukerResponse>> Login(LoginRequest loginRequest)
        {
            var loginResponse = await _loginService.LoginAsync(loginRequest);
            var jwtToken = _loginService.GenerateJwtToken(loginResponse);
            Response.Cookies.Append("jwtToken", jwtToken, GetCookieOptions(loginResponse.Expires));
            return Ok(loginResponse);
        }
        /// <summary>
        /// Logout a user
        /// </summary>
        /// <returns>Status 200 OK</returns>
        [HttpPost("logout")]
        public async Task<ActionResult> Logout()
        {
            Response.Cookies.Append("jwtToken", "", GetCookieOptions(DateTime.UtcNow.AddDays(1)));
            return Ok();
        }
        /// <summary>
        /// Create a new cookie
        /// </summary>
        /// <param name="expires">The date when the cookie should expire</param>
        /// <returns>A new cookie</returns>
        private CookieOptions GetCookieOptions(DateTime expires)
        {
            return new CookieOptions
            {
                Secure = true,
                HttpOnly = true,
                SameSite = SameSiteMode.None,
                Domain = Request.Host.Host,
                IsEssential = true,
                Path = "/",
                Expires = expires
            };
        }
    }
}