﻿using Backend.Models;
using Microsoft.AspNetCore.Authorization;

namespace Backend.Auth
{
    public class SuperAdminRequirement : IAuthorizationRequirement { }

    public class SuperAdminAuthorizationHandler : AuthorizationHandler<SuperAdminRequirement>
    {
        private readonly AuthorizationHandlerHelper _authorizationHandlerHelper;

        public SuperAdminAuthorizationHandler(AuthorizationHandlerHelper authorizationHandlerHelper)
        {
            _authorizationHandlerHelper = authorizationHandlerHelper;
        }
        /// <summary>
        /// Check if the credentials are of type SuperAdmin
        /// </summary>
        /// <param name="authorizationHandlerContext">Context</param>
        /// <param name="requirement">Requirement</param>
        /// <returns>Nothing</returns>
        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext authorizationHandlerContext, SuperAdminRequirement requirement)
        {
            var claimedRolle = _authorizationHandlerHelper.GetClaimedRolle(authorizationHandlerContext);

            if (claimedRolle == Rolle.SuperAdmin)
            {
                authorizationHandlerContext.Succeed(requirement);
            }
            else
            {
                authorizationHandlerContext.Fail();
            }
        }
    }
}