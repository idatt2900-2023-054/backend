﻿using AutoMapper;
using Backend.DTOs.Requests;
using Backend.DTOs.Responses;
using Backend.Models;
using Microsoft.EntityFrameworkCore;

namespace Backend.Services
{
    /// <summary>
    /// Interface of the Varegruppe Service
    /// </summary>
    public interface IVaregruppeService
    {
        public Task<IReadOnlyList<VaregruppeResponse>> GetAllVaregrupperAsync();
        public Task<IReadOnlyList<VaregruppeResponse>> GetVaregrupperOnLokasjonAsync(int lokasjonId);
        public Task<IReadOnlyList<HovedgrupperResponse>> GetHovedgrupperRecursiveAsync();
        public Task<IReadOnlyList<HovedgrupperLokasjonResponse>> GetHovedgrupperRecursiveWithLokasjonAsync(int lokasjonId);
        public Task<VaregruppeResponse> AddAsync(RegisterVaregruppeRequest request);
        public Task<VaregruppeResponse> UpdateAsync(int varegruppeId, RegisterVaregruppeRequest request);
        public Task DeleteAsync(int varegruppeId);
        public Task AddVaregruppeToLokasjonAsync(int varegruppeId, int lokasjonId);
        public Task DeleteVaregruppeFromLokasjonAsync(int varegruppeId, int lokasjonId);
    }
    public class VaregruppeService : IVaregruppeService
    {
        private readonly ILogger<VaregruppeService> _logger;
        private readonly DatabaseContext _database;
        private readonly IMapper _mapper;
        private readonly IValidator _validator;

        public VaregruppeService(ILogger<VaregruppeService> logger, DatabaseContext database, IMapper mapper, IValidator validator)
        {
            _logger = logger;
            _database = database;
            _mapper = mapper;
            _validator = validator;
        }
        /// <summary>
        /// Get all Varegrupper
        /// </summary>
        /// <returns>A list of all Varegrupper</returns>
        public async Task<IReadOnlyList<VaregruppeResponse>> GetAllVaregrupperAsync()
        {
            _logger.LogInformation("Henter alle varegrupper");
            var varegrupper = await _database.Varegrupper.ToListAsync();
            return _mapper.Map<List<VaregruppeResponse>>(varegrupper);

        }

        /// <summary>
        /// Get all Varegrupper at a Lokasjon
        /// </summary>
        /// <param name="lokasjonId">Lokasjon to search</param>
        /// <returns>A list of Varegrupper</returns>
        public async Task<IReadOnlyList<VaregruppeResponse>> GetVaregrupperOnLokasjonAsync(int lokasjonId)
        {
            _logger.LogInformation($"Henter varegrupper fra lokasjon {lokasjonId}");
            var varegrupper = await _database.VaregruppeLokasjoner
                .Where(x => x.LokasjonId == lokasjonId)
                .Select(x => x.Varegruppe)
                .ToListAsync();

            return _mapper.Map<List<VaregruppeResponse>>(varegrupper);
        }

        /// <summary>
        /// Get a tree structure of all the Varegrupper
        /// The nodes are stored in the Varegruppe
        /// </summary>
        /// <returns>A list of Hovedgrupper</returns>
        public async Task<IReadOnlyList<HovedgrupperResponse>> GetHovedgrupperRecursiveAsync()
        {
            _logger.LogInformation("Henter alle hovedgrupper");
            var varegrupper = _mapper.Map<Dictionary<int, HovedgrupperResponse>>(await _database.Varegrupper.ToDictionaryAsync(x => x.Id));

            foreach (var varegruppe in varegrupper.Values)
            {
                if (varegruppe.HovedgruppeId != null)
                {
                    if (varegrupper[(int)varegruppe.HovedgruppeId].Undergrupper == null)
                    {
                        varegrupper[(int)varegruppe.HovedgruppeId].Undergrupper = new List<HovedgrupperResponse>();
                    }
                    varegrupper[(int)varegruppe.HovedgruppeId].Undergrupper.Add(varegruppe);
                }
            }

            return varegrupper.Values.Where(x => x.HovedgruppeId == null).ToList();
        }

        /// <summary>
        /// Get a tree structure of all the Varegrupper with their active status
        /// The nodes are stored in the Varegruppe
        /// </summary>
        /// <param name="lokasjonId">Lokasjon to check active status</param>
        /// <returns>A list of Hovedgrupper</returns>
        public async Task<IReadOnlyList<HovedgrupperLokasjonResponse>> GetHovedgrupperRecursiveWithLokasjonAsync(int lokasjonId)
        {
            _logger.LogInformation($"Henter hovedgrupper ved lokasjon {lokasjonId}");
            var varegrupper = _mapper.Map<Dictionary<int, HovedgrupperLokasjonResponse>>(await _database.Varegrupper.ToDictionaryAsync(x => x.Id));

            var varegrupperLokasjon = await _database.VaregruppeLokasjoner
                .Where(x => x.LokasjonId == lokasjonId)
                .Select(x => x.Varegruppe)
                .ToListAsync();


            foreach (var varegruppe in varegrupper.Values)
            {
                if (varegrupperLokasjon.Any(x => x.Id == varegruppe.Id))
                {
                    varegruppe.Aktiv = true;
                }
                else varegruppe.Aktiv = false;
                if (varegruppe.HovedgruppeId != null)
                {
                    if (varegrupper[(int)varegruppe.HovedgruppeId].Undergrupper == null)
                    {
                        varegrupper[(int)varegruppe.HovedgruppeId].Undergrupper = new List<HovedgrupperLokasjonResponse>();
                    }
                    varegrupper[(int)varegruppe.HovedgruppeId].Undergrupper.Add(varegruppe);
                }
            }

            return varegrupper.Values.Where(x => x.HovedgruppeId == null).ToList();
        }

        /// <summary>
        /// Add a new Varegruppe
        /// Validates Hovedgruppe (if selected) and puts default kassevekt to 0 if null
        /// </summary>
        /// <param name="request">Vareguppe info</param>
        /// <returns>The new varegruppe</returns>
        public async Task<VaregruppeResponse> AddAsync(RegisterVaregruppeRequest request)
        {
            _logger.LogInformation("Legger til ny varegruppe");
            if (request.DefaultKassevekt == null)
            {
                request.DefaultKassevekt = 0;
            }
            if (request.HovedgruppeId.HasValue)
            {
                await _validator.VaregruppeExistsAsync(request.HovedgruppeId.Value);
            }
            var varegruppe = _mapper.Map<Varegruppe>(request);

            varegruppe = await _database.AddSaveAndGetAsync(_database.Varegrupper, varegruppe);
            _logger.LogInformation($"Ny varegruppe lagt til {varegruppe.Id}");
            return _mapper.Map<VaregruppeResponse>(varegruppe);
        }

        /// <summary>
        /// Update a varegruppe
        /// Validates Varegruppe, Hovedgruppe (if selected) and puts default kassevekt to 0 if null
        /// </summary>
        /// <param name="varegruppeId">Id of Varegruppe to be updated</param>
        /// <param name="request">Updated Varegruppe info</param>
        /// <returns>The update Varegruppe</returns>
        public async Task<VaregruppeResponse> UpdateAsync(int varegruppeId, RegisterVaregruppeRequest request)
        {
            _logger.LogInformation($"Oppdaterer varegruppe {varegruppeId}");
            if (request.DefaultKassevekt == null)
            {
                request.DefaultKassevekt = 0;
            }
            if (request.HovedgruppeId.HasValue)
            {
                await _validator.VaregruppeExistsAsync(request.HovedgruppeId.Value);
            }
            await _validator.VaregruppeExistsAsync(varegruppeId);

            var varegruppe = _mapper.Map<Varegruppe>(request);
            varegruppe.Id = varegruppeId;

            varegruppe = await _database.UpdateSaveAndGetAsync(_database.Varegrupper, varegruppe);
            _logger.LogInformation("Varegruppe oppdatert");
            return _mapper.Map<VaregruppeResponse>(varegruppe);
        }

        /// <summary>
        /// Delete a given Varegruppe
        /// Checks if Varegruppe exists
        /// </summary>
        /// <param name="varegruppeId">Id of Varegruppe to be deleted</param>
        /// <returns>Nothing</returns>
        /// <exception cref="ValidationException">Throws Valdiation exception if Varegruppe does not exist</exception>
        public async Task DeleteAsync(int varegruppeId)
        {
            _logger.LogInformation($"Sletter varegruppe {varegruppeId}");
            var varegruppe = await _database.Varegrupper.FirstOrDefaultAsync(x => x.Id == varegruppeId);
            if (varegruppe is null)
            {
                throw new ValidationException($"Invalid varegruppe {varegruppeId}");
            }
            _database.Varegrupper.Remove(varegruppe);
            await _database.TrySaveChangesAsync("Kunne ikke slette varegruppen.");
            _logger.LogInformation("Varegruppe slettet");
        }

        /// <summary>
        /// Add a Varegruppe to a Lokasjon
        /// Validates Varegruppe and Lokasjon
        /// Checks if Varegruppe already exists at Lokasjon
        /// </summary>
        /// <param name="varegruppeId">Id of Varegruppe to be added</param>
        /// <param name="lokasjonId">Id of Lokasjon to add Varegruppe</param>
        /// <returns>Nothing</returns>
        /// <exception cref="ValidationException">Throws Valdiation exception if Varegruppe already exists at Lokasjon</exception>
        public async Task AddVaregruppeToLokasjonAsync(int varegruppeId, int lokasjonId)
        {
            _logger.LogInformation($"Legger til varegruppe {varegruppeId} ved lokasjon {lokasjonId}");
            await _validator.VaregruppeExistsAsync(varegruppeId);
            await _validator.LokasjonExistsAsync(lokasjonId);

            var varegruppeLokasjon = await _database.VaregruppeLokasjoner.FirstOrDefaultAsync(x => x.VaregruppeId == varegruppeId && x.LokasjonId == lokasjonId);
            if (varegruppeLokasjon is not null)
            {
                throw new ValidationException($"Varegruppe {varegruppeId} eksisterer allerede på lokasjon {lokasjonId}");
            }

            var newVaregruppeLokason = new VaregruppeLokasjon
            {
                LokasjonId = lokasjonId,
                VaregruppeId = varegruppeId,
            };

            _database.VaregruppeLokasjoner.Add(newVaregruppeLokason);
            await _database.TrySaveChangesAsync("Kunne ikke legge varegruppe til ved lokasjon");
            _logger.LogInformation("Varegruppe lagt til ved lokasjon");
        }

        /// <summary>
        /// Delete Varegruppe from Lokasjon
        /// Checks if Varegruppe exists at Lokasjon
        /// </summary>
        /// <param name="varegruppeId">Id of Varegruppe to be deleted</param>
        /// <param name="lokasjonId">Id of Lokasjon to add Varegruppe</param>
        /// <returns>Nothing</returns>
        /// <exception cref="ValidationException">Throws Valdiation exception if Varegruppe does not exist at Lokasjon</exception>
        public async Task DeleteVaregruppeFromLokasjonAsync(int varegruppeId, int lokasjonId)
        {
            _logger.LogInformation($"Fjerner varegruppe {varegruppeId} fra lokasjon {lokasjonId}");
            var varegruppeLokasjon = await _database.VaregruppeLokasjoner.FirstOrDefaultAsync(x => x.VaregruppeId == varegruppeId && x.LokasjonId == lokasjonId);
            if (varegruppeLokasjon is null)
            {
                throw new ValidationException($"Invalid varegruppe {varegruppeId} og lokasjon {lokasjonId}");
            }
            _database.VaregruppeLokasjoner.Remove(varegruppeLokasjon);
            await _database.TrySaveChangesAsync("Kunne ikke slette varegruppe fra lokasjon.");
            _logger.LogInformation("Varegruppe fjernet fra lokasjon");
        }
    }
}
