﻿namespace Backend.DTOs.Responses
{
    public class LokasjonResponse
    {
        public int Id { get; set; }
        public string Navn { get; set; }
    }
}
