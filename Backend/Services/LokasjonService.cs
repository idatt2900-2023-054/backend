﻿using AutoMapper;
using Backend.DTOs.Requests;
using Backend.DTOs.Responses;
using Backend.Models;
using Microsoft.EntityFrameworkCore;

namespace Backend.Services
{
    /// <summary>
    /// Interface of Lokasjon Service
    /// </summary>
    public interface ILokasjonService
    {
        public Task<IReadOnlyList<LokasjonResponse>> GetAllLokasjonerAsync();
        public Task<LokasjonResponse> AddAsync(RegisterLokasjonRequest request);
        public Task<LokasjonResponse> UpdateAsync(int lokasjonId, RegisterLokasjonRequest request);
        public Task DeleteAsync(int lokasjonId);
    }

    public class LokasjonService : ILokasjonService
    {
        private readonly ILogger<LokasjonService> _logger;
        private readonly DatabaseContext _database;
        private readonly IMapper _mapper;
        private readonly IValidator _validator;

        public LokasjonService(ILogger<LokasjonService> logger, DatabaseContext database, IMapper mapper, IValidator validator)
        {
            _logger = logger;
            _database = database;
            _mapper = mapper;
            _validator = validator;
        }

        /// <summary>
        /// Get all Lokasjoner
        /// </summary>
        /// <returns>A list of Lokasjoner</returns>
        public async Task<IReadOnlyList<LokasjonResponse>> GetAllLokasjonerAsync()
        {
            _logger.LogInformation("Henter alle lokasjonene");
            var lokasjoner = await _database.Lokasjoner.ToListAsync();
            return _mapper.Map<List<LokasjonResponse>>(lokasjoner);
        }

        /// <summary>
        /// Add a new Lokasjon
        /// </summary>
        /// <param name="request">Lokasjon request</param>
        /// <returns>The new Lokasjon</returns>
        public async Task<LokasjonResponse> AddAsync(RegisterLokasjonRequest request)
        {
            _logger.LogInformation("Legger til ny lokasjon");

            var lokasjon = _mapper.Map<Lokasjon>(request);
            lokasjon = await _database.AddSaveAndGetAsync(_database.Lokasjoner, lokasjon);
            _logger.LogInformation($"Lokasjon lagt til {lokasjon.Id}");
            return _mapper.Map<LokasjonResponse>(lokasjon);
        }

        /// <summary>
        /// Update a Lokasjon
        /// </summary>
        /// <param name="lokasjonId">Id of Lokasjon to be updated</param>
        /// <param name="request">Updated Lokasjon request</param>
        /// <returns>The updated Lokasjon</returns>
        public async Task<LokasjonResponse> UpdateAsync(int lokasjonId, RegisterLokasjonRequest request)
        {
            _logger.LogInformation($"Oppdaterer lokasjon {lokasjonId}");
            await _validator.LokasjonExistsAsync(lokasjonId);
            var lokasjon = _mapper.Map<Lokasjon>(request);
            lokasjon.Id = lokasjonId;

            lokasjon = await _database.UpdateSaveAndGetAsync(_database.Lokasjoner, lokasjon);
            _logger.LogInformation($"Lokasjon oppdatert");
            return _mapper.Map<LokasjonResponse>(lokasjon);
        }

        /// <summary>
        /// Delete a Lokasjon
        /// Checks if Lokasjon exists
        /// </summary>
        /// <param name="lokasjonId">Id of Lokasjon to be deleted</param>
        /// <returns>Nothing</returns>
        /// <exception cref="ValidationException">Throws validation exception if Lokasjon does not exist</exception>
        public async Task DeleteAsync(int lokasjonId)
        {
            _logger.LogInformation($"Sletter lokasjon {lokasjonId}");
            var lokasjon = await _database.Lokasjoner.FirstOrDefaultAsync(x => x.Id == lokasjonId);
            if (lokasjon == null)
            {
                throw new ValidationException($"Invalid lokasjon {lokasjonId}");
            }
            _database.Lokasjoner.Remove(lokasjon);
            await _database.TrySaveChangesAsync("Kunne ikke slette lokasjonen.");
            _logger.LogInformation($"Lokasjon slettet");
        }
    }
}
