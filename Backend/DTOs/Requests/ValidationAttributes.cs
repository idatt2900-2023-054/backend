﻿using Backend.Models;
using System.ComponentModel.DataAnnotations;

namespace Backend.DTOs.Requests
{
    /// <summary>
    /// Attribute that checks if a object is a positive Integer
    /// </summary>
    public class PositiveInteger : ValidationAttribute
    {
        public bool IsNullable = false;
        protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
        {
            if (value == null && !IsNullable) { return new ValidationResult($"{validationContext.DisplayName} mangler"); }
            return !(Convert.ToInt32(value) < 0) ?
              ValidationResult.Success :
              new ValidationResult($"{validationContext.DisplayName} må være større enn 0");
        }
    }

    /// <summary>
    /// Attribute that checks if a object is a positive Float
    /// </summary>
    public class PositiveFloat : ValidationAttribute
    {
        public bool IsNullable = false;
        protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
        {
            if (value == null && !IsNullable) { return new ValidationResult($"{validationContext.DisplayName} mangler"); }
            return (value == null || !(Convert.ToSingle(value) < 0)) ?
              ValidationResult.Success :
              new ValidationResult($"{validationContext.DisplayName} må være større enn 0");
        }
    }

    /// <summary>
    /// Attribute that checks if a object is a valid DateOnly, must be after the Unix epoch
    /// </summary>
    public class ValidDate : ValidationAttribute
    {
        protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
        {
            if (value == null || value is not DateOnly) { return new ValidationResult($"{validationContext.DisplayName} mangler"); }
            return new DateOnly(1970, 1, 1).CompareTo(value) < 0 ?
              ValidationResult.Success :
              new ValidationResult($"{validationContext.DisplayName} må være etter 1970");
        }
    }

    /// <summary>
    /// Attribute that checks if a object is a non empty String
    /// </summary>
    public class NonEmptyString : ValidationAttribute
    {
        protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
        {
            if (value == null) { return new ValidationResult($"{validationContext.DisplayName} mangler"); }
            return (value.ToString().Trim().Length != 0) ?
                    ValidationResult.Success :
                    new ValidationResult($"{validationContext.DisplayName} kan ikke være tom");
        }
    }

    /// <summary>
    /// Attribute that checks if a object is a valid Rolle, as defined in the Rolle.cs in Models directory
    /// </summary>
    public class RolleString : ValidationAttribute
    {
        protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
        {
            if (value == null) { return new ValidationResult($"{validationContext.DisplayName} mangler"); }
            return Enum.TryParse<Rolle>(value.ToString(), out _) ?
              ValidationResult.Success :
              new ValidationResult($"{validationContext.DisplayName} må ha korrekt rolle navn");
        }
    }
}
