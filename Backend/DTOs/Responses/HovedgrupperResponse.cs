﻿namespace Backend.DTOs.Responses
{
    public class HovedgrupperResponse
    {
        public int Id { get; set; }
        public string Navn { get; set; }

        public int? HovedgruppeId { get; set; }
        public List<HovedgrupperResponse> Undergrupper { get; set; }
        public float DefaultKassevekt { get; set; }
    }
}
