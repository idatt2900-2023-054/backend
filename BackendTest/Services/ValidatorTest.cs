﻿using Microsoft.Extensions.Logging;
using Moq.EntityFrameworkCore;

namespace BackendTest.Services
{
    public class ValidatorTests
    {
        /// <summary>
        /// Unit tests for Validator
        /// Test name format: "Method name" _ "What is expected to happen"
        /// Ex: Method_ThrowsException
        /// </summary>
        private readonly ILogger<Validator> _logger;

        public ValidatorTests()
        {
            _logger = Utils.GetLogger<Validator>();
        }

        [Fact]
        public async Task BrukerExistsAsync_ThrowsValidationExceptionForInvalidBruker()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            var validator = new Validator(_logger, databaseMock.Object);

            databaseMock.Setup(x => x.Brukere).ReturnsDbSet(new List<Bruker>());

            // Act and Assert
            var ex = await Assert.ThrowsAsync<ValidationException>(async () => await validator.BrukerExistsAsync(1));
            Assert.Equal("Invalid bruker 1", ex.Message);
        }

        [Fact]
        public async Task BrukerExistsAsync_ReturnsEmailForValidBruker()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            var validator = new Validator(_logger, databaseMock.Object);
            var email = "test@matsentralen.no";

            databaseMock.Setup(x => x.Brukere).ReturnsDbSet(new List<Bruker>()
                {
                new Bruker()
                    {
                    Id = 1,
                    Email = email
                }
            });

            // Act and Assert
            var response = await validator.BrukerExistsAsync(1);
            Assert.Equal(response, email);
        }

        [Fact]
        public async Task BrukerDoesNotExistAsync_ThrowsValidationExceptionForExistingBruker()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            var validator = new Validator(_logger, databaseMock.Object);

            databaseMock.Setup(x => x.Brukere).ReturnsDbSet(new List<Bruker>
            {
                new Bruker
                {
                    Email = "bruker@matsentralen.no"
                }
            });

            // Act and Assert
            var ex = await Assert.ThrowsAsync<ValidationException>(async () => await validator.BrukerDoesNotExistAsync("bruker@matsentralen.no"));
            Assert.Equal("Bruker bruker@matsentralen.no er allerede opprettet", ex.Message);
        }

        [Fact]
        public async Task LokasjonExistsAsync_ThrowsValidationExceptionForInvalidLokasjon()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            var validator = new Validator(_logger, databaseMock.Object);

            databaseMock.Setup(x => x.Lokasjoner).ReturnsDbSet(new List<Lokasjon>());

            // Act and Assert
            var ex = await Assert.ThrowsAsync<ValidationException>(async () => await validator.LokasjonExistsAsync(1));
            Assert.Equal("Invalid lokasjon 1", ex.Message);
        }

        [Fact]
        public async Task LokasjonExistsAsync_DoesNotThrowValidationExceptionForValidLokasjon()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();

            databaseMock.Setup(x => x.Lokasjoner).ReturnsDbSet(new List<Lokasjon> {
                new Lokasjon { Id = 1 },
                new Lokasjon { Id = 2 },
                new Lokasjon { Id = 3 }
            });

            var validator = new Validator(_logger, databaseMock.Object);

            // Act
            var ex = await Record.ExceptionAsync(async () => await validator.LokasjonExistsAsync(1));

            // Assert
            Assert.Null(ex);
        }

        [Fact]
        public async Task OrganisasjonerExistsAsync_ThrowsValidationExceptionForInvalidOrganisasjoner()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();

            // Mock the behavior of the DatabaseContext so that it returns only a subset of the input OrganisasjonIds
            databaseMock.Setup(x => x.Organisasjoner).ReturnsDbSet(new List<Organisasjon>
            {
                new Organisasjon { Id = 1 },
                new Organisasjon { Id = 2 },
                new Organisasjon { Id = 3 }
            });

            var validator = new Validator(_logger, databaseMock.Object);

            var invalidOrganisasjoner = new List<int> { 4, 5 };
            var expectedErrorMessage = $"Invalid organisasjon(er) {string.Join(", ", invalidOrganisasjoner)}";

            // Act and Assert
            var exception = await Assert.ThrowsAsync<ValidationException>(async () => await validator.OrganisasjonerExistsAsync(invalidOrganisasjoner));
            Assert.Equal(expectedErrorMessage, exception.Message);
        }

        [Fact]
        public async Task OrganisasjonerExistsAsync_DoesNotThrowValidationExceptionForValidOrganisasjoner()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();

            var organisasjonIds = new List<int> { 1, 2, 3 };

            databaseMock.Setup(x => x.Organisasjoner).ReturnsDbSet(new List<Organisasjon>
            {
                new Organisasjon { Id = 1 },
                new Organisasjon { Id = 2 },
                new Organisasjon { Id = 3 }
            });

            var validator = new Validator(_logger, databaseMock.Object);

            // Act
            var ex = await Record.ExceptionAsync(async () => await validator.OrganisasjonerExistsAsync(organisasjonIds));

            // Assert
            Assert.Null(ex);
        }

        [Fact]
        public async Task LeverandorExistsAsync_ThrowsValidationExceptionForInvalidLeverandor()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();

            // Mock the behavior of the DatabaseContext 
            databaseMock.Setup(x => x.Leverandorer).ReturnsDbSet(new List<Leverandor>
            {
                new Leverandor { Id = 1 },
                new Leverandor { Id = 2 }
            });

            var validator = new Validator(_logger, databaseMock.Object);

            int invalidLeverandor = 3;
            var expectedErrorMessage = $"Invalid leverandør {invalidLeverandor}";

            // Act and Assert
            var exception = await Assert.ThrowsAsync<ValidationException>(async () => await validator.LeverandorExistsAsync(invalidLeverandor));
            Assert.Equal(expectedErrorMessage, exception.Message);
        }

        [Fact]
        public async Task LeverandorExistsAsync_DoesNotThrowValidationExceptionForValidLeverandor()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();

            int leverandorIds = 2;

            databaseMock.Setup(x => x.Leverandorer).ReturnsDbSet(new List<Leverandor>
            {
                new Leverandor { Id = 1 },
                new Leverandor { Id = 2 }
            });

            var validator = new Validator(_logger, databaseMock.Object);

            // Act
            var ex = await Record.ExceptionAsync(async () => await validator.LeverandorExistsAsync(leverandorIds));

            // Assert
            Assert.Null(ex);
        }

        [Fact]
        public async Task LeverandorerExistsAsync_ThrowsValidationExceptionForInvalidLeverandor()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();

            // Mock the behavior of the DatabaseContext so that it returns only a subset of the input OrganisasjonIds
            databaseMock.Setup(x => x.Leverandorer).ReturnsDbSet(new List<Leverandor>
            {
                new Leverandor { Id = 1 },
                new Leverandor { Id = 2 },
                new Leverandor { Id = 3 }
            });

            var validator = new Validator(_logger, databaseMock.Object);

            var invalidLeverandorer = new List<int> { 4, 5 };
            var expectedErrorMessage = $"Invalid leverandør(er) {string.Join(", ", invalidLeverandorer)}";

            // Act and Assert
            var exception = await Assert.ThrowsAsync<ValidationException>(async () => await validator.LeverandorerExistsAsync(invalidLeverandorer));
            Assert.Equal(expectedErrorMessage, exception.Message);
        }

        [Fact]
        public async Task LeverandorerExistsAsync_DoesNotThrowValidationExceptionForValidLeverandor()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();

            var leverandorIds = new List<int> { 1, 2, 3 };

            databaseMock.Setup(x => x.Leverandorer).ReturnsDbSet(new List<Leverandor>
            {
                new Leverandor { Id = 1 },
                new Leverandor { Id = 2 },
                new Leverandor { Id = 3 }
            });

            var validator = new Validator(_logger, databaseMock.Object);

            // Act
            var ex = await Record.ExceptionAsync(async () => await validator.LeverandorerExistsAsync(leverandorIds));

            // Assert
            Assert.Null(ex);
        }

        [Fact]
        public async Task InnleveringExistsAsync_ThrowsValidationExceptionForInvalidInnlevering()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();

            // Mock the behavior of the DatabaseContext so that it returns only a subset of the input OrganisasjonIds
            databaseMock.Setup(x => x.Innleveringer).ReturnsDbSet(new List<Innlevering>
            {
                new Innlevering { Id = 1 },
            });

            var validator = new Validator(_logger, databaseMock.Object);

            var invalidInnlevering = 2;
            var expectedErrorMessage = $"Invalid innlevering 2";

            // Act and Assert
            var exception = await Assert.ThrowsAsync<ValidationException>(async () => await validator.InnleveringExistsAsync(invalidInnlevering));
            Assert.Equal(expectedErrorMessage, exception.Message);
        }

        [Fact]
        public async Task InnleveringExistsAsync_DoesNotThrowValidationExceptionForValidInnlevering()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();

            // Mock the behavior of the DatabaseContext
            databaseMock.Setup(x => x.Innleveringer).ReturnsDbSet(new List<Innlevering>
            {
                new Innlevering { Id = 1 }
            });

            var validator = new Validator(_logger, databaseMock.Object);

            // Act
            var ex = await Record.ExceptionAsync(async () => await validator.InnleveringExistsAsync(1));

            // Assert
            Assert.Null(ex);
        }

        [Fact]
        public async Task UtleveringExistsAsync_ThrowsValidationExceptionForInvalidUtlevering()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();

            // Mock the behavior of the DatabaseContext
            databaseMock.Setup(x => x.Utleveringer).ReturnsDbSet(new List<Utlevering>
            {
                new Utlevering { Id = 1 },
                new Utlevering { Id = 2 },
                new Utlevering { Id = 3 }
            });

            var validator = new Validator(_logger, databaseMock.Object);

            var invalidUtlevering = 4;
            var expectedErrorMessage = $"Invalid utlevering 4";

            // Act and Assert
            var exception = await Assert.ThrowsAsync<ValidationException>(async () => await validator.UtleveringExistsAsync(invalidUtlevering));
            Assert.Equal(expectedErrorMessage, exception.Message);
        }

        [Fact]
        public async Task UtleveringExistsAsync_DoesNotThrowValidationExceptionForValidUtlevering()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();

            // Mock the behavior of the DatabaseContext
            databaseMock.Setup(x => x.Utleveringer).ReturnsDbSet(new List<Utlevering>
            {
                new Utlevering { Id = 1 },
                new Utlevering { Id = 2 },
                new Utlevering { Id = 3 }
            });

            var validator = new Validator(_logger, databaseMock.Object);

            // Act
            var ex = await Record.ExceptionAsync(async () => await validator.UtleveringExistsAsync(1));

            // Assert
            Assert.Null(ex);
        }

        [Fact]
        public async Task VektgruppeExistsOnLokasjonAsync_ThrowsValidationExceptionForInvalidVektgruppe()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();

            // Mock the behavior of the DatabaseContext
            databaseMock.Setup(x => x.Vektgrupper).ReturnsDbSet(new List<Vektgruppe>
            {
                new Vektgruppe { Id = 1, LokasjonId = 1 },
                new Vektgruppe { Id = 2, LokasjonId = 1 },
                new Vektgruppe { Id = 3, LokasjonId = 1 }
            });

            var validator = new Validator(_logger, databaseMock.Object);

            var invalidVektgruppe = 4;
            var expectedErrorMessage = $"Invalid vektgruppe {invalidVektgruppe}";

            // Act and Assert
            var exception = await Assert.ThrowsAsync<ValidationException>(async () => await validator.VektgruppeExistsOnLokasjonAsync(invalidVektgruppe, 1));
            Assert.Equal(expectedErrorMessage, exception.Message);
        }

        [Fact]
        public async Task VektgruppeExistsOnLokasjonAsyncc_ThrowsValidationExceptionForInvalidLokasjon()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();

            // Mock the behavior of the DatabaseContext
            databaseMock.Setup(x => x.Vektgrupper).ReturnsDbSet(new List<Vektgruppe>
            {
                new Vektgruppe { Id = 1, LokasjonId = 1 },
                new Vektgruppe { Id = 2, LokasjonId = 1 },
                new Vektgruppe { Id = 3, LokasjonId = 1 }
            });

            var validator = new Validator(_logger, databaseMock.Object);

            var invalidVektgruppe = 1;
            var expectedErrorMessage = $"Invalid vektgruppe {invalidVektgruppe}";

            // Act and Assert
            var exception = await Assert.ThrowsAsync<ValidationException>(async () => await validator.VektgruppeExistsOnLokasjonAsync(invalidVektgruppe, 2));
            Assert.Equal(expectedErrorMessage, exception.Message);
        }

        [Fact]
        public async Task VektgruppeExistsOnLokasjonAsync_DoesNotThrowValidationExceptionForValidVektgruppe()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();

            // Mock the behavior of the DatabaseContext so that it returns only a subset of the input OrganisasjonIds
            databaseMock.Setup(x => x.Vektgrupper).ReturnsDbSet(new List<Vektgruppe>
            {
                new Vektgruppe { Id = 1, LokasjonId = 2 },
                new Vektgruppe { Id = 2, LokasjonId = 2 },
                new Vektgruppe { Id = 3, LokasjonId = 2 },
            });

            var validator = new Validator(_logger, databaseMock.Object);

            // Act
            var ex = await Record.ExceptionAsync(async () => await validator.VektgruppeExistsOnLokasjonAsync(1, 2));

            // Assert
            Assert.Null(ex);
        }

        [Fact]
        public async Task VaregruppeExistsAsync_ThrowsValidationExceptionForInvalidVaregruppe()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();

            // Mock the behavior of the DatabaseContext
            databaseMock.Setup(x => x.Varegrupper).ReturnsDbSet(new List<Varegruppe>
            {
                new Varegruppe { Id = 1 },
                new Varegruppe { Id = 2 },
                new Varegruppe { Id = 3 }
            });

            var validator = new Validator(_logger, databaseMock.Object);

            var invalidVaregruppe = 4;
            var expectedErrorMessage = $"Invalid varegruppe {invalidVaregruppe}";

            // Act and Assert
            var exception = await Assert.ThrowsAsync<ValidationException>(async () => await validator.VaregruppeExistsAsync(invalidVaregruppe));
            Assert.Equal(expectedErrorMessage, exception.Message);
        }

        [Fact]
        public async Task SelskapExistsAsync_DoesNotThrowValidationExceptionForValidSelskap()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();

            // Mock the behavior of the DatabaseContext so that it returns only a subset of the input OrganisasjonIds
            databaseMock.Setup(x => x.Selskaper).ReturnsDbSet(new List<Selskap>
            {
                new Selskap { Id = 1 },
                new Selskap { Id = 2 },
                new Selskap { Id = 3 }
            });

            var validator = new Validator(_logger, databaseMock.Object);

            // Act
            var ex = await Record.ExceptionAsync(async () => await validator.SelskapExistsAsync(1));

            // Assert
            Assert.Null(ex);
        }

        [Fact]
        public async Task SelskapExistsAsync_ThrowsValidationExceptionForInvalidSelskap()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();

            // Mock the behavior of the DatabaseContext
            databaseMock.Setup(x => x.Selskaper).ReturnsDbSet(new List<Selskap>
            {
                new Selskap { Id = 1 },
                new Selskap { Id = 2 },
                new Selskap { Id = 3 }
            });

            var validator = new Validator(_logger, databaseMock.Object);

            var invalidSelskap = 4;
            var expectedErrorMessage = $"Invalid selskap {invalidSelskap}";

            // Act and Assert
            var exception = await Assert.ThrowsAsync<ValidationException>(async () => await validator.SelskapExistsAsync(invalidSelskap));
            Assert.Equal(expectedErrorMessage, exception.Message);
        }

        [Fact]
        public async Task VaregruppeExistsAsync_DoesNotThrowValidationExceptionForValidVaregruppe()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();

            // Mock the behavior of the DatabaseContext so that it returns only a subset of the input OrganisasjonIds
            databaseMock.Setup(x => x.Varegrupper).ReturnsDbSet(new List<Varegruppe>
            {
                new Varegruppe { Id = 1 },
                new Varegruppe { Id = 2 },
                new Varegruppe { Id = 3 }
            });

            var validator = new Validator(_logger, databaseMock.Object);

            // Act
            var ex = await Record.ExceptionAsync(async () => await validator.VaregruppeExistsAsync(1));

            // Assert
            Assert.Null(ex);
        }

        [Fact]
        public async Task VektgrupperIsValidAsync_ThrowsValidationExceptionForInvalidVektgrupper()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();

            var lokasjonId = 1;
            var vektgrupperIds = new List<int>()
            {
                 4, 5, 6
            };

            databaseMock.Setup(db => db.Vektgrupper).ReturnsDbSet(new List<Vektgruppe>
            {
                new Vektgruppe { Id = 1 , LokasjonId = 1},
                new Vektgruppe { Id = 2 , LokasjonId = 1},
                new Vektgruppe { Id = 3 , LokasjonId = 1},
                new Vektgruppe { Id = 4 , LokasjonId = 1}
            });

            var validator = new Validator(_logger, databaseMock.Object);

            // Act and assert
            await Assert.ThrowsAsync<ValidationException>(async () => await validator.VektgrupperIsValidAsync(vektgrupperIds, lokasjonId));
        }

        [Fact]
        public async Task VektgrupperIsValidAsync_DoesNotThrowValidationExceptionForValidVektgrupper()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();

            var lokasjonId = 1;
            var vektgrupperIds = new List<int> { 1, 2, 3, 4 };

            databaseMock.Setup(db => db.Vektgrupper).ReturnsDbSet(new List<Vektgruppe>
            {
                new Vektgruppe { Id = 1 , LokasjonId = 1},
                new Vektgruppe { Id = 2 , LokasjonId = 1},
                new Vektgruppe { Id = 3 , LokasjonId = 1},
                new Vektgruppe { Id = 4 , LokasjonId = 1}
            });

            var validator = new Validator(_logger, databaseMock.Object);

            // Act
            var ex = await Record.ExceptionAsync(async () => await validator.VektgrupperIsValidAsync(vektgrupperIds, lokasjonId));

            // Assert
            Assert.Null(ex);
        }

        [Fact]
        public async Task VaregruppeIsValidAsync_DoesNotThrowValidationExceptionForValidVaregruppe()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();

            var lokasjonId = 1;
            var varegruppeId = new List<int> { 1, 2 };

            databaseMock.Setup(db => db.VaregruppeLokasjoner).ReturnsDbSet(new List<VaregruppeLokasjon>
            {
                new VaregruppeLokasjon{VaregruppeId = 1, LokasjonId = 1},
                new VaregruppeLokasjon{VaregruppeId = 2, LokasjonId = 1},
            });

            var validator = new Validator(_logger, databaseMock.Object);

            // Act
            var ex = await Record.ExceptionAsync(async () => await validator.VaregrupperIsValidAsync(varegruppeId, lokasjonId));

            // Assert
            Assert.Null(ex);
        }

        [Fact]
        public async Task VaregrupperIsValidAsync_ThrowsValidationExceptionForNonExistantVaregruppe()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();

            var lokasjonId = 1;
            var invalid = 3;
            var varegruppeId = new List<int> { 1, 2, invalid };

            databaseMock.Setup(db => db.VaregruppeLokasjoner).ReturnsDbSet(new List<VaregruppeLokasjon>
            {
                new VaregruppeLokasjon { VaregruppeId = 1 , LokasjonId = 1},
                new VaregruppeLokasjon {VaregruppeId = 2, LokasjonId = 1},

            });

            var validator = new Validator(_logger, databaseMock.Object);
            var expectedErrorMessage = $"Invalid varegrupper {invalid}";

            // Act and Assert
            var exception = await Assert.ThrowsAsync<ValidationException>(async () => await validator.VaregrupperIsValidAsync(varegruppeId, lokasjonId));

            Assert.Equal(expectedErrorMessage, exception.Message);
        }

        [Fact]
        public async Task VaregrupperIsValidAsync_ThrowsValidationExceptionForWrongCombinationOfVaregruppeLokasjon()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();

            var lokasjonId = 2;
            var invalid = 1;
            var varegruppeId = new List<int> { 1, 2 };

            databaseMock.Setup(db => db.VaregruppeLokasjoner).ReturnsDbSet(new List<VaregruppeLokasjon>
            {
                new VaregruppeLokasjon { VaregruppeId = 1 , LokasjonId = 1},
                new VaregruppeLokasjon {VaregruppeId = 2, LokasjonId = 2},

            });

            var validator = new Validator(_logger, databaseMock.Object);
            var expectedErrorMessage = $"Invalid varegrupper {invalid}";

            // Act and Assert
            var exception = await Assert.ThrowsAsync<ValidationException>(async () => await validator.VaregrupperIsValidAsync(varegruppeId, lokasjonId));

            Assert.Equal(expectedErrorMessage, exception.Message);
        }

        [Fact]
        public async Task VaregrupperIsValidAsync_DoesNotThrowValidationExceptionForValidVaregruppe()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();

            var lokasjonId = 1;
            var varegruppeId = 1;

            databaseMock.Setup(db => db.VaregruppeLokasjoner).ReturnsDbSet(new List<VaregruppeLokasjon>
            {
                new VaregruppeLokasjon{VaregruppeId = 1, LokasjonId = 1},
                new VaregruppeLokasjon{VaregruppeId = 2, LokasjonId = 1},
            });

            var validator = new Validator(_logger, databaseMock.Object);

            // Act
            var ex = await Record.ExceptionAsync(async () => await validator.VaregruppeIsValidAsync(varegruppeId, lokasjonId));

            // Assert
            Assert.Null(ex);
        }

        [Fact]
        public async Task VaregruppeIsValidAsync_ThrowsValidationExceptionForNonExistantVaregruppe()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();

            var lokasjonId = 1;
            var varegruppeId = 3;

            databaseMock.Setup(db => db.VaregruppeLokasjoner).ReturnsDbSet(new List<VaregruppeLokasjon>
            {
                new VaregruppeLokasjon { VaregruppeId = 1 , LokasjonId = 1},
                new VaregruppeLokasjon {VaregruppeId = 2, LokasjonId = 1},

            });

            var validator = new Validator(_logger, databaseMock.Object);
            var expectedErrorMessage = $"Invalid varegruppe {varegruppeId}";

            // Act and Assert
            var exception = await Assert.ThrowsAsync<ValidationException>(async () => await validator.VaregruppeIsValidAsync(varegruppeId, lokasjonId));

            Assert.Equal(expectedErrorMessage, exception.Message);
        }

        [Fact]
        public async Task VaregruppeIsValidAsync_ThrowsValidationExceptionForWrongCombinationOfVaregruppeLokasjon()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();

            var lokasjonId = 2;
            var varegruppeId = 1;

            databaseMock.Setup(db => db.VaregruppeLokasjoner).ReturnsDbSet(new List<VaregruppeLokasjon>
            {
                new VaregruppeLokasjon { VaregruppeId = 1 , LokasjonId = 1},
                new VaregruppeLokasjon {VaregruppeId = 2, LokasjonId = 2},

            });

            var validator = new Validator(_logger, databaseMock.Object);
            var expectedErrorMessage = $"Invalid varegruppe {varegruppeId}";

            // Act and Assert
            var exception = await Assert.ThrowsAsync<ValidationException>(async () => await validator.VaregruppeIsValidAsync(varegruppeId, lokasjonId));

            Assert.Equal(expectedErrorMessage, exception.Message);
        }

        [Fact]
        public void FloatIsPositive_ThrowsValidationExceptionWhenNegativeNumber()
        {
            var negativeFloat = -1.2f;
            var parameterName = "test";
            var databaseMock = Utils.GetDatabaseContextMock();
            var validator = new Validator(_logger, databaseMock.Object);

            var expectedErrorMessage = $"{parameterName} kan ikke være negativ";
            var exception = Assert.Throws<ValidationException>(() => validator.FloatIsPositive(negativeFloat, parameterName));
            Assert.Equal(expectedErrorMessage, exception.Message);
        }

        [Fact]
        public void FloatIsPositive_DoesNotThrowsValidationExceptionWhenNotNegativeNumber()
        {
            var positiveFloat = 1.2f;
            var databaseMock = Utils.GetDatabaseContextMock();
            var validator = new Validator(_logger, databaseMock.Object);

            var exception = Record.Exception(() => validator.FloatIsPositive(positiveFloat, "test"));
            Assert.Null(exception);
        }

    }
}
