﻿namespace Backend.DTOs.Responses
{
    public class LoginResponse
    {
        public BrukerResponse Bruker { get; set; }
        public DateTime Expires { get; set; }
    }
}
