﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Logging;
using Moq;
using Moq.EntityFrameworkCore;

namespace BackendTest.Services
{
    /// <summary>
    /// Unit tests for Selskap Service
    /// Test name format: "Method name" _ "What is expected to happen"
    /// Ex: Method_ThrowsException
    /// </summary>
    public class SelskapServiceTest
    {
        private readonly ILogger<SelskapService> _loggerMock;
        private readonly IMapper _mapper;
        private readonly IValidator _validator;

        public SelskapServiceTest()
        {
            _loggerMock = new Mock<ILogger<SelskapService>>().Object;

            _mapper = new Mapper(new MapperConfiguration(config =>
                config.AddProfile<AutoMapperProfile>()
            ));

            _validator = Utils.GetValidatorMock().Object;
        }

        [Fact]
        public async Task GetAllSelskapAsync_ReturnsSelskapResponse()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Selskaper).ReturnsDbSet(
                new List<Selskap> {
                    new Selskap{ Id = 1 },
                    new Selskap{ Id = 2 },
                }
            );

            var selskapService = new SelskapService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var selskaper = await selskapService.GetAllSelskapAsync();

            //Assert
            Assert.Equal(2, selskaper.Count);
            Assert.True(selskaper.Select(x => x.Id == 1).Any());
            Assert.True(selskaper.Select(x => x.Id == 2).Any());
        }

        [Fact]
        public async Task GetAllSelskapAsync_ReturnsEmptyWhenEmpty()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Selskaper).ReturnsDbSet(
                new List<Selskap>()
            );

            var selskapService = new SelskapService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var selskaper = await selskapService.GetAllSelskapAsync();

            //Assert
            Assert.Empty(selskaper);
        }

        [Fact]
        public async Task AddAsync_RegistersAndReturnsSelskapResponse()
        {
            // Arrange
            var registerSelskapRequest = new RegisterSelskapRequest
            {
                Navn = "Selskap",
            };
            var selskap = _mapper.Map<Selskap>(registerSelskapRequest);

            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.AddSaveAndGetAsync(It.IsAny<DbSet<Selskap>>(), It.IsAny<Selskap>()))
                .Callback(() => selskap.Id = 1)
                .ReturnsAsync(selskap);

            var selskapService = new SelskapService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var selskaper = await selskapService.AddAsync(registerSelskapRequest);

            //Assert
            Assert.NotNull(selskaper);
            Assert.Equal(selskap.Id, selskaper.Id);
            Assert.Equal(selskap.Navn, selskaper.Navn);
        }

        [Fact]
        public async Task DeleteAsync_DeletesAndReturnsNothing()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            var selskaper = new List<Selskap>
            {
                new Selskap{ Id = 1},
            };
            databaseMock.Setup(x => x.Selskaper).ReturnsDbSet(selskaper);
            databaseMock.Setup(x => x.Selskaper.Remove(It.IsAny<Selskap>())).Callback(selskaper.Clear);
            var selskapService = new SelskapService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            await selskapService.DeleteAsync(1);

            // Assert
            Assert.Empty(selskaper);
        }

        [Fact]
        public async Task UpdateAsync_RegistersAndReturnsSelskapResponse()
        {
            // Arrange
            var updatedSelskap = new RegisterSelskapRequest
            {
                Navn = "Nytt selskap",
            };

            var selskap = _mapper.Map<Selskap>(updatedSelskap);

            var entityEntryMock = new Mock<EntityEntry<Selskap>>(null);
            entityEntryMock.Setup(x => x.Entity).Returns(selskap);

            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Selskaper).ReturnsDbSet(
                new List<Selskap>
            {
                new Selskap{ Id = 1},
            });
            databaseMock.Setup(x => x.UpdateSaveAndGetAsync(It.IsAny<DbSet<Selskap>>(), It.IsAny<Selskap>()))
                .Callback(() => { selskap.Id = 1; })
                .ReturnsAsync(selskap);

            var selskapService = new SelskapService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var selskaper = await selskapService.UpdateAsync(1, updatedSelskap);

            //Assert

            Assert.NotNull(selskaper);
            Assert.Equal(selskap.Id, selskaper.Id);
            Assert.Equal(selskap.Navn, selskaper.Navn);
        }
    }
}
