﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Logging;
using Moq;
using Moq.EntityFrameworkCore;

namespace BackendTest.Services
{
    /// <summary>
    /// Unit tests for Vektgruppe Service
    /// Test name format: "Method name" _ "What is expected to happen"
    /// Ex: Method_ThrowsException
    /// </summary>
    public class VektgruppeServiceTest
    {
        private readonly ILogger<VektgruppeService> _loggerMock;
        private readonly IMapper _mapper;
        private readonly IValidator _validator;

        public VektgruppeServiceTest()
        {
            _loggerMock = Utils.GetLogger<VektgruppeService>();
            _mapper = Utils.GetMapper();
            _validator = Utils.GetValidatorMock().Object;
        }

        [Fact]
        public async Task GetAllVektgrupperAsync_ReturnsVektgruppeResponses()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Vektgrupper).ReturnsDbSet(
                new List<Vektgruppe> {
                    new Vektgruppe{ Id = 1 },
                    new Vektgruppe{ Id = 2 },
                }
            );

            var vektgruppeService = new VektgruppeService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var vektgrupper = await vektgruppeService.GetAllVektgrupperAsync();

            //Assert
            Assert.Equal(2, vektgrupper.Count);
            Assert.True(vektgrupper.Select(x => x.Id == 1).Any());
            Assert.True(vektgrupper.Select(x => x.Id == 2).Any());
        }

        [Fact]
        public async Task GetAllVektgrupperAsync_ReturnsEmptyWhenEmpty()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Vektgrupper).ReturnsDbSet(
                new List<Vektgruppe>()
            );

            var vektgruppeService = new VektgruppeService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var vektgrupper = await vektgruppeService.GetAllVektgrupperAsync();

            //Assert
            Assert.Empty(vektgrupper);
        }
        [Fact]
        public async Task GetVektgrupperOnLokasjonAsync_ReturnsVektgruppeResponses()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Vektgrupper).ReturnsDbSet(
                new List<Vektgruppe> {
                    new Vektgruppe{ Id = 1, LokasjonId = 1, Lokasjon = new Lokasjon{ Id = 1 }},
                    new Vektgruppe{ Id = 2, LokasjonId = 1, Lokasjon = new Lokasjon{ Id = 1 }},
                    new Vektgruppe{ Id = 3, LokasjonId = 2, Lokasjon = new Lokasjon{ Id = 2 } },
                }
            );

            var vektgruppeService = new VektgruppeService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var vektgrupper = await vektgruppeService.GetVektgruppeOnLokasjonAsync(1);

            //Assert
            Assert.Equal(2, vektgrupper.Count);
            Assert.True(vektgrupper.Select(x => x.Id = 1).Any());
            Assert.True(vektgrupper.Select(x => x.Id = 2).Any());
            Assert.All(vektgrupper, vektgruppe => Assert.Equal(1, vektgruppe.Lokasjon.Id));
        }

        [Fact]
        public async Task GetVektgrupperOnLokasjonAsync_ReturnsEmptyWhenNoVektgruppeExists()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Vektgrupper).ReturnsDbSet(
                new List<Vektgruppe>()
            );

            var vektgruppeService = new VektgruppeService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var vektgrupper = await vektgruppeService.GetVektgruppeOnLokasjonAsync(1);

            //Assert
            Assert.Empty(vektgrupper);
        }

        [Fact]
        public async Task GetVektgrupperOnLokasjonAsync_ReturnsEmptyWhenNoVektgruppeWithSpecifiedLokasjonExists()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Vektgrupper).ReturnsDbSet(
                new List<Vektgruppe>
                {
                    new Vektgruppe{ Id = 3, LokasjonId = 2, Lokasjon = new Lokasjon{ Id = 2 } },
                }
            );

            var vektgruppeService = new VektgruppeService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var vektgrupper = await vektgruppeService.GetVektgruppeOnLokasjonAsync(1);

            //Assert
            Assert.Empty(vektgrupper);
        }

        [Fact]
        public async Task AddAsync_RegistersAndReturnsVektgruppeResponse()
        {
            // Arrange
            var newVektgruppeRequest = new RegisterVektgruppeRequest
            {
                Navn = "vektgruppe",
                DefaultKassevekt = 1.4f
            };
            var vektgruppe = _mapper.Map<Vektgruppe>(newVektgruppeRequest);

            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.AddSaveAndGetAsync(It.IsAny<DbSet<Vektgruppe>>(), It.IsAny<Vektgruppe>()))
                .Callback(() => vektgruppe.Id = 1)
                .ReturnsAsync(vektgruppe);

            var vektgruppeService = new VektgruppeService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var vektgrupper = await vektgruppeService.AddAsync(1, newVektgruppeRequest);

            //Assert
            Assert.NotNull(vektgrupper);
            Assert.Equal(vektgruppe.Id, vektgrupper.Id);
            Assert.Equal(vektgruppe.Navn, vektgrupper.Navn);
            Assert.Equal(vektgruppe.DefaultKassevekt, vektgrupper.DefaultKassevekt);
        }

        [Fact]
        public async Task DeleteAsync_DeletesAndReturnsNothing()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            var vektgrupper = new List<Vektgruppe>
            {
                new Vektgruppe{ Id = 1, LokasjonId = 1},
            };
            databaseMock.Setup(x => x.Vektgrupper).ReturnsDbSet(vektgrupper);
            databaseMock.Setup(x => x.Vektgrupper.Remove(It.IsAny<Vektgruppe>())).Callback(vektgrupper.Clear);
            var vektgruppeService = new VektgruppeService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            await vektgruppeService.DeleteAsync(1, 1);

            // Assert
            Assert.Empty(vektgrupper);
        }

        [Fact]
        public async Task UpdateAsync_RegistersAndReturnsVaregruppeResponse()
        {
            // Arrange
            var updatedVektgruppe = new RegisterVektgruppeRequest
            {
                Navn = "Mat",
                DefaultKassevekt = 1.4f
            };
            var vektgruppe = _mapper.Map<Vektgruppe>(updatedVektgruppe);

            var entityEntryMock = new Mock<EntityEntry<Vektgruppe>>(null);
            entityEntryMock.Setup(x => x.Entity).Returns(vektgruppe);

            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Vektgrupper).ReturnsDbSet(
                new List<Vektgruppe>
            {
                new Vektgruppe{ Id = 1, LokasjonId = 1},
            });
            databaseMock.Setup(x => x.UpdateSaveAndGetAsync(It.IsAny<DbSet<Vektgruppe>>(), It.IsAny<Vektgruppe>()))
                .Callback(() => { vektgruppe.Id = 1; vektgruppe.Lokasjon = new Lokasjon { Id = 1, Navn = "Lokasjon" }; })
                .ReturnsAsync(vektgruppe);

            var vektgruppeService = new VektgruppeService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var vektgrupper = await vektgruppeService.UpdateAsync(1, 1, updatedVektgruppe);

            //Assert

            Assert.NotNull(vektgruppe);
            Assert.Equal(1, vektgrupper.Id);
            Assert.Equal(1, vektgrupper.Lokasjon.Id);
            Assert.Equal(vektgruppe.Navn, vektgrupper.Navn);
        }
    }
}
