using Backend.Auth;
using Backend.Controllers;
using Backend.Models;
using Backend.Services;
using Google.Apis.Auth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDbContext<DatabaseContext>(options =>
{
    options.UseSqlServer(builder.Configuration.GetConnectionString("DatabaseContext"));
});

builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(policy =>
        policy.WithOrigins("http://localhost:5173")
        .AllowAnyHeader()
        .AllowAnyMethod()
        .AllowCredentials()
    );
});

var googleValidator = new GoogleValidator(new GoogleJsonWebSignature.ValidationSettings
{
    Audience = new string[] { builder.Configuration.GetValue<string>("ClientId")! }
});
builder.Services.AddSingleton<IGoogleValidator>(googleValidator);

var jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
jwtSecurityTokenHandler.InboundClaimTypeMap.Clear();
builder.Services.AddSingleton(jwtSecurityTokenHandler);

var secret = builder.Configuration.GetValue<string>("SymmetricSecurityKey")!;
var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secret));
var signingCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature);
builder.Services.AddSingleton(signingCredentials);

builder.Services.AddAuthentication(options =>
{
    options.RequireAuthenticatedSignIn = true;
    options.DefaultAuthenticateScheme = CookieJwtTokenAuthenticationHandler.Name;
    options.DefaultChallengeScheme = CookieJwtTokenAuthenticationHandler.Name;
    options.DefaultForbidScheme = CookieJwtTokenAuthenticationHandler.Name;
})
.AddCookie()
.AddScheme<CookieJwtTokenAuthenticationOptions, CookieJwtTokenAuthenticationHandler>
(
    CookieJwtTokenAuthenticationHandler.Name,
    options =>
    {
        options.JwtSecurityTokenHandler = jwtSecurityTokenHandler;
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = signingCredentials.Key,
            ValidIssuer = "Matsentralen",
            ValidateAudience = false,
            ValidateLifetime = true,
        };
    }
);

builder.Services.AddAuthorization(options =>
{
    options.AddPolicy(Policy.Lokasjon, policy =>
    {
        policy.Requirements.Add(new LokasjonRequirement());
    });
    options.AddPolicy(Policy.Admin, policy =>
    {
        policy.Requirements.Add(new AdminRequirement());
    });
    options.AddPolicy(Policy.SuperAdmin, policy =>
    {
        policy.Requirements.Add(new SuperAdminRequirement());
    });
});

builder.Services.AddSingleton<IAuthorizationHandler, LokasjonAuthorizationHandler>();
builder.Services.AddSingleton<IAuthorizationHandler, AdminAuthorizationHandler>();
builder.Services.AddSingleton<IAuthorizationHandler, SuperAdminAuthorizationHandler>();
builder.Services.AddSingleton<AuthorizationHandlerHelper>();

builder.Services.AddControllers(options =>
{
    options.Filters.Add(typeof(ReformatHandler));
    options.SuppressImplicitRequiredAttributeForNonNullableReferenceTypes = true;
});
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    var filePath = Path.Combine(AppContext.BaseDirectory, "Backend.xml");
    options.IncludeXmlComments(filePath);
});

builder.Services.AddScoped<ILoginService, LoginService>();
builder.Services.AddScoped<IBrukerService, BrukerService>();
builder.Services.AddScoped<IInnleveringService, InnleveringService>();
builder.Services.AddScoped<ILeverandorService, LeverandorService>();
builder.Services.AddScoped<ILokasjonService, LokasjonService>();
builder.Services.AddScoped<ISelskapService, SelskapService>();
builder.Services.AddScoped<IVaregruppeService, VaregruppeService>();
builder.Services.AddScoped<IUtleveringService, UtleveringService>();
builder.Services.AddScoped<IVektgruppeService, VektgruppeService>();
builder.Services.AddScoped<IOrganisasjonService, OrganisasjonService>();
builder.Services.AddScoped<IValidator, Validator>();

builder.Services.AddHttpContextAccessor();

builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    using (var scope = app.Services.CreateScope())
    {
        var services = scope.ServiceProvider;
        SeedData.Initialize(services);
    }

    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseCors();

app.UseExceptionHandler("/error");

app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

app.Run();
