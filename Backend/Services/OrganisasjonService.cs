﻿using AutoMapper;
using Backend.DTOs.Responses;
using Backend.Models;
using Microsoft.EntityFrameworkCore;

namespace Backend.Services
{
    /// <summary>
    /// Interface of Organisasjon Service
    ///
    /// Adding, deleting and updating Organisasjoner is connected to another project
    /// </summary>
    public interface IOrganisasjonService
    {
        public Task<IReadOnlyList<OrganisasjonResponse>> GetAllOrganisasjoner();
    }

    public class OrganisasjonService : IOrganisasjonService
    {
        private readonly ILogger<OrganisasjonService> _logger;
        private readonly DatabaseContext _database;
        private readonly IMapper _mapper;

        public OrganisasjonService(ILogger<OrganisasjonService> logger, DatabaseContext database, IMapper mapper)
        {
            _logger = logger;
            _database = database;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all Organisasjoner
        /// </summary>
        /// <returns>A list of Organisasjoner</returns>
        public async Task<IReadOnlyList<OrganisasjonResponse>> GetAllOrganisasjoner()
        {
            _logger.LogInformation($"Henter alle organisasjoner");
            var organisasjoner = await _database.Organisasjoner.ToListAsync();
            return _mapper.Map<List<OrganisasjonResponse>>(organisasjoner);
        }
    }
}
