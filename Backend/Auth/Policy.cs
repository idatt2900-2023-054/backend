﻿namespace Backend.Auth
{
    /// <summary>
    /// The policies which are used to constraint use of endpoints
    /// </summary>
    public static class Policy
    {
        public const string Lokasjon = "Lokasjon";
        public const string Admin = "Admin";
        public const string SuperAdmin = "SuperAdmin";
    }
}
