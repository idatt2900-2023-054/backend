﻿namespace Backend.DTOs.Responses
{
    public class UtleveringResponse
    {
        public int Id { get; set; }
        public DateOnly Dato { get; set; }
        public float Bruttovekt { get; set; }
        public int KasserInn { get; set; }
        public int KasserUt { get; set; }
        public float Kassevekt { get; set; }
        public float Nettovekt { get; set; }
        public bool Primaer { get; set; }
        public string? Notat { get; set; }
        public VektgruppeResponse Vektgruppe { get; set; }
        public LokasjonResponse Lokasjon { get; set; }
        public OrganisasjonResponse Organisasjon { get; set; }
    }
}
