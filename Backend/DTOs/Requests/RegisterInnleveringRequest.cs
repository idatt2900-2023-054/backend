﻿namespace Backend.DTOs.Requests
{
    public class RegisterInnleveringRequest
    {
        [ValidDate]
        public DateOnly Dato { get; set; }
        [PositiveInteger]
        public int KasserInn { get; set; }
        [PositiveInteger]
        public int VaregruppeId { get; set; } = -1;
        [PositiveFloat]
        public float BruttoVekt { get; set; } = -1;
        [PositiveFloat]
        public float Kassevekt { get; set; }
        public bool Primaer { get; set; }
        public bool RenDonasjon { get; set; }
        [PositiveInteger]
        public int LeverandorId { get; set; } = -1;
        public string? Notat { get; set; }
    }
}
