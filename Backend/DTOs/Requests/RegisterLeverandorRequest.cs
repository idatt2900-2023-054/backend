﻿namespace Backend.DTOs.Requests
{
    public class RegisterLeverandorRequest
    {
        [NonEmptyString]
        public string Navn { get; set; }
        public bool Aktiv { get; set; }
        [NonEmptyString]
        public string LeverandorType { get; set; }
        [NonEmptyString]
        public string Adresse { get; set; }
        [PositiveInteger(IsNullable = true)]
        public int? SelskapId { get; set; }
    }
}