﻿using Microsoft.EntityFrameworkCore;

namespace Backend.Models
{
    [Index(nameof(LokasjonId), nameof(Dato))]
    public class Utlevering
    {
        public int Id { get; set; }
        public DateTime Dato { get; set; }
        public float Bruttovekt { get; set; }
        public float Kassevekt { get; set; }
        public int KasserInn { get; set; }
        public int KasserUt { get; set; }
        public int Kassebalanse => KasserInn - KasserUt;
        public float Nettovekt => Bruttovekt - KasserUt * Kassevekt;
        public bool Primaer { get; set; }
        public string? Notat { get; set; }
        public Vektgruppe Vektgruppe { get; set; }
        public int VektgruppeId { get; set; }
        public Lokasjon Lokasjon { get; set; }
        public int LokasjonId { get; set; }
        public Organisasjon Organisasjon { get; set; }
        public int OrganisasjonId { get; set; }
    }
}
