﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Text.RegularExpressions;

namespace Backend.Controllers
{
    /// <summary>
    /// Class to catch validation problems and reformat their error message
    /// </summary>
    public class ReformatHandler : ActionFilterAttribute
    {
        /// <summary>
        /// Catche problems with validation and rewrites the error message format
        /// </summary>
        public override void OnResultExecuting(ResultExecutingContext context)
        {
            if (context.Result is BadRequestObjectResult badRequestObjectResult)
                if (badRequestObjectResult.Value is ValidationProblemDetails validationProblemDetalis)
                {
                    List<string> errors = new List<string>();
                    foreach (var error in validationProblemDetalis.Errors)
                    {
                        if (error.Key.StartsWith("$"))
                        {
                            errors.Add("Feil med " + Regex.Replace(error.Key, @"[^a-zA-Z]", ""));
                        }
                        else errors.Add(error.Value.First());
                    }
                    context.Result = new BadRequestObjectResult(new { Message = $"{string.Join(", ", errors)}" });
                }
            base.OnResultExecuting(context);
        }
    }
}
