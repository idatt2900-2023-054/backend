using Backend.Auth;
using Backend.DTOs.Requests;
using Backend.DTOs.Responses;
using Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers
{
    /// <summary>
    /// Controller class for the endpoints connected to Lokasjoner
    /// All of the endpoints have the SuperAdmin Policy
    /// </summary>
    [ApiController]
    public class LokasjonController : ControllerBase
    {
        private readonly ILogger<LokasjonController> _logger;
        private readonly ILokasjonService _lokasjonService;

        public LokasjonController(ILogger<LokasjonController> logger, ILokasjonService lokasjonService)
        {
            _logger = logger;
            _lokasjonService = lokasjonService;
        }

        /// <summary>
        /// Get all Lokasjoner
        /// </summary>
        /// <returns>A list of all Lokasjoner</returns>
        [HttpGet("lokasjoner")]
        [Authorize(Policy.SuperAdmin)]
        public async Task<ActionResult<LokasjonResponse>> GetAll()
        {
            return Ok(await _lokasjonService.GetAllLokasjonerAsync());
        }

        /// <summary>
        /// Add a new Lokasjon
        /// </summary>
        /// <param name="request">Lokasjon request</param>
        /// <returns>The new Lokasjon</returns>
        [HttpPost("lokasjon")]
        [Authorize(Policy.SuperAdmin)]
        public async Task<ActionResult<LokasjonResponse>> Add
            (
                [FromBody] RegisterLokasjonRequest request
            )
        {
            return Created(nameof(Add), await _lokasjonService.AddAsync(request));
        }

        /// <summary>
        /// Update a given Lokasjon
        /// </summary>
        /// <param name="lokasjonId">Id of Lokasjon to be updated</param>
        /// <param name="request">Updated Lokasjon request</param>
        /// <returns>The updated Lokasjon</returns>
        [HttpPut("lokasjon/{lokasjonId}")]
        [Authorize(Policy.SuperAdmin)]
        public async Task<ActionResult<LokasjonResponse>> Update
    (
        [FromRoute] int lokasjonId,
        [FromBody] RegisterLokasjonRequest request
    )
        {
            return Ok(await _lokasjonService.UpdateAsync(lokasjonId, request));
        }

        /// <summary>
        /// Delete a Lokasjon
        /// </summary>
        /// <param name="lokasjonId">Id of the Lokasjon to be deleted</param>
        /// <returns>Status 200 OK if deleted</returns>
        [HttpDelete("lokasjon/{lokasjonId}")]
        [Authorize(Policy.SuperAdmin)]
        public async Task<ActionResult> Delete
            (
                [FromRoute] int lokasjonId
            )
        {
            await _lokasjonService.DeleteAsync(lokasjonId);
            return Ok();
        }
    }
}
