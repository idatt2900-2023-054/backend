﻿namespace Backend.Models
{
    public class Leverandor
    {
        public int Id { get; set; }
        public string Navn { get; set; }
        public bool Aktiv { get; set; }
        public string LeverandorType { get; set; }
        public string Adresse { get; set; }
        public Selskap? Selskap { get; set; }
        public int? SelskapId { get; set; }

    }
}
