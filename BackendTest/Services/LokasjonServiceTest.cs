﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Logging;
using Moq;
using Moq.EntityFrameworkCore;

namespace BackendTest.Services
{
    /// <summary>
    /// Unit tests for Lokasjon Service
    /// Test name format: "Method name" _ "What is expected to happen"
    /// Ex: Method_ThrowsException
    /// </summary>
    public class LokasjonServiceTest
    {
        private readonly ILogger<LokasjonService> _loggerMock;
        private readonly IMapper _mapper;
        private readonly IValidator _validator;

        public LokasjonServiceTest()
        {
            _loggerMock = new Mock<ILogger<LokasjonService>>().Object;

            _mapper = new Mapper(new MapperConfiguration(config =>
                config.AddProfile<AutoMapperProfile>()
            ));

            _validator = Utils.GetValidatorMock().Object;
        }

        [Fact]
        public async Task GetAllLokasjonerAsync_ReturnsLokasjonResponses()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Lokasjoner).ReturnsDbSet(
                new List<Lokasjon> {
                    new Lokasjon{ Id = 1 },
                    new Lokasjon{ Id = 2 },
                }
            );

            var lokasjonService = new LokasjonService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var lokasjoner = await lokasjonService.GetAllLokasjonerAsync();

            //Assert
            Assert.Equal(2, lokasjoner.Count);
            Assert.True(lokasjoner.Select(x => x.Id == 1).Any());
            Assert.True(lokasjoner.Select(x => x.Id == 2).Any());
        }

        [Fact]
        public async Task GetAllLokasjonerAsync_ReturnsEmptyWhenEmpty()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Lokasjoner).ReturnsDbSet(
                new List<Lokasjon>()
            );

            var lokasjonService = new LokasjonService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var lokasjoner = await lokasjonService.GetAllLokasjonerAsync();

            //Assert
            Assert.Empty(lokasjoner);
        }

        [Fact]
        public async Task AddAsync_RegistersAndReturnsLokasjonResponse()
        {
            // Arrange
            var registerLokasjonRequest = new RegisterLokasjonRequest
            {
                Navn = "lokasjon",
            };
            var lokasjon = _mapper.Map<Lokasjon>(registerLokasjonRequest);

            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.AddSaveAndGetAsync(It.IsAny<DbSet<Lokasjon>>(), It.IsAny<Lokasjon>()))
                .Callback(() => lokasjon.Id = 1)
                .ReturnsAsync(lokasjon);

            var lokasjonService = new LokasjonService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var returnLokasjon = await lokasjonService.AddAsync(registerLokasjonRequest);

            //Assert
            Assert.NotNull(returnLokasjon);
            Assert.Equal(lokasjon.Id, returnLokasjon.Id);
            Assert.Equal(lokasjon.Navn, returnLokasjon.Navn);
        }

        [Fact]
        public async Task UpdateAsync_RegistersAndReturnsLokasjonResponse()
        {
            // Arrange
            var updatedLokasjonRequest = new RegisterLokasjonRequest
            {
                Navn = "lokasjon"
            };
            var lokasjon = _mapper.Map<Lokasjon>(updatedLokasjonRequest);

            var entityEntryMock = new Mock<EntityEntry<Lokasjon>>(null);
            entityEntryMock.Setup(x => x.Entity).Returns(lokasjon);

            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.UpdateSaveAndGetAsync(It.IsAny<DbSet<Lokasjon>>(), It.IsAny<Lokasjon>()))
                .Callback(() => lokasjon.Id = 1)
                .ReturnsAsync(lokasjon);

            var lokasjonService = new LokasjonService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var lokasjoner = await lokasjonService.UpdateAsync(1, updatedLokasjonRequest);

            //Assert

            Assert.NotNull(lokasjoner);
            Assert.Equal(1, lokasjoner.Id);
            Assert.Equal(lokasjon.Navn, lokasjoner.Navn);
        }



        [Fact]
        public async Task DeleteAsync_DeletesAndReturnsNothing()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            var lokasjoner = new List<Lokasjon>
                {
                    new Lokasjon{ Id = 1},
                };
            databaseMock.Setup(x => x.Lokasjoner).ReturnsDbSet(lokasjoner);
            databaseMock.Setup(x => x.Lokasjoner.Remove(It.IsAny<Lokasjon>())).Callback(() =>
            {
                lokasjoner.Clear();
            });
            var lokasjonService = new LokasjonService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            await lokasjonService.DeleteAsync(1);
            Assert.Empty(lokasjoner);
        }
    }
}