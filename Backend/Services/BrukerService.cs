﻿using AutoMapper;
using Backend.DTOs.Requests;
using Backend.DTOs.Responses;
using Backend.Models;
using Microsoft.EntityFrameworkCore;

namespace Backend.Services
{
    /// <summary>
    /// Interface of the Bruker Service
    /// </summary>
    public interface IBrukerService
    {
        public Task<IReadOnlyList<BrukerResponse>> GetAllBrukereAsync();
        public Task<BrukerResponse> AddAsync(RegisterBrukerRequest request);
        public Task<BrukerResponse> UpdateAsync(int brukerId, RegisterBrukerRequest updateBrukerRequest);
        public Task DeleteAsync(int brukerId);
    }

    public class BrukerService : IBrukerService
    {
        private readonly ILogger<BrukerService> _logger;
        private readonly DatabaseContext _database;
        private readonly IMapper _mapper;
        private readonly IValidator _validator;

        public BrukerService(ILogger<BrukerService> logger, DatabaseContext database, IMapper mapper, IValidator validator)
        {
            _logger = logger;
            _database = database;
            _mapper = mapper;
            _validator = validator;
        }
        /// <summary>
        /// Get all Brukere
        /// </summary>
        /// <returns>List of Brukere</returns>
        public async Task<IReadOnlyList<BrukerResponse>> GetAllBrukereAsync()
        {
            _logger.LogInformation("Henter alle brukere");
            var brukerr = await _database.Brukere.ToListAsync();
            return _mapper.Map<List<BrukerResponse>>(brukerr);
        }

        /// <summary>
        /// Add a new Bruker
        /// Validates Email and Lokasjon
        /// </summary>
        /// <param name="request">Bruker request</param>
        /// <returns></returns>
        public async Task<BrukerResponse> AddAsync(RegisterBrukerRequest request)
        {
            _logger.LogInformation("Legger til ny bruker");
            await _validator.BrukerDoesNotExistAsync(request.Email);
            await _validator.LokasjonExistsAsync(request.LokasjonId);

            var bruker = _mapper.Map<Bruker>(request);

            bruker = await _database.AddSaveAndGetAsync(_database.Brukere, bruker);
            _logger.LogInformation($"Bruker {bruker.Id} lagt til");

            return _mapper.Map<BrukerResponse>(bruker);
        }

        /// <summary>
        /// Update a Bruker
        /// Validates Bruker and Lokasjon
        /// </summary>
        /// <param name="brukerId">Id of Bruker to be updated</param>
        /// <param name="request">Updated Bruker request</param>
        /// <returns>The updated Bruker</returns>
        public async Task<BrukerResponse> UpdateAsync(int brukerId, RegisterBrukerRequest request)
        {
            _logger.LogInformation($"Oppdaterer bruker {brukerId}");
            var oldEmail = await _validator.BrukerExistsAsync(brukerId);
            if (oldEmail != request.Email)
            {
                await _validator.BrukerDoesNotExistAsync(request.Email);
            }
            await _validator.LokasjonExistsAsync(request.LokasjonId);

            var bruker = _mapper.Map<Bruker>(request);
            bruker.Id = brukerId;

            bruker = await _database.UpdateSaveAndGetAsync(_database.Brukere, bruker);
            _logger.LogInformation("Bruker oppdatert");

            return _mapper.Map<BrukerResponse>(bruker);
        }
        /// <summary>
        /// Delete a Bruker
        /// </summary>
        /// <param name="brukerId">Id of Bruker to be deleted</param>
        /// <returns>Nothing</returns>
        /// <exception cref="ValidationException"></exception>
        public async Task DeleteAsync(int brukerId)
        {
            _logger.LogInformation($"Sletter bruker {brukerId}");
            var bruker = await _database.Brukere.FirstOrDefaultAsync(x => x.Id == brukerId);
            if (bruker == null)
            {
                throw new ValidationException($"Invalid bruker {brukerId}");
            }
            _database.Brukere.Remove(bruker);
            await _database.TrySaveChangesAsync("Kunne ikke slette bruker.");
            _logger.LogInformation("Bruker slettet");
        }
    }
}