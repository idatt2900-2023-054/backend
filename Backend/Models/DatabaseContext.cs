﻿using Backend.Services;
using Microsoft.EntityFrameworkCore;

namespace Backend.Models
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
        }

        /// <summary>
        /// Sets constraints and connections between different database tables on constructions
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var bruker = modelBuilder.Entity<Bruker>();
            bruker.Navigation(x => x.Lokasjon).AutoInclude();
            bruker.HasIndex(x => x.Email).IsUnique();
            bruker.HasOne(x => x.Lokasjon).WithMany().OnDelete(DeleteBehavior.NoAction);

            var innlevering = modelBuilder.Entity<Innlevering>();
            innlevering.Navigation(x => x.Varegruppe).AutoInclude();
            innlevering.Navigation(x => x.Lokasjon).AutoInclude();
            innlevering.Navigation(x => x.Leverandor).AutoInclude();
            innlevering.HasOne(x => x.Varegruppe).WithMany().OnDelete(DeleteBehavior.NoAction);
            innlevering.HasOne(x => x.Leverandor).WithMany().OnDelete(DeleteBehavior.NoAction);
            innlevering.HasOne(x => x.Lokasjon).WithMany().OnDelete(DeleteBehavior.NoAction);

            var leverandor = modelBuilder.Entity<Leverandor>();
            leverandor.HasOne(x => x.Selskap).WithMany().OnDelete(DeleteBehavior.NoAction);

            var varegruppe = modelBuilder.Entity<Varegruppe>();
            varegruppe.HasOne(x => x.Hovedgruppe).WithMany().OnDelete(DeleteBehavior.NoAction);

            var vektgruppe = modelBuilder.Entity<Vektgruppe>();
            vektgruppe.HasOne(x => x.Lokasjon).WithMany().OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Leverandor>().Navigation(x => x.Selskap).AutoInclude();

            var varegruppeLokasjon = modelBuilder.Entity<VaregruppeLokasjon>();
            varegruppeLokasjon.Navigation(x => x.Varegruppe).AutoInclude();
            varegruppeLokasjon.Navigation(x => x.Lokasjon).AutoInclude();
            varegruppeLokasjon.HasOne(x => x.Lokasjon).WithMany().OnDelete(DeleteBehavior.NoAction);
            varegruppeLokasjon.HasOne(x => x.Varegruppe).WithMany().OnDelete(DeleteBehavior.NoAction);

            var utlevering = modelBuilder.Entity<Utlevering>();
            utlevering.Navigation(x => x.Organisasjon).AutoInclude();
            utlevering.Navigation(x => x.Lokasjon).AutoInclude();
            utlevering.Navigation(x => x.Vektgruppe).AutoInclude();
            utlevering.HasOne(x => x.Vektgruppe).WithMany().OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Vektgruppe>().Navigation(x => x.Lokasjon).AutoInclude();
        }

        public virtual DbSet<Bruker> Brukere { get; set; }
        public virtual DbSet<Innlevering> Innleveringer { get; set; }
        public virtual DbSet<Lokasjon> Lokasjoner { get; set; }
        public virtual DbSet<Varegruppe> Varegrupper { get; set; }
        public virtual DbSet<VaregruppeLokasjon> VaregruppeLokasjoner { get; set; }
        public virtual DbSet<Leverandor> Leverandorer { get; set; }
        public virtual DbSet<Selskap> Selskaper { get; set; }
        public virtual DbSet<Vektgruppe> Vektgrupper { get; set; }
        public virtual DbSet<Utlevering> Utleveringer { get; set; }
        public virtual DbSet<Organisasjon> Organisasjoner { get; set; }

        /// <summary>
        /// Add and save an object to the database
        /// </summary>
        /// <typeparam name="T">The object type</typeparam>
        /// <param name="dbSet">The database table</param>
        /// <param name="entity">The object to add</param>
        /// <returns>The added object</returns>
        public virtual async Task<T> AddSaveAndGetAsync<T>(DbSet<T> dbSet, T entity) where T : class
        {
            var entry = dbSet.Add(entity);
            await TrySaveChangesAsync("Kunne ikke legge til objektet.");
            return await dbSet.FirstAsync(x => x == entity);
        }

        /// <summary>
        /// Add and save a list of objects to the database
        /// </summary>
        /// <typeparam name="T">The object types</typeparam>
        /// <param name="dbSet">The database table</param>
        /// <param name="entities">The objects to add</param>
        /// <returns>The added objects</returns>
        public virtual async Task<List<T>> AddRangeSaveAndGetAsync<T>(DbSet<T> dbSet, List<T> entities) where T : class
        {
            var entries = entities.Select(dbSet.Add).ToList();
            await TrySaveChangesAsync("Kunne ikke legge til objektene.");
            return await dbSet.Where(x => entities.Contains(x)).ToListAsync();
        }

        /// <summary>
        /// Update and save an object to the database
        /// </summary>
        /// <typeparam name="T">The object type</typeparam>
        /// <param name="dbSet">The database table</param>
        /// <param name="entity">The object to update</param>
        /// <returns>The updated object</returns>
        public virtual async Task<T> UpdateSaveAndGetAsync<T>(DbSet<T> dbSet, T entity) where T : class
        {
            dbSet.Update(entity);
            await TrySaveChangesAsync("Kunne ikke oppdatere objektet.");
            return await dbSet.FirstAsync(x => x == entity);
        }

        /// <summary>
        /// Update and save a list of objects to the database
        /// </summary>
        /// <typeparam name="T">The object types</typeparam>
        /// <param name="dbSet">The database table</param>
        /// <param name="entities">The objects to update</param>
        /// <returns>The updated objects</returns>
        public virtual async Task<List<T>> UpdateRangeSaveAndGetAsync<T>(DbSet<T> dbSet, List<T> entities) where T : class
        {
            dbSet.UpdateRange(entities);
            await TrySaveChangesAsync("Kunne ikke oppdatere objektene.");
            return await dbSet.Where(x => entities.Contains(x)).ToListAsync();
        }

        /// <summary>
        /// Save the database
        /// </summary>
        /// <param name="errorMessage">The message to be given in the validation exception</param>
        /// <returns>Nothing </returns>
        /// <exception cref="ValidationException">Throws a validation exception if the saving process fails</exception>
        public virtual async Task TrySaveChangesAsync(string errorMessage)
        {
            try
            {
                await SaveChangesAsync();
            }
            catch (DbUpdateException _)
            {
                throw new ValidationException("Noe gikk feil. " + errorMessage);
            }
        }
    }
}
