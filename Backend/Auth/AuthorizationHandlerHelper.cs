﻿using Backend.Models;
using Microsoft.AspNetCore.Authorization;

namespace Backend.Auth
{
    /// <summary>
    /// Helper to access Authorization requests
    /// </summary>
    public class AuthorizationHandlerHelper
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AuthorizationHandlerHelper(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// Get claimed Rolle
        /// </summary>
        /// <param name="authorizationHandlerContext">Context</param>
        /// <returns>Claimed Rolle</returns>
        public Rolle? GetClaimedRolle(AuthorizationHandlerContext authorizationHandlerContext)
        {
            var rolleString = authorizationHandlerContext.User.Claims.FirstOrDefault(c => c.Type == "rolle")?.Value;
            return Enum.TryParse(rolleString, out Rolle rolle) ? rolle : null;
        }

        /// <summary>
        /// Get claimed Lokasjon id
        /// </summary>
        /// <param name="authorizationHandlerContext">Context</param>
        /// <returns>Claimed Lokasjon id</returns>
        public int? GetClaimedLokasjonId(AuthorizationHandlerContext authorizationHandlerContext)
        {
            var locationIdString = authorizationHandlerContext.User.Claims.FirstOrDefault(c => c.Type == "lokasjonId")?.Value;
            return int.TryParse(locationIdString, out int locationId) ? locationId : null;
        }

        /// <summary>
        /// Get Lokasjon Id from HTTP request
        /// </summary>
        /// <returns>Null or Lokasjon Id</returns>
        public int? GetRequestLokasjonId()
        {
            var request = _httpContextAccessor.HttpContext?.Request;
            if (request == null)
                return null;

            if (!request.RouteValues.TryGetValue("lokasjonId", out var value))
                return null;

            if (value is not string lokasjonIdString)
                return null;

            if (!int.TryParse(lokasjonIdString, out int lokasjonId))
                return null;

            return lokasjonId;
        }
    }
}