﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Text.Encodings.Web;

namespace Backend.Auth
{
    public class CookieJwtTokenAuthenticationOptions : AuthenticationSchemeOptions
    {
        public JwtSecurityTokenHandler JwtSecurityTokenHandler { get; set; }
        public TokenValidationParameters TokenValidationParameters { get; set; }
    }

    /// <summary>
    /// Handler for Cookie JWT Tokens
    /// </summary>
    public class CookieJwtTokenAuthenticationHandler : AuthenticationHandler<CookieJwtTokenAuthenticationOptions>
    {
        public const string Name = "CookieJwtTokenAuthenticationScheme";

        public CookieJwtTokenAuthenticationHandler(IOptionsMonitor<CookieJwtTokenAuthenticationOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock) : base(options, logger, encoder, clock)
        {
        }

        /// <summary>
        /// Authenticate a JWT Token
        /// </summary>
        /// <returns>Success or Fail</returns>
        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (Request.Cookies.TryGetValue("jwtToken", out var token))
            {
                var jwtTokenResult = await Options.JwtSecurityTokenHandler.ValidateTokenAsync(token, Options.TokenValidationParameters);
                if (jwtTokenResult.IsValid)
                {
                    var principal = new ClaimsPrincipal(jwtTokenResult.ClaimsIdentity);
                    var ticket = new AuthenticationTicket(principal, Scheme.Name);
                    return AuthenticateResult.Success(ticket);
                }
            }

            Request.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            return AuthenticateResult.Fail("Authentication failed");
        }
    }

}