﻿namespace Backend.DTOs.Responses
{
    public class InnleveringResponse
    {
        public int Id { get; set; }
        public DateOnly Dato { get; set; }
        public float Bruttovekt { get; set; }
        public int KasserInn { get; set; }
        public int Kassebalanse { get; set; }
        public float Kassevekt { get; set; }
        public float Nettovekt { get; set; }
        public bool RenDonasjon { get; set; }
        public bool Primaer { get; set; }
        public string? Notat { get; set; }
        public VaregruppeResponse Varegruppe { get; set; }
        public LokasjonResponse Lokasjon { get; set; }
        public LeverandorResponse Leverandor { get; set; }
    }
}
