﻿using Microsoft.EntityFrameworkCore;

namespace Backend.Models;

public static class SeedData
{
    /// <summary>
    /// Startup data for the database (Dummy data)
    /// </summary>
    /// <param name="serviceProvider">Service Provider</param>
    public static void Initialize(IServiceProvider serviceProvider)
    {
        using var context = new DatabaseContext(serviceProvider.GetRequiredService<DbContextOptions<DatabaseContext>>());
        context.Database.EnsureDeleted();
        context.Database.EnsureCreated();

        var trondheim = new Lokasjon
        {
            Navn = "Trondheim"
        };
        var oslo = new Lokasjon
        {
            Navn = "Oslo"
        };
        context.Lokasjoner.AddRange(trondheim, oslo);

        context.Brukere.Add(new Bruker
        {
            Email = "ntnu54@matsentralen.no",
            Lokasjon = trondheim,
            Rolle = Rolle.SuperAdmin
        });

        var mat = new Varegruppe
        {
            Navn = "Mat",
            DefaultKassevekt = 1.4f
        };
        var kjott = new Varegruppe
        {
            Navn = "Kjott",
            Hovedgruppe = mat,
            DefaultKassevekt = 1.4f
        };
        var drikke = new Varegruppe
        {
            Navn = "Drikke",
            Hovedgruppe = mat
        };
        context.Varegrupper.AddRange(mat, kjott, drikke);

        context.VaregruppeLokasjoner.AddRange(
            new VaregruppeLokasjon
            {
                Varegruppe = mat,
                Lokasjon = oslo
            },
            new VaregruppeLokasjon
            {
                Varegruppe = kjott,
                Lokasjon = trondheim
            },
            new VaregruppeLokasjon
            {
                Varegruppe = drikke,
                Lokasjon = trondheim
            }
        );

        var gilde = new Selskap
        {
            Navn = "Gilde"
        };
        var tine = new Selskap
        {
            Navn = "Tine"
        };
        context.Selskaper.AddRange(gilde, tine);

        var gildeTrondheim = new Leverandor
        {
            Navn = "Gilde Trondheim",
            Aktiv = true,
            LeverandorType = "produsent",
            Adresse = "Gildeveien 1",
            Selskap = gilde
        };
        var tineTrondheim = new Leverandor
        {
            Navn = "Tine Trondheim",
            Aktiv = true,
            LeverandorType = "produsent",
            Adresse = "Melkeveien 1",
            Selskap = tine
        };
        context.Leverandorer.AddRange(gildeTrondheim, tineTrondheim);

        context.Innleveringer.AddRange(
            new Innlevering
            {
                Dato = DateTime.Today,
                Bruttovekt = 56.4f,
                KasserInn = 3,
                Kassevekt = 1.8f,
                RenDonasjon = false,
                Primaer = false,
                Varegruppe = kjott,
                Lokasjon = trondheim,
                Leverandor = gildeTrondheim,
                Notat = "Notat som forteller nyttig informasjon om innleveringen og eventuelt andre ting. Disse notatene kan være nokså lange."
            },
            new Innlevering
            {
                Dato = DateTime.Today,
                Bruttovekt = 27.9f,
                KasserInn = 3,
                Kassevekt = 1.8f,
                RenDonasjon = false,
                Primaer = false,
                Varegruppe = drikke,
                Lokasjon = trondheim,
                Leverandor = tineTrondheim
            }
        );

        var torr = new Vektgruppe
        {
            Navn = "Torr",
            Lokasjon = trondheim
        };
        var kjol = new Vektgruppe
        {
            Navn = "Kjol",
            Lokasjon = trondheim,
            DefaultKassevekt = 1.4f
        };
        context.Vektgrupper.AddRange(torr, kjol);

        var kirkensBymisjon = new Organisasjon
        {
            Navn = "Kirkens Bymisjon"
        };
        context.Organisasjoner.AddRange(kirkensBymisjon);

        context.Utleveringer.AddRange(
            new Utlevering
            {
                Dato = DateTime.Today,
                KasserInn = 1,
                Bruttovekt = 27.9f,
                KasserUt = 4,
                Kassevekt = 1.8f,
                Primaer = false,
                Lokasjon = trondheim,
                Organisasjon = kirkensBymisjon,
                Vektgruppe = torr,
                Notat = "Notat som forteller nyttig informasjon om utleveringen og eventuelt andre ting. Disse notatene kan være nokså lange."
            },
            new Utlevering
            {
                Dato = DateTime.Today,
                KasserInn = 2,
                Bruttovekt = 23.9f,
                KasserUt = 2,
                Kassevekt = 1.6f,
                Primaer = true,
                Lokasjon = trondheim,
                Organisasjon = kirkensBymisjon,
                Vektgruppe = torr
            }
        );

        context.SaveChanges();
    }
}