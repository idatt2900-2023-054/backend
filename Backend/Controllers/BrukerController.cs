﻿using Backend.Auth;
using Backend.DTOs.Requests;
using Backend.DTOs.Responses;
using Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers
{
    /// <summary>
    /// Controller class for the endpoints connected to Brukere
    /// All of the endpoints have the SuperAdmin Policy
    /// </summary>
    [ApiController]
    public class BrukerController : ControllerBase
    {
        private readonly ILogger<VektgruppeController> _logger;
        private readonly IBrukerService _brukerService;

        public BrukerController(ILogger<VektgruppeController> logger, IBrukerService brukerService)
        {
            _logger = logger;
            _brukerService = brukerService;
        }

        /// <summary>
        /// Get all the registered Brukere
        /// </summary>
        /// <returns>All registered brukere</returns>
        [HttpGet("brukere")]
        [Authorize(Policy.SuperAdmin)]
        public async Task<ActionResult<IEnumerable<BrukerResponse>>> GetAll()
        {
            return Ok(await _brukerService.GetAllBrukereAsync());
        }

        /// <summary>
        /// Add a new Bruker
        /// The parameteres are sent through the body of the http request
        /// </summary>
        /// <param name="request">The Bruker info</param>
        /// <returns>The registered Bruker</returns>
        [HttpPost("bruker")]
        [Authorize(Policy.SuperAdmin)]
        public async Task<ActionResult<BrukerResponse>> Add([FromBody] RegisterBrukerRequest request)
        {
            return Created(nameof(Add), await _brukerService.AddAsync(request));
        }

        /// <summary>
        /// Update an existing Bruker
        /// </summary>
        /// <param name="brukerId">The id of the Bruker to be updated</param>
        /// <param name="updateBrukerRequest">The updated Bruker info</param>
        /// <returns>The updated Bruker</returns>
        [HttpPut("bruker/{brukerId}")]
        [Authorize(Policy.SuperAdmin)]
        public async Task<ActionResult<BrukerResponse>> Update
            (
                [FromRoute] int brukerId,
                [FromBody] RegisterBrukerRequest updateBrukerRequest
            )
        {
            return Ok(await _brukerService.UpdateAsync(brukerId, updateBrukerRequest));
        }

        /// <summary>
        /// Delete a bruker
        /// </summary>
        /// <param name="brukerId">The id of the Bruker to be deleted</param>
        /// <returns>Status 200 if deleted</returns>
        [HttpDelete("bruker/{brukerId}")]
        [Authorize(Policy.SuperAdmin)]
        public async Task<ActionResult> Delete([FromRoute] int brukerId)
        {
            await _brukerService.DeleteAsync(brukerId);
            return Ok();
        }
    }
}