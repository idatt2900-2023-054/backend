﻿using AutoMapper;
using Backend.DTOs.Requests;
using Backend.DTOs.Responses;
using Backend.Models;
using Microsoft.EntityFrameworkCore;

namespace Backend.Services
{
    /// <summary>
    /// Interface of the Leverandør Service
    /// </summary>
    public interface ISelskapService
    {
        public Task<IReadOnlyList<SelskapResponse>> GetAllSelskapAsync();
        public Task<SelskapResponse> AddAsync(RegisterSelskapRequest request);
        public Task<SelskapResponse> UpdateAsync(int selskapId, RegisterSelskapRequest request);
        public Task DeleteAsync(int selskapId);
    }

    public class SelskapService : ISelskapService
    {
        private readonly ILogger<SelskapService> _logger;
        private readonly DatabaseContext _database;
        private readonly IMapper _mapper;
        private readonly IValidator _validator;

        public SelskapService(ILogger<SelskapService> logger, DatabaseContext database, IMapper mapper, IValidator validator)
        {
            _logger = logger;
            _database = database;
            _mapper = mapper;
            _validator = validator;
        }
        /// <summary>
        /// Get all Selskaper
        /// </summary>
        /// <returns>A list of Selskaper</returns>
        public async Task<IReadOnlyList<SelskapResponse>> GetAllSelskapAsync()
        {
            _logger.LogInformation("Henter alle selskapene");
            var leverandorSelskap = await _database.Selskaper.ToListAsync();
            return _mapper.Map<List<SelskapResponse>>(leverandorSelskap);
        }

        /// <summary>
        /// Add a new Selskap
        /// </summary>
        /// <param name="request">Selskap request</param>
        /// <returns>The new Selskap</returns>
        public async Task<SelskapResponse> AddAsync(RegisterSelskapRequest request)
        {
            _logger.LogInformation("Legger til nytt selskap");
            var selskap = _mapper.Map<Selskap>(request);
            selskap = await _database.AddSaveAndGetAsync(_database.Selskaper, selskap);
            _logger.LogInformation($"Selskap lagt til {selskap.Id}");
            return _mapper.Map<SelskapResponse>(selskap);
        }

        /// <summary>
        /// Update a Selskap
        /// </summary>
        /// <param name="selskapId">Id of Selskap to be updated</param>
        /// <param name="request">Updated Selskap request</param>
        /// <returns>The updated Selskap</returns>
        public async Task<SelskapResponse> UpdateAsync(int selskapId, RegisterSelskapRequest request)
        {
            _logger.LogInformation($"Oppdaterer selskap {selskapId}");
            await _validator.SelskapExistsAsync(selskapId);

            var selskap = _mapper.Map<Selskap>(request);
            selskap.Id = selskapId;

            selskap = await _database.UpdateSaveAndGetAsync(_database.Selskaper, selskap);
            _logger.LogInformation("Selskap oppdatert");
            return _mapper.Map<SelskapResponse>(selskap);
        }

        /// <summary>
        /// Delete a Selskap
        /// Checks if Selskap exists
        /// </summary>
        /// <param name="selskapId">Id of Selskap to be deleted</param>
        /// <returns>Nothing</returns>
        /// <exception cref="ValidationException">Throws validation exception if Selskap does not exist</exception>
        public async Task DeleteAsync(int selskapId)
        {
            _logger.LogInformation($"Sletter selskap {selskapId}");
            var selskap = await _database.Selskaper.FirstOrDefaultAsync(x => x.Id == selskapId);
            if (selskap == null)
            {
                throw new ValidationException($"Invalid selskap {selskapId}");
            }
            _database.Selskaper.Remove(selskap);
            await _database.TrySaveChangesAsync("Kunne ikke slette selskapet.");
            _logger.LogInformation("Selskap slettet");
        }
    }
}
