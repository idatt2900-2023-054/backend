﻿using System.ComponentModel.DataAnnotations;

namespace Backend.DTOs.Requests
{
    public class RegisterBrukerRequest
    {
        [EmailAddress(ErrorMessage = "Email er på feil format")]
        public string Email { get; set; }
        [PositiveInteger]
        public int LokasjonId { get; set; } = -1;
        [RolleString]
        public string Rolle { get; set; }
    }
}
