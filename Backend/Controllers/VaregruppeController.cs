﻿using Backend.Auth;
using Backend.DTOs.Requests;
using Backend.DTOs.Responses;
using Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers
{
    /// <summary>
    /// Controller class for the endpoints connected to Varegrupper
    /// The endpoints have different policies:
    /// - Getting Varegrupper is Lokasjon policy
    /// - Handling Varegrupper is SuperAdmin policy
    /// - Handling Varegrupper at a Lokasjon is Admin policy
    /// </summary>
    [ApiController]
    public class VaregruppeController : ControllerBase
    {
        private readonly ILogger<VaregruppeController> _logger;
        private readonly IVaregruppeService _varegruppeService;

        public VaregruppeController(ILogger<VaregruppeController> logger, IVaregruppeService varegruppeService)
        {
            _logger = logger;
            _varegruppeService = varegruppeService;
        }

        /// <summary>
        /// Get all Varegrupper
        /// </summary>
        /// <returns>List of all Varegrupper</returns>
        [HttpGet("varegrupper")]
        [Authorize(Policy.Lokasjon)]
        public async Task<ActionResult<IEnumerable<VaregruppeResponse>>> GetAll()
        {
            return Ok(await _varegruppeService.GetAllVaregrupperAsync());
        }

        /// <summary>
        /// Get all Varegrupper at a Lokasjon
        /// </summary>
        /// <param name="lokasjonId">Id of Lokasjon to find from</param>
        /// <returns>A list of Varegrupper</returns>
        [HttpGet("lokasjon/{lokasjonId}/varegrupper")]
        [Authorize(Policy.Lokasjon)]
        public async Task<ActionResult<IEnumerable<VaregruppeResponse>>> GetOnLokasjon([FromRoute] int lokasjonId)
        {
            return Ok(await _varegruppeService.GetVaregrupperOnLokasjonAsync(lokasjonId));
        }

        /// <summary>
        /// Get all Hovedgrupper, with a tree structure that contains nodes
        /// </summary>
        /// <returns>All hovedgrupper and their nodes</returns>
        [HttpGet("hovedgrupper")]
        [Authorize(Policy.SuperAdmin)]
        public async Task<ActionResult<IEnumerable<HovedgrupperResponse>>> GetHovedgrupper()
        {
            return Ok(await _varegruppeService.GetHovedgrupperRecursiveAsync());
        }

        /// <summary>
        /// Get all Hovedgrupper at a Lokasjon, with a tree structure that contains nodes
        /// Also has information if the Varegruppe is active or not
        /// </summary>
        /// <param name="lokasjonId">Id of Lokasjon to find from</param>
        /// <returns>All hovedgrupper and their nodes</returns>
        [HttpGet("lokasjon/{lokasjonId}/hovedgrupper")]
        [Authorize(Policy.Admin)]
        public async Task<ActionResult<IEnumerable<HovedgrupperResponse>>> GetHovedgrupperOnLokasjon([FromRoute] int lokasjonId)
        {
            return Ok(await _varegruppeService.GetHovedgrupperRecursiveWithLokasjonAsync(lokasjonId));
        }

        /// <summary>
        /// Add a new Varegruppe
        /// </summary>
        /// <param name="request">Info about Varegruppe</param>
        /// <returns>The new Varegruppe</returns>
        [HttpPost("varegruppe")]
        [Authorize(Policy.SuperAdmin)]
        public async Task<ActionResult<VaregruppeResponse>> Add([FromBody] RegisterVaregruppeRequest request)
        {
            return Created(nameof(Add), await _varegruppeService.AddAsync(request));
        }

        /// <summary>
        /// Update a given Varegruppe
        /// </summary>
        /// <param name="varegruppeId">Id of Varegruppe to be updated</param>
        /// <param name="request">Updated Varegruppe request</param>
        /// <returns>Updated Varegruppe</returns>
        [HttpPut("varegruppe/{varegruppeId}")]
        [Authorize(Policy.SuperAdmin)]
        public async Task<ActionResult<VaregruppeResponse>> Update([FromRoute] int varegruppeId, [FromBody] RegisterVaregruppeRequest request)
        {
            return Ok(await _varegruppeService.UpdateAsync(varegruppeId, request));
        }

        /// <summary>
        /// Delete a given Varegruppe
        /// </summary>
        /// <param name="varegruppeId">Id of Varegruppe to be updated</param>
        /// <returns>Status 200 OK if deleted</returns>
        [HttpDelete("varegruppe/{varegruppeId}")]
        [Authorize(Policy.SuperAdmin)]
        public async Task<ActionResult> Delete([FromRoute] int varegruppeId)
        {
            await _varegruppeService.DeleteAsync(varegruppeId);
            return Ok();
        }

        /// <summary>
        /// Add a Varegruppe to a Lokasjon
        /// </summary>
        /// <param name="lokasjonId">Id of Lokasjon</param>
        /// <param name="varegruppeId">Id of Varegruppe</param>
        /// <returns></returns>
        [HttpPost("lokasjon/{lokasjonId}/varegruppe/{varegruppeId}")]
        [Authorize(Policy.Admin)]
        [Authorize(Policy.Lokasjon)]
        public async Task<ActionResult> AddToLokasjon([FromRoute] int lokasjonId, [FromRoute] int varegruppeId)
        {
            await _varegruppeService.AddVaregruppeToLokasjonAsync(varegruppeId, lokasjonId);
            return Ok();
        }

        /// <summary>
        /// Delete a Varegruppe from a Lokasjon
        /// </summary>
        /// <param name="lokasjonId">Id of Lokasjon</param>
        /// <param name="varegruppeId">Id of Varegruppe</param>
        /// <returns>Status 200 OK if deleted</returns>
        [HttpDelete("lokasjon/{lokasjonId}/varegruppe/{varegruppeId}")]
        [Authorize(Policy.Admin)]
        [Authorize(Policy.Lokasjon)]
        public async Task<ActionResult> DeleteFromLokasjon([FromRoute] int lokasjonId, [FromRoute] int varegruppeId)
        {
            await _varegruppeService.DeleteVaregruppeFromLokasjonAsync(varegruppeId, lokasjonId);
            return Ok();
        }
    }
}