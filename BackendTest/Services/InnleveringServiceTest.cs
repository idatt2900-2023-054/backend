﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Logging;
using Moq;
using Moq.EntityFrameworkCore;

namespace BackendTest.Services
{
    /// <summary>
    /// Unit tests for Innlevering Service
    /// Test name format: "Method name" _ "What is expected to happen"
    /// Ex: Method_ThrowsException
    /// </summary>
    public class InnleveringServiceTest
    {
        private readonly ILogger<InnleveringService> _loggerMock;
        private readonly IMapper _mapper;
        private readonly IValidator _validator;

        public InnleveringServiceTest()
        {
            _loggerMock = Utils.GetLogger<InnleveringService>();
            _mapper = Utils.GetMapper();
            _validator = Utils.GetValidatorMock().Object;
        }

        [Fact]
        public async Task GetAllInnleveringerAsync_ReturnsInnleveringResponses()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Innleveringer).ReturnsDbSet(
                new List<Innlevering> {
                    new Innlevering{ Id = 1 },
                    new Innlevering{ Id = 2 },
                }
            );

            var innleveringService = new InnleveringService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var innleveringer = await innleveringService.GetAllInnleveringerAsync();

            //Assert
            Assert.Equal(2, innleveringer.Count);
            Assert.True(innleveringer.Select(x => x.Id = 1).Any());
            Assert.True(innleveringer.Select(x => x.Id = 2).Any());
        }

        [Fact]
        public async Task GetAllleveringerAsync_ReturnsEmptyWhenEmpty()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Innleveringer).ReturnsDbSet(
                new List<Innlevering>()
            );

            var innleveringService = new InnleveringService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var innleveringer = await innleveringService.GetAllInnleveringerAsync();

            //Assert
            Assert.Empty(innleveringer);
        }

        [Fact]
        public async Task GetInnleveringerByLokasjonAsync_ReturnsUtleveringResponses()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Innleveringer).ReturnsDbSet(
                new List<Innlevering> {
                    new Innlevering{ Id = 1, LokasjonId = 1, Lokasjon = new Lokasjon{ Id = 1 }, Dato = DateTime.Today },
                    new Innlevering{ Id = 2, LokasjonId = 1, Lokasjon = new Lokasjon{ Id = 1 }, Dato = DateTime.Today },
                    new Innlevering{ Id = 3, LokasjonId = 2, Lokasjon = new Lokasjon{ Id = 2 }, Dato = DateTime.Today },
                }
            );

            var innleveringService = new InnleveringService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var innleveringer = await innleveringService.GetInnleveringerOnLokasjonAsync(1, DateOnly.FromDateTime(DateTime.Today), DateOnly.FromDateTime(DateTime.Today));

            //Assert
            Assert.Equal(2, innleveringer.Count);
            Assert.True(innleveringer.Select(x => x.Id = 1).Any());
            Assert.True(innleveringer.Select(x => x.Id = 2).Any());
            Assert.All(innleveringer, innlevering => Assert.Equal(1, innlevering.Lokasjon.Id));
        }

        [Fact]
        public async Task GetInnleveringerByLokasjonAsync_ReturnsEmptyWhenNoInnleveringExists()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Innleveringer).ReturnsDbSet(
                new List<Innlevering>()
            );

            var innleveringService = new InnleveringService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var innleveringer = await innleveringService.GetInnleveringerOnLokasjonAsync(1, DateOnly.FromDateTime(DateTime.Today), DateOnly.FromDateTime(DateTime.Today));

            //Assert
            Assert.Empty(innleveringer);
        }

        [Fact]
        public async Task GetInnleveringerByLokasjonAsync_ReturnsEmptyWhenNoInnleveringWithSpecifiedLokasjonExists()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Innleveringer).ReturnsDbSet(
                new List<Innlevering>
                {
                    new Innlevering{ Id = 3, LokasjonId = 2, Lokasjon = new Lokasjon{ Id = 2 } },
                }
            );

            var innleveringService = new InnleveringService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var innleveringer = await innleveringService.GetInnleveringerOnLokasjonAsync(1, DateOnly.FromDateTime(DateTime.Today), DateOnly.FromDateTime(DateTime.Today));

            //Assert
            Assert.Empty(innleveringer);
        }

        [Fact]
        public async Task AddRangeAsync_RegistersAndReturnsInnleveringResponse()
        {
            // Arrange
            var newInnleveringRequest = new RegisterInnleveringRequest
            {
                Dato = DateOnly.FromDateTime(DateTime.Today),
                KasserInn = 2,
                Primaer = false,
                RenDonasjon = false,
                LeverandorId = 1,
                VaregruppeId = 1,
                BruttoVekt = 54.5f,
                Kassevekt = 1.5f,

            };
            var innlevering = _mapper.Map<Innlevering>(newInnleveringRequest);

            var entityEntryMock = new Mock<EntityEntry<Innlevering>>(null);
            entityEntryMock.Setup(x => x.Entity).Returns(innlevering);

            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.AddRangeSaveAndGetAsync(It.IsAny<DbSet<Innlevering>>(), It.IsAny<List<Innlevering>>()))
                    .Callback(() => innlevering.Id = 1)
                    .ReturnsAsync(new List<Innlevering> { innlevering
        });

            var innleveringService = new InnleveringService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var innleveringer = await innleveringService.AddRangeAsync(1, new List<RegisterInnleveringRequest> { newInnleveringRequest });

            //Assert
            Assert.Single(innleveringer);
            Assert.NotNull(innleveringer[0]);
            Assert.Equal(innlevering.Id, innleveringer[0].Id);
        }

        [Fact]
        public async Task UpdateAsync_RegistersAndReturnsInnleveringResponse()
        {
            // Arrange
            var updatedInnleveringRequest = new RegisterInnleveringRequest
            {
                Dato = DateOnly.FromDateTime(DateTime.Today),
                KasserInn = 2,
                Primaer = false,
                RenDonasjon = false,
                LeverandorId = 1,
                VaregruppeId = 1,
                BruttoVekt = 54.5f,
                Kassevekt = 1.5f,
            };
            var innlevering = _mapper.Map<Innlevering>(updatedInnleveringRequest);

            var entityEntryMock = new Mock<EntityEntry<Innlevering>>(null);
            entityEntryMock.Setup(x => x.Entity).Returns(innlevering);

            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.UpdateSaveAndGetAsync(It.IsAny<DbSet<Innlevering>>(), It.IsAny<Innlevering>()))
                .Callback(() => innlevering.Id = 1)
                .ReturnsAsync(innlevering);

            var innleveringService = new InnleveringService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var innleveringer = await innleveringService.UpdateAsync(1, 1, updatedInnleveringRequest);

            //Assert

            Assert.NotNull(innleveringer);
            Assert.Equal(1, innleveringer.Id);
            Assert.Equal(innlevering.KasserInn, innleveringer.KasserInn);
            Assert.Equal(innlevering.Primaer, innleveringer.Primaer);
            Assert.Equal(innlevering.RenDonasjon, innleveringer.RenDonasjon);
        }


        [Fact]
        public async Task DeleteAsync_DeletesAndReturnsNothing()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            var innleveringer = new List<Innlevering>
            {
                new Innlevering{ Id = 1, LokasjonId = 1},
            };
            databaseMock.Setup(x => x.Innleveringer).ReturnsDbSet(innleveringer);
            databaseMock.Setup(x => x.Innleveringer.Remove(It.IsAny<Innlevering>())).Callback(innleveringer.Clear);
            var innleveringService = new InnleveringService(_loggerMock, databaseMock.Object, _mapper, _validator);

            // Act
            await innleveringService.DeleteAsync(1, 1);

            // Assert
            Assert.Empty(innleveringer);
        }
    }
}
