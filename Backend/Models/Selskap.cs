﻿namespace Backend.Models
{
    public class Selskap
    {
        public int Id { get; set; }
        public string Navn { get; set; }
    }
}
