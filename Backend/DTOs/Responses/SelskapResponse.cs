﻿namespace Backend.DTOs.Responses
{
    public class SelskapResponse
    {
        public int Id { get; set; }
        public string Navn { get; set; }
    }
}
