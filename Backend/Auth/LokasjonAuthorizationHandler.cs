﻿using Backend.Models;
using Microsoft.AspNetCore.Authorization;

namespace Backend.Auth
{
    public class LokasjonRequirement : IAuthorizationRequirement { }

    public class LokasjonAuthorizationHandler : AuthorizationHandler<LokasjonRequirement>
    {
        private readonly AuthorizationHandlerHelper _authorizationHandlerHelper;

        public LokasjonAuthorizationHandler(AuthorizationHandlerHelper authorizationHandlerHelper)
        {
            _authorizationHandlerHelper = authorizationHandlerHelper;
        }

        /// <summary>
        /// Check if the credentials can access lokasjon or is Superadmin
        /// </summary>
        /// <param name="authorizationHandlerContext">Context</param>
        /// <param name="requirement">Requirement</param>
        /// <returns>Nothing</returns>
        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext authorizationHandlerContext, LokasjonRequirement requirement)
        {
            var claimedRolle = _authorizationHandlerHelper.GetClaimedRolle(authorizationHandlerContext);
            var claimedLokasjonId = _authorizationHandlerHelper.GetClaimedLokasjonId(authorizationHandlerContext);
            var requestLokasjonId = _authorizationHandlerHelper.GetRequestLokasjonId();

            if (
                claimedRolle == Rolle.SuperAdmin ||
                claimedLokasjonId != null &&
                requestLokasjonId != null &&
                claimedLokasjonId == requestLokasjonId
            )
            {
                authorizationHandlerContext.Succeed(requirement);
            }
            else
            {
                authorizationHandlerContext.Fail();
            }
        }
    }
}