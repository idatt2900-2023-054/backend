﻿using Microsoft.EntityFrameworkCore;

namespace Backend.Models
{
    [PrimaryKey("VaregruppeId", "LokasjonId")]
    public class VaregruppeLokasjon
    {
        public Varegruppe Varegruppe { get; set; }
        public int VaregruppeId { get; set; }
        public Lokasjon Lokasjon { get; set; }
        public int LokasjonId { get; set; }
    }
}
