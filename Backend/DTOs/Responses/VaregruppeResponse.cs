﻿namespace Backend.DTOs.Responses
{
    public class VaregruppeResponse
    {
        public int Id { get; set; }
        public string Navn { get; set; }
        public int? HovedgruppeId { get; set; }
        public float DefaultKassevekt { get; set; }
    }
}
