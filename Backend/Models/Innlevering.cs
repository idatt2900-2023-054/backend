﻿using Microsoft.EntityFrameworkCore;

namespace Backend.Models
{
    [Index(nameof(LokasjonId), nameof(Dato))]
    public class Innlevering
    {
        public int Id { get; set; }
        public DateTime Dato { get; set; }
        public float Bruttovekt { get; set; }
        public int KasserInn { get; set; }
        public float Kassevekt { get; set; }
        public float Nettovekt => Bruttovekt - KasserInn * Kassevekt;
        public bool RenDonasjon { get; set; }
        public bool Primaer { get; set; }
        public string? Notat { get; set; }
        public Varegruppe Varegruppe { get; set; }
        public int VaregruppeId { get; set; }
        public Lokasjon Lokasjon { get; set; }
        public int LokasjonId { get; set; }
        public Leverandor Leverandor { get; set; }
        public int LeverandorId { get; set; }
    }
}
