﻿namespace Backend.DTOs.Requests
{
    public class RegisterVektgruppeRequest
    {
        [NonEmptyString]
        public string Navn { get; set; }
        [PositiveFloat]
        public float? DefaultKassevekt { get; set; }
    }
}
