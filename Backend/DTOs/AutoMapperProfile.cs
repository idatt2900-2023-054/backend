﻿using AutoMapper;
using Backend.DTOs.Requests;
using Backend.DTOs.Responses;
using Backend.Models;

namespace Backend.DTOs
{
    /// <summary>
    /// Mapper to convert request to Objects and Object to Responses
    /// </summary>
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Bruker, BrukerResponse>();
            CreateMap<Innlevering, InnleveringResponse>();
            CreateMap<Lokasjon, LokasjonResponse>();
            CreateMap<Varegruppe, VaregruppeResponse>();
            CreateMap<Varegruppe, HovedgrupperResponse>();
            CreateMap<Varegruppe, HovedgrupperLokasjonResponse>();
            CreateMap<Leverandor, LeverandorResponse>();
            CreateMap<Selskap, SelskapResponse>();
            CreateMap<Utlevering, UtleveringResponse>();
            CreateMap<Organisasjon, OrganisasjonResponse>();
            CreateMap<Vektgruppe, VektgruppeResponse>();

            CreateMap<RegisterBrukerRequest, Bruker>();
            CreateMap<RegisterUtleveringRequest, Utlevering>();
            CreateMap<RegisterInnleveringRequest, Innlevering>();
            CreateMap<RegisterVektgruppeRequest, Vektgruppe>();
            CreateMap<RegisterVaregruppeRequest, Varegruppe>();
            CreateMap<RegisterLokasjonRequest, Lokasjon>();
            CreateMap<RegisterSelskapRequest, Selskap>();
            CreateMap<RegisterLeverandorRequest, Leverandor>();

            CreateMap<DateOnly, DateTime>()
                .ConstructUsing(dateOnly => dateOnly.ToDateTime(TimeOnly.MinValue));

            CreateMap<DateTime, DateOnly>()
                .ConstructUsing(dateTime => DateOnly.FromDateTime(dateTime));
        }
    }
}
