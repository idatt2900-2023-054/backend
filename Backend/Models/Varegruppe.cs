﻿namespace Backend.Models
{
    public class Varegruppe
    {
        public int Id { get; set; }
        public string Navn { get; set; }
        public int? HovedgruppeId { get; set; }
        public float DefaultKassevekt { get; set; }
        public Varegruppe? Hovedgruppe { get; set; }
    }
}
