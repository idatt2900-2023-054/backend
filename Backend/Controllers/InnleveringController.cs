using Backend.Auth;
using Backend.DTOs.Requests;
using Backend.DTOs.Responses;
using Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers
{
    /// <summary>
    /// Controller class for the endpoints connected to Innleveringer
    /// All of the endpoints have the Lokasjon Policy, except getting all the innleveringer which is SuperAdmin
    /// </summary>
    [ApiController]
    public class InnleveringController : ControllerBase
    {
        private readonly ILogger<InnleveringController> _logger;
        private readonly IInnleveringService _innleveringService;

        public InnleveringController(ILogger<InnleveringController> logger, IInnleveringService innleveringService)
        {
            _logger = logger;
            _innleveringService = innleveringService;
        }
        /// <summary>
        /// Get all Innleveringer
        /// </summary>
        /// <returns>All Innleveringer</returns>
        [HttpGet("innleveringer")]
        [Authorize(Policy.SuperAdmin)]
        public async Task<ActionResult<IEnumerable<InnleveringResponse>>> GetAll()
        {
            return Ok(await _innleveringService.GetAllInnleveringerAsync());
        }

        /// <summary>
        /// Get all the Innleveringer from a lokasjon within a given timespan
        /// </summary>
        /// <param name="lokasjonId">Lokasjon to get Innleveringer</param>
        /// <param name="startDate">Start search date/param>
        /// <param name="endDate">End search date</param>
        /// <returns>List of Innleveringer which matches the criteria</returns>
        [HttpGet("lokasjon/{lokasjonId}/innleveringer")]
        [Authorize(Policy.Lokasjon)]
        public async Task<ActionResult<IEnumerable<InnleveringResponse>>> GetOnLokasjon
            (
                [FromRoute] int lokasjonId,
                [FromQuery] DateOnly startDate,
                [FromQuery] DateOnly endDate
            )
        {
            return Ok(await _innleveringService.GetInnleveringerOnLokasjonAsync(lokasjonId, startDate, endDate));
        }

        /// <summary>
        /// Register a list of new Innleveringer
        /// </summary>
        /// <param name="lokasjonId">Lokasjon to be registered at</param>
        /// <param name="registerInnleveringRequests">List about Innlevering info</param>
        /// <returns>A list of the registered Innleveringer</returns>
        [HttpPost("lokasjon/{lokasjonId}/innlevering")]
        [Authorize(Policy.Lokasjon)]
        public async Task<ActionResult<IEnumerable<InnleveringResponse>>> Add
            (
                [FromRoute] int lokasjonId,
                [FromBody] List<RegisterInnleveringRequest> registerInnleveringRequests
            )
        {
            return Created(nameof(Add), await _innleveringService.AddRangeAsync(lokasjonId, registerInnleveringRequests));
        }

        /// <summary>
        /// Update a given Innlevering
        /// </summary>
        /// <param name="lokasjonId">Lokasjon of the Innlevering to be updated</param>
        /// <param name="innleveringId">Id of the Innlevering</param>
        /// <param name="updateInnleveringRequest">Updated info about the Innlevering</param>
        /// <returns>The updated Innlevering</returns>
        [HttpPut("lokasjon/{lokasjonId}/innlevering/{innleveringId}")]
        [Authorize(Policy.Lokasjon)]
        public async Task<ActionResult<InnleveringResponse>> Update
            (
                [FromRoute] int lokasjonId,
                [FromRoute] int innleveringId,
                [FromBody] RegisterInnleveringRequest updateInnleveringRequest
            )
        {
            return Ok(await _innleveringService.UpdateAsync(innleveringId, lokasjonId, updateInnleveringRequest));
        }
        /// <summary>
        /// Delete a Innlevering
        /// </summary>
        /// <param name="lokasjonId">Lokasjon of the Innlevering to be deleted</param>
        /// <param name="innleveringId">Id of the Innlevering to be deleted</param>
        /// <returns>Status 200 OK if deleted</returns>
        [HttpDelete("lokasjon/{lokasjonId}/innlevering/{innleveringId}")]
        [Authorize(Policy.Lokasjon)]
        public async Task<ActionResult> Delete
            (
                [FromRoute] int lokasjonId,
                [FromRoute] int innleveringId
            )
        {
            await _innleveringService.DeleteAsync(innleveringId, lokasjonId);
            return Ok();
        }
    }
}