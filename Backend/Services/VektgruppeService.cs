﻿using AutoMapper;
using Backend.DTOs.Requests;
using Backend.DTOs.Responses;
using Backend.Models;
using Microsoft.EntityFrameworkCore;

namespace Backend.Services
{
    public interface IVektgruppeService
    {
        /// <summary>
        /// Interface of the Vektgruppe Service
        /// </summary>
        public Task<VektgruppeResponse> AddAsync(int lokasjonId, RegisterVektgruppeRequest request);
        public Task DeleteAsync(int vektgruppeId, int lokasjonId);
        public Task<VektgruppeResponse> UpdateAsync(int vektgruppeId, int lokasjonId, RegisterVektgruppeRequest request);
        public Task<IReadOnlyList<VektgruppeResponse>> GetAllVektgrupperAsync();
        public Task<IReadOnlyList<VektgruppeResponse>> GetVektgruppeOnLokasjonAsync(int lokasjonId);
    }

    public class VektgruppeService : IVektgruppeService
    {
        private readonly ILogger<VektgruppeService> _logger;
        private readonly DatabaseContext _database;
        private readonly IMapper _mapper;
        private readonly IValidator _validator;

        public VektgruppeService(ILogger<VektgruppeService> logger, DatabaseContext database, IMapper mapper, IValidator validator)
        {
            _logger = logger;
            _database = database;
            _mapper = mapper;
            _validator = validator;
        }

        /// <summary>
        /// Get all Vektgrupper
        /// </summary>
        /// <returns>A list of all Vektgrupper</returns>
        public async Task<IReadOnlyList<VektgruppeResponse>> GetAllVektgrupperAsync()
        {
            _logger.LogInformation("Henter alle vektgrupper");
            var vektgrupper = await _database.Vektgrupper.ToListAsync();
            return _mapper.Map<List<VektgruppeResponse>>(vektgrupper);
        }

        /// <summary>
        /// Get all Vektgrupper at a Lokasjon
        /// </summary>
        /// <param name="lokasjonId">Lokasjon to find from</param>
        /// <returns>A list of Vektgrupper</returns>
        public async Task<IReadOnlyList<VektgruppeResponse>> GetVektgruppeOnLokasjonAsync(int lokasjonId)
        {
            _logger.LogInformation($"Henter vektgrupper ved lokasjon {lokasjonId}");
            var vektgrupper = await _database.Vektgrupper.Where(x => x.LokasjonId == lokasjonId).ToListAsync();
            return _mapper.Map<List<VektgruppeResponse>>(vektgrupper);
        }

        /// <summary>
        /// Add a new Vektgruppe at Lokasjon
        /// Validates Lokasjon and sets default kassevekt to 0 if null
        /// </summary>
        /// <param name="lokasjonId">Id of Lokasjon to add Vektgruppe</param>
        /// <param name="request">Vektgruppe info</param>
        /// <returns>The new Vektgruppe</returns>
        public async Task<VektgruppeResponse> AddAsync(int lokasjonId, RegisterVektgruppeRequest request)
        {
            _logger.LogInformation("Legger til ny vektgruppe");
            if (request.DefaultKassevekt == null)
            {
                request.DefaultKassevekt = 0;
            }
            await _validator.LokasjonExistsAsync(lokasjonId);

            var vektgruppe = _mapper.Map<Vektgruppe>(request);
            vektgruppe.LokasjonId = lokasjonId;

            vektgruppe = await _database.AddSaveAndGetAsync(_database.Vektgrupper, vektgruppe);
            _logger.LogInformation($"Lagt til ny vektgruppe {vektgruppe.Id} ved lokasjon {lokasjonId}");
            return _mapper.Map<VektgruppeResponse>(vektgruppe);
        }

        /// <summary>
        /// Update a Vektgruppe
        /// Validates Vektgruppe and Lokasjon, and sets default kassevekt to 0 if null
        /// </summary>
        /// <param name="vektgruppeId">Id of vektgruppe to be updated</param>
        /// <param name="lokasjonId">Id of Lokasjon connected to Vektgruppe</param>
        /// <param name="request">Updated Vektgruppe info</param>
        /// <returns>Updated Vektgruppe</returns>
        public async Task<VektgruppeResponse> UpdateAsync(int vektgruppeId, int lokasjonId, RegisterVektgruppeRequest request)
        {
            _logger.LogInformation($"Oppdaterer vektgrupper {vektgruppeId}");
            if (request.DefaultKassevekt == null)
            {
                request.DefaultKassevekt = 0;
            }
            await _validator.VektgruppeExistsOnLokasjonAsync(vektgruppeId, lokasjonId);
            var vektgruppe = _mapper.Map<Vektgruppe>(request);
            vektgruppe.LokasjonId = lokasjonId;
            vektgruppe.Id = vektgruppeId;

            vektgruppe = await _database.UpdateSaveAndGetAsync(_database.Vektgrupper, vektgruppe);
            _logger.LogInformation("Oppdatert vektgruppe");
            return _mapper.Map<VektgruppeResponse>(vektgruppe);
        }

        /// <summary>
        /// Delete Vektgruppe
        /// Checks if Vektgruppe exists at Lokasjon
        /// </summary>
        /// <param name="vektgruppeId">Id of vektgruppe to be deleted</param>
        /// <param name="lokasjonId">Id of Lokasjon connected to Vektgruppe</param>
        /// <returns>Nothing</returns>
        /// <exception cref="ValidationException">Throws validation exception if Vektgruppe does not exist at Lokasjon</exception>
        public async Task DeleteAsync(int vektgruppeId, int lokasjonId)
        {
            _logger.LogInformation($"Sletter vektgruppe {vektgruppeId} fra lokasjon {lokasjonId}");
            var vektgruppe = await _database.Vektgrupper.FirstOrDefaultAsync(x => x.Id == vektgruppeId);
            if (vektgruppe == null || vektgruppe.LokasjonId != lokasjonId)
            {
                throw new ValidationException($"Invalid vektgruppe {vektgruppeId}");
            }
            _database.Vektgrupper.Remove(vektgruppe);
            await _database.TrySaveChangesAsync("Kunne ikke slette vektgruppen.");
            _logger.LogInformation("Slettet vektgruppe");
        }
    }
}
