﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Logging;
using Moq;
using Moq.EntityFrameworkCore;

namespace BackendTest.Services
{
    /// <summary>
    /// Unit tests for Leverandør Service
    /// Test name format: "Method name" _ "What is expected to happen"
    /// Ex: Method_ThrowsException
    /// </summary>
    public class LeverandorServiceTest
    {
        private readonly ILogger<LeverandorService> _loggerMock;
        private readonly IMapper _mapper;
        private readonly IValidator _validator;

        public LeverandorServiceTest()
        {
            _loggerMock = new Mock<ILogger<LeverandorService>>().Object;

            _mapper = new Mapper(new MapperConfiguration(config =>
                config.AddProfile<AutoMapperProfile>()
            ));

            _validator = Utils.GetValidatorMock().Object;
        }

        [Fact]
        public async Task GetAllLeverandorerAsync_ReturnsLeverandorResponse()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Leverandorer).ReturnsDbSet(
                new List<Leverandor> {
                    new Leverandor{ Id = 1 },
                    new Leverandor{ Id = 2 },
                }
            );

            var leverandorService = new LeverandorService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var leverandorer = await leverandorService.GetAllLeverandorerAsync();

            //Assert
            Assert.Equal(2, leverandorer.Count);
            Assert.True(leverandorer.Select(x => x.Id == 1).Any());
            Assert.True(leverandorer.Select(x => x.Id == 2).Any());
        }

        [Fact]
        public async Task GetAllLeverandorerAsync_ReturnsEmptyWhenEmpty()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Leverandorer).ReturnsDbSet(
                new List<Leverandor>()
            );

            var leverandorService = new LeverandorService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var leverandorer = await leverandorService.GetAllLeverandorerAsync();

            //Assert
            Assert.Empty(leverandorer);
        }

        [Fact]
        public async Task GetAktiveLeverandorerAsync_ReturnsLeverandorResponse()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Leverandorer).ReturnsDbSet(
                new List<Leverandor> {
                    new Leverandor{ Id = 1 , Aktiv = false},
                    new Leverandor{ Id = 2 , Aktiv = true},
                    new Leverandor{ Id = 3, Aktiv = true},
                }
            );

            var leverandorService = new LeverandorService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var leverandorer = await leverandorService.GetAktiveLeverandorerAsync();

            //Assert
            Assert.Equal(2, leverandorer.Count);
            Assert.True(leverandorer.Select(x => x.Id == 2).Any());
            Assert.True(leverandorer.Select(x => x.Id == 3).Any());
        }

        [Fact]
        public async Task GetAktiveLeverandorerAsync_ReturnsEmptyWhenEmpty()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Leverandorer).ReturnsDbSet(
                new List<Leverandor>()
            );

            var leverandorService = new LeverandorService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var leverandorer = await leverandorService.GetAktiveLeverandorerAsync();

            //Assert
            Assert.Empty(leverandorer);
        }

        [Fact]
        public async Task AddAsync_RegistersAndReturnsLeverandorResponse()
        {
            // Arrange
            var registerLeverandorRequest = new RegisterLeverandorRequest
            {
                Navn = "Leverandor",
                Adresse = "adresse",
                Aktiv = true,
                LeverandorType = "type",
                SelskapId = 1,

            };
            var leverandor = _mapper.Map<Leverandor>(registerLeverandorRequest);

            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.AddSaveAndGetAsync(It.IsAny<DbSet<Leverandor>>(), It.IsAny<Leverandor>()))
                .Callback(() => leverandor.Id = 1)
                .ReturnsAsync(leverandor);

            var leverandorService = new LeverandorService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var leverandorer = await leverandorService.AddAsync(registerLeverandorRequest);

            //Assert
            Assert.NotNull(leverandorer);
            Assert.Equal(leverandor.Id, leverandorer.Id);
            Assert.Equal(leverandor.Navn, leverandorer.Navn);
            Assert.Equal(leverandor.Aktiv, leverandorer.Aktiv);
            Assert.Equal(leverandor.LeverandorType, leverandorer.LeverandorType);
            Assert.Equal(leverandor.Navn, leverandorer.Navn);
        }

        [Fact]
        public async Task DeleteAsync_DeletesAndReturnsNothing()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            var leverandorer = new List<Leverandor>
            {
                new Leverandor{ Id = 1},
            };
            databaseMock.Setup(x => x.Leverandorer).ReturnsDbSet(leverandorer);
            databaseMock.Setup(x => x.Leverandorer.Remove(It.IsAny<Leverandor>())).Callback(leverandorer.Clear);
            var leverandorService = new LeverandorService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            await leverandorService.DeleteAsync(1);

            // Assert
            Assert.Empty(leverandorer);
        }

        [Fact]
        public async Task UpdateAsync_RegistersAndReturnsLeverandorResponse()
        {
            // Arrange
            var updatedLeverandor = new RegisterLeverandorRequest
            {
                Navn = "Leverandor",
                Adresse = "ny adresse",
                Aktiv = true,
                LeverandorType = "type",
                SelskapId = 1,
            };

            var leverandor = _mapper.Map<Leverandor>(updatedLeverandor);

            var entityEntryMock = new Mock<EntityEntry<Leverandor>>(null);
            entityEntryMock.Setup(x => x.Entity).Returns(leverandor);

            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Leverandorer).ReturnsDbSet(
                new List<Leverandor>
            {
                new Leverandor{ Id = 1},
            });
            databaseMock.Setup(x => x.UpdateSaveAndGetAsync(It.IsAny<DbSet<Leverandor>>(), It.IsAny<Leverandor>()))
                .Callback(() => { leverandor.Id = 1; leverandor.Selskap = new Selskap { Id = 1, Navn = "Selskap" }; })
                .ReturnsAsync(leverandor);

            var leverandorService = new LeverandorService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var leverandorer = await leverandorService.UpdateAsync(1, updatedLeverandor);

            //Assert

            Assert.NotNull(leverandorer);
            Assert.Equal(leverandor.Id, leverandorer.Id);
            Assert.Equal(leverandor.Navn, leverandorer.Navn);
            Assert.Equal(leverandor.Aktiv, leverandorer.Aktiv);
            Assert.Equal(leverandor.LeverandorType, leverandorer.LeverandorType);
            Assert.Equal(leverandor.Navn, leverandorer.Navn);
        }
    }
}
