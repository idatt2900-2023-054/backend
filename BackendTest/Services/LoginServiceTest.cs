﻿using AutoMapper;
using Google.Apis.Auth;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Moq;
using Moq.EntityFrameworkCore;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace BackendTest.Services
{
    /// <summary>
    /// Unit tests for Login Service
    /// Test name format: "Method name" _ "What is expected to happen"
    /// Ex: Method_ThrowsException
    /// </summary>
    public class LoginServiceTest
    {
        private readonly ILogger<LoginService> _loggerMock;
        private readonly IMapper _mapper;
        private readonly JwtSecurityTokenHandler _jwtSecurityTokenHandler;
        private readonly SigningCredentials _signingCredentials;
        private readonly TokenValidationParameters _tokenValidationParameters;

        public LoginServiceTest()
        {
            _loggerMock = Utils.GetLogger<LoginService>();
            _mapper = Utils.GetMapper();

            _jwtSecurityTokenHandler = new();
            _jwtSecurityTokenHandler.InboundClaimTypeMap.Clear();

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("secret_secret_secret_secret_secret_secret_secret"));
            _signingCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature);

            _tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = _signingCredentials.Key,
                ValidIssuer = "Matsentralen",
                ValidateAudience = false,
                ValidateLifetime = true,
            };

        }

        [Fact]
        public async Task LoginAsync_ReturnsLoginResponse()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Brukere).ReturnsDbSet(
                new List<Bruker> {
                    new Bruker
                    {
                        Id = 1,
                        Email = "bruker@matsentralen.no",
                        Lokasjon = new Lokasjon { Id = 1 },
                        Rolle = Rolle.Bruker
                    }
                }
            );

            var googleValidatorMock = new Mock<IGoogleValidator>();
            googleValidatorMock.Setup(v => v.ValidateAsync(It.IsAny<string>()))
                .ReturnsAsync(new GoogleJsonWebSignature.Payload
                {
                    Email = "bruker@matsentralen.no",
                    EmailVerified = true,
                });

            var request = new LoginRequest
            {
                Credential = "credential"
            };

            var loginService = new LoginService(_loggerMock, databaseMock.Object, _mapper, googleValidatorMock.Object, _jwtSecurityTokenHandler, _signingCredentials);

            //Act
            var response = await loginService.LoginAsync(request);

            //Assert
            Assert.NotNull(response);
            Assert.Equal(1, response.Bruker.Id);
            Assert.Equal("bruker@matsentralen.no", response.Bruker.Email);
            Assert.Equal(1, response.Bruker.Lokasjon.Id);
            Assert.Equal(Rolle.Bruker.ToString(), response.Bruker.Rolle);
        }

        [Fact]
        public async Task LoginAsync_ThrowsExceptionForInvalidUser()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Brukere).ReturnsDbSet(new List<Bruker>());

            var googleValidatorMock = new Mock<IGoogleValidator>();
            googleValidatorMock.Setup(v => v.ValidateAsync(It.IsAny<string>()))
                .ReturnsAsync(new GoogleJsonWebSignature.Payload
                {
                    Email = "bruker@matsentralen.no",
                    EmailVerified = true,
                });

            var request = new LoginRequest
            {
                Credential = "credential"
            };

            var loginService = new LoginService(_loggerMock, databaseMock.Object, _mapper, googleValidatorMock.Object, _jwtSecurityTokenHandler, _signingCredentials);

            // Act and Assert
            var exception = await Assert.ThrowsAsync<ValidationException>(async () => await loginService.LoginAsync(request));
            Assert.Equal("Bruker er ikke registrert", exception.Message);
        }

        [Fact]

        public async Task LoginAsync_ThrowsExceptionForNotVerifiedEmail()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Brukere).ReturnsDbSet(
                new List<Bruker> {
                    new Bruker
                    {
                        Id = 1,
                        Email = "bruker@matsentralen.no",
                        Lokasjon = new Lokasjon { Id = 1 },
                        Rolle = Rolle.Bruker
                    }
                }
            );

            var googleValidatorMock = new Mock<IGoogleValidator>();
            googleValidatorMock.Setup(v => v.ValidateAsync(It.IsAny<string>()))
                .ReturnsAsync(new GoogleJsonWebSignature.Payload
                {
                    Email = "bruker@matsentralen.no",
                    EmailVerified = false,
                });

            var request = new LoginRequest
            {
                Credential = "credential"
            };

            var loginService = new LoginService(_loggerMock, databaseMock.Object, _mapper, googleValidatorMock.Object, _jwtSecurityTokenHandler, _signingCredentials);

            // Act and Assert
            var exception = await Assert.ThrowsAsync<ValidationException>(async () => await loginService.LoginAsync(request));
            Assert.Equal("Email må være verifisert", exception.Message);
        }

        [Fact]
        public async Task LoginAsync_ThrowsExceptionForInvalidCredential()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Brukere).ReturnsDbSet(
                new List<Bruker> {
                    new Bruker
                    {
                        Id = 1,
                        Email = "bruker@matsentralen.no",
                        Lokasjon = new Lokasjon { Id = 1 },
                        Rolle = Rolle.Bruker
                    }
                }
            );

            var googleValidatorMock = new Mock<IGoogleValidator>();
            GoogleJsonWebSignature.Payload? payload = null;
            googleValidatorMock.Setup(v => v.ValidateAsync(It.IsAny<string>()))!.ReturnsAsync(payload);

            var request = new LoginRequest
            {
                Credential = "credential"
            };

            var loginService = new LoginService(_loggerMock, databaseMock.Object, _mapper, googleValidatorMock.Object, _jwtSecurityTokenHandler, _signingCredentials);

            // Act and Assert
            var exception = await Assert.ThrowsAsync<ValidationException>(async () => await loginService.LoginAsync(request));
            Assert.Equal("Legitimasjonen er feil", exception.Message);
        }

        [Fact]
        public async Task GenerateJwtToken_ReturnsJwtToken()
        {
            // Arrange
            var database = Utils.GetDatabaseContextMock().Object;
            var googleValidator = new Mock<IGoogleValidator>().Object;

            var bruker = new BrukerResponse
            {
                Id = 1,
                Email = "bruker@matsentralen.no",
                Lokasjon = new LokasjonResponse { Id = 1 },
                Rolle = Rolle.Bruker.ToString()
            };

            var loginResponse = new LoginResponse
            {
                Bruker = bruker,
                Expires = DateTime.UtcNow.AddDays(1)
            };

            var loginService = new LoginService(_loggerMock, database, _mapper, googleValidator, _jwtSecurityTokenHandler, _signingCredentials);

            //Act
            var jwtToken = loginService.GenerateJwtToken(loginResponse);
            var jwtTokenResult = await _jwtSecurityTokenHandler.ValidateTokenAsync(jwtToken, _tokenValidationParameters);
            var claims = jwtTokenResult.Claims;

            //Assert
            Assert.NotNull(jwtToken);
            Assert.NotNull(jwtTokenResult);
            Assert.True(jwtTokenResult.IsValid);
            Assert.Equal(bruker.Id.ToString(), claims["brukerId"]);
            Assert.Equal(bruker.Rolle, claims["rolle"]);
            Assert.Equal(bruker.Lokasjon.Id.ToString(), claims["lokasjonId"]);
            Assert.Equal(bruker.Email, claims["email"]);
            Assert.Equal("Matsentralen", claims["iss"]);
            Assert.Equal(bruker.Email, claims["aud"]);
            Assert.Equal((int)(loginResponse.Expires - DateTime.UnixEpoch).TotalSeconds, claims["exp"]);
        }
    }
}
