using Backend.Auth;
using Backend.DTOs.Requests;
using Backend.DTOs.Responses;
using Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers
{
    /// <summary>
    /// Controller class for the endpoints connected to Utleveringer
    /// All of the endpoints have the Lokasjon Policy, except getting all the innleveringer which is SuperAdmin
    /// </summary>
    [ApiController]
    public class UtleveringController : ControllerBase
    {
        private readonly ILogger<UtleveringController> _logger;
        private readonly IUtleveringService _utleveringService;

        public UtleveringController(ILogger<UtleveringController> logger, IUtleveringService utleveringService)
        {
            _logger = logger;
            _utleveringService = utleveringService;
        }

        /// <summary>
        /// Get all Utleveringer
        /// </summary>
        /// <returns>All Utleveringer</returns>
        [HttpGet("utleveringer")]
        [Authorize(Policy.SuperAdmin)]
        public async Task<ActionResult<IEnumerable<InnleveringResponse>>> GetAll()
        {
            return Ok(await _utleveringService.GetAllUtleveringerAsync());
        }

        /// <summary>
        /// Get all the Utleveringer from a lokasjon within given timespan
        /// </summary>
        /// <param name="lokasjonId">Lokasjon to get Utleveringer from</param>
        /// <param name="startDate">Start search date/param>
        /// <param name="endDate">End search date</param>
        /// <returns>List of Utleveringer which matches the criteria</returns>
        [HttpGet("lokasjon/{lokasjonId}/utleveringer")]
        [Authorize(Policy.Lokasjon)]
        public async Task<ActionResult<IEnumerable<InnleveringResponse>>> GetOnLokasjon
            (
                [FromRoute] int lokasjonId,
                [FromQuery] DateOnly startDate,
                [FromQuery] DateOnly endDate
            )
        {
            return Ok(await _utleveringService.GetUtleveringOnLokasjonAsync(lokasjonId, startDate, endDate));
        }

        /// <summary>
        /// Register a list of new Utleveringer
        /// </summary>
        /// <param name="lokasjonId">Lokasjon to be registered at</param>
        /// <param name="registerUtleveringRequests">List of Utlevering requests</param>
        /// <returns>A list of the registered Utleveringer</returns>
        [HttpPost("lokasjon/{lokasjonId}/utlevering")]
        [Authorize(Policy.Lokasjon)]
        public async Task<ActionResult<IEnumerable<UtleveringResponse>>> Add
            (
                [FromRoute] int lokasjonId,
                [FromBody] List<RegisterUtleveringRequest> registerUtleveringRequests
            )
        {
            return Created(nameof(Add), await _utleveringService.AddRangeAsync(lokasjonId, registerUtleveringRequests));
        }

        /// <summary>
        /// Update a given Utlevering
        /// </summary>
        /// <param name="lokasjonId">Lokasjon of the Innlevering to be updated</param>
        /// <param name="utleveringId">Id of the Utlevering</param>
        /// <param name="newUtleveringRequest">Updated Utlevering request</param>
        /// <returns>The updated Utlevering</returns>
        [HttpPut("lokasjon/{lokasjonId}/utlevering/{utleveringId}")]
        [Authorize(Policy.Lokasjon)]
        public async Task<ActionResult<UtleveringResponse>> Update
            (
                [FromRoute] int lokasjonId,
                [FromRoute] int utleveringId,
                [FromBody] RegisterUtleveringRequest newUtleveringRequest
            )
        {
            return Ok(await _utleveringService.UpdateAsync(utleveringId, lokasjonId, newUtleveringRequest));
        }

        /// <summary>
        /// Delete an Utlevering
        /// </summary>
        /// <param name="lokasjonId">Lokasjon of the Utlevering to be deleted</param>
        /// <param name="utleveringId">Id of the Utlevering to be deleted</param>
        /// <returns>Status 200 if deleted</returns>
        [HttpDelete("lokasjon/{lokasjonId}/utlevering/{utleveringId}")]
        [Authorize(Policy.Lokasjon)]
        public async Task<ActionResult> Delete
            (
                [FromRoute] int lokasjonId,
                [FromRoute] int utleveringId
            )
        {
            await _utleveringService.DeleteAsync(utleveringId, lokasjonId);
            return Ok();
        }
    }
}
