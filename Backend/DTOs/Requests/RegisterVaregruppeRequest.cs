﻿namespace Backend.DTOs.Requests
{
    public class RegisterVaregruppeRequest
    {
        [NonEmptyString]
        public string Navn { get; set; }
        [PositiveInteger(IsNullable = true)]
        public int? HovedgruppeId { get; set; }
        [PositiveInteger(IsNullable = true)]
        public float? DefaultKassevekt { get; set; }
    }
}
