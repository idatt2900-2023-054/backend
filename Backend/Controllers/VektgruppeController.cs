using Backend.Auth;
using Backend.DTOs.Requests;
using Backend.DTOs.Responses;
using Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers
{
    /// <summary>
    /// Controller class for the endpoints connected to Varegrupper
    /// All endpoints have Admin policy, except the getters which have SuperAdmin and Lokasjon respectively
    /// </summary>
    [ApiController]
    public class VektgruppeController : ControllerBase
    {
        private readonly ILogger<VektgruppeController> _logger;
        private readonly IVektgruppeService _vektgruppeService;

        public VektgruppeController(ILogger<VektgruppeController> logger, IVektgruppeService vektgruppeService)
        {
            _logger = logger;
            _vektgruppeService = vektgruppeService;
        }

        /// <summary>
        /// Get all Vektgrupper
        /// </summary>
        /// <returns>List of Vektgrupper</returns>
        [HttpGet("vektgrupper")]
        [Authorize(Policy.SuperAdmin)]
        public async Task<ActionResult<IEnumerable<VektgruppeResponse>>> GetAll()
        {
            return Ok(await _vektgruppeService.GetAllVektgrupperAsync());
        }

        /// <summary>
        /// Get all Vektgrupper at a Lokasjon
        /// </summary>
        /// <param name="lokasjonId">Lokasjon to search from</param>
        /// <returns>A list of Vektgrupper</returns>
        [HttpGet("lokasjon/{lokasjonId}/vektgrupper")]
        [Authorize(Policy.Lokasjon)]
        public async Task<ActionResult<IEnumerable<VektgruppeResponse>>> GetOnLokasjon([FromRoute] int lokasjonId)
        {
            return Ok(await _vektgruppeService.GetVektgruppeOnLokasjonAsync(lokasjonId));
        }

        /// <summary>
        /// Add a new Vektgruppe at a lokasjon
        /// </summary>
        /// <param name="lokasjonId">Lokasjon for the Vektgruppe</param>
        /// <param name="request">Vektgruppe request</param>
        /// <returns>The new Vektgruppe</returns>
        [HttpPost("lokasjon/{lokasjonId}/vektgruppe")]
        [Authorize(Policy.Admin)]
        [Authorize(Policy.Lokasjon)]
        public async Task<ActionResult<VektgruppeResponse>> Add
            (
                [FromRoute] int lokasjonId,
                [FromBody] RegisterVektgruppeRequest request
            )
        {
            return Created(nameof(Add), await _vektgruppeService.AddAsync(lokasjonId, request));
        }

        /// <summary>
        /// Update a given Vektgruppe
        /// </summary>
        /// <param name="lokasjonId">Lokasjon of the Vektgruppe</param>
        /// <param name="vektgruppeId">Id of the Vektgruppe to be updated</param>
        /// <param name="request">Updated Vektgruppe request</param>
        /// <returns>The updated Vektgruppe</returns>
        [HttpPut("lokasjon/{lokasjonId}/vektgruppe/{vektgruppeId}")]
        [Authorize(Policy.Admin)]
        [Authorize(Policy.Lokasjon)]
        public async Task<ActionResult<VektgruppeResponse>> Update
            (
                [FromRoute] int lokasjonId,
                [FromRoute] int vektgruppeId,
                [FromBody] RegisterVektgruppeRequest request
            )
        {
            return Ok(await _vektgruppeService.UpdateAsync(vektgruppeId, lokasjonId, request));
        }

        /// <summary>
        /// Delete a given Vektgruppe
        /// </summary>
        /// <param name="vektgruppeId">Id of the Vektgruppe to be deleted</param>
        /// <param name="lokasjonId">Lokasjon of the Vektgruppe</param>
        /// <returns>Status 200 OK if deleted</returns>
        [HttpDelete("lokasjon/{lokasjonId}/vektgruppe/{vektgruppeId}")]
        [Authorize(Policy.Admin)]
        [Authorize(Policy.Lokasjon)]
        public async Task<ActionResult> Delete
            (
                [FromRoute] int vektgruppeId,
                [FromRoute] int lokasjonId
            )
        {
            await _vektgruppeService.DeleteAsync(vektgruppeId, lokasjonId);
            return Ok();
        }
    }
}
