﻿using AutoMapper;
using Backend.Auth;
using Backend.DTOs.Requests;
using Backend.DTOs.Responses;
using Backend.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace Backend.Services
{
    /// <summary>
    /// Interface of the Login Service
    /// </summary>
    public interface ILoginService
    {
        public string GenerateJwtToken(LoginResponse loginResponse);
        public Task<LoginResponse> LoginAsync(LoginRequest loginRequest);
    }

    public class LoginService : ILoginService
    {
        private readonly ILogger<LoginService> _logger;
        private readonly DatabaseContext _database;
        private readonly IMapper _mapper;
        private readonly IGoogleValidator _googleValidator;
        private readonly JwtSecurityTokenHandler _jwtSecurityTokenHandler;
        private readonly SigningCredentials _signingCredentials;

        public LoginService
            (
                ILogger<LoginService> logger,
                DatabaseContext database,
                IMapper mapper,
                IGoogleValidator googleValidator,
                JwtSecurityTokenHandler jwtSecurityTokenHandler,
                SigningCredentials signingCredentials
            )
        {
            _logger = logger;
            _database = database;
            _mapper = mapper;
            _googleValidator = googleValidator;
            _jwtSecurityTokenHandler = jwtSecurityTokenHandler;
            _signingCredentials = signingCredentials;
        }

        /// <summary>
        /// Handles login requests
        /// </summary>
        /// <param name="loginRequest">Credentials</param>
        /// <returns>Information about logged in Bruker and when session expires</returns>
        /// <exception cref="ValidationException">Throws validation exception if Bruker is not registered</exception>
        public async Task<LoginResponse> LoginAsync(LoginRequest loginRequest)
        {
            var email = await ValidateLoginRequestAsync(loginRequest);
            var bruker = await _database.Brukere.SingleOrDefaultAsync(x => x.Email == email);
            if (bruker is null)
            {
                throw new ValidationException("Bruker er ikke registrert");
            }

            return new LoginResponse
            {
                Bruker = _mapper.Map<BrukerResponse>(bruker),
                Expires = DateTime.UtcNow.AddDays(1)
            };
        }

        /// <summary>
        /// Generate a new JWT Token
        /// </summary>
        /// <param name="loginResponse">Info to create Token from</param>
        /// <returns>JWT Token</returns>
        public string GenerateJwtToken(LoginResponse loginResponse)
        {
            var bruker = loginResponse.Bruker;
            var claims = new ClaimsIdentity(new Claim[]
            {
                new("brukerId", bruker.Id.ToString()),
                new("email", bruker.Email),
                new("rolle", bruker.Rolle),
                new("lokasjonId", bruker.Lokasjon.Id.ToString())
            });

            return _jwtSecurityTokenHandler.CreateEncodedJwt(
                issuer: "Matsentralen",
                audience: bruker.Email,
                subject: claims,
                notBefore: DateTime.UtcNow,
                expires: loginResponse.Expires,
                issuedAt: DateTime.UtcNow,
                _signingCredentials
            );
        }

        /// <summary>
        /// Validate the login request with Google
        /// </summary>
        /// <param name="loginRequest">Info to be validated</param>
        /// <returns>The email of the request</returns>
        /// <exception cref="ValidationException">Throw validation exception if it fails or if email is not verified</exception>
        private async Task<string> ValidateLoginRequestAsync(LoginRequest loginRequest)
        {
            var payload = await _googleValidator.ValidateAsync(loginRequest.Credential);

            if (payload is null)
            {
                throw new ValidationException("Legitimasjonen er feil");
            }
            if (!payload.EmailVerified)
            {
                throw new ValidationException("Email må være verifisert");
            }

            return payload.Email;
        }
    }
}
