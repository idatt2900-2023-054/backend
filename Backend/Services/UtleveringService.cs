﻿using AutoMapper;
using Backend.DTOs.Requests;
using Backend.DTOs.Responses;
using Backend.Models;
using Microsoft.EntityFrameworkCore;

namespace Backend.Services
{
    /// <summary>
    /// Interface of the Utlevering Service
    /// </summary>
    public interface IUtleveringService
    {
        public Task<IReadOnlyList<UtleveringResponse>> AddRangeAsync(int lokasjonId, List<RegisterUtleveringRequest> registerUtleveringRequests);
        public Task<IReadOnlyList<UtleveringResponse>> GetAllUtleveringerAsync();
        public Task<IReadOnlyList<UtleveringResponse>> GetUtleveringOnLokasjonAsync(int lokasjonId, DateOnly startDate, DateOnly endDate);
        public Task<UtleveringResponse> UpdateAsync(int utleveringId, int lokasjonId, RegisterUtleveringRequest registerUtleveringRequest);
        public Task DeleteAsync(int utleveringId, int lokasjonId);
    }

    public class UtleveringService : IUtleveringService
    {
        private readonly ILogger<UtleveringService> _logger;
        private readonly DatabaseContext _database;
        private readonly IMapper _mapper;
        private readonly IValidator _validator;

        public UtleveringService(ILogger<UtleveringService> logger, DatabaseContext database, IMapper mapper, IValidator validator)
        {
            _logger = logger;
            _database = database;
            _mapper = mapper;
            _validator = validator;
        }

        /// <summary>
        /// Get all Utleveringer
        /// </summary>
        /// <returns>A list of all Utleveringer</returns>
        public async Task<IReadOnlyList<UtleveringResponse>> GetAllUtleveringerAsync()
        {
            _logger.LogInformation("Henter alle utleveringer");
            var utleveringer = await _database.Utleveringer.ToListAsync();
            return _mapper.Map<List<UtleveringResponse>>(utleveringer);
        }

        /// <summary>
        /// Get all Utleveringer at a Lokasjon within a given timespan
        /// Validates the Lokasjon
        /// </summary>
        /// <param name="lokasjonId">Lokasjon to find from</param>
        /// <param name="startDate">Start search date</param>
        /// <param name="endDate">End search date</param>
        /// <returns>A list of Utleveringer which matches the criteria</returns>
        public async Task<IReadOnlyList<UtleveringResponse>> GetUtleveringOnLokasjonAsync(int lokasjonId, DateOnly startDate, DateOnly endDate)
        {
            _logger.LogInformation($"Henter utleveringer fra lokasjon {lokasjonId}");
            await _validator.LokasjonExistsAsync(lokasjonId);

            var start = startDate.ToDateTime(TimeOnly.MinValue);
            var end = endDate.ToDateTime(TimeOnly.MinValue);
            var utleveringer = await _database.Utleveringer
                .Where(x => x.LokasjonId == lokasjonId && x.Dato >= start && x.Dato <= end).OrderByDescending(x => x.Dato)
                .ToListAsync();

            return _mapper.Map<List<UtleveringResponse>>(utleveringer);
        }

        /// <summary>
        /// Add a list of Utleveringer
        /// Validates Lokasjon, Organisasjoner, Nettovekt and Vektgrupper
        /// </summary>
        /// <param name="lokasjonId">Lokasjon to be registered at</param>
        /// <param name="registerUtleveringRequests">List of Utlevering request</param>
        /// <returns>A list of new Utleveringer</returns>
        public async Task<IReadOnlyList<UtleveringResponse>> AddRangeAsync(int lokasjonId, List<RegisterUtleveringRequest> registerUtleveringRequests)
        {
            _logger.LogInformation("Legger til nye utleveringer");
            await _validator.LokasjonExistsAsync(lokasjonId);
            await _validator.OrganisasjonerExistsAsync(registerUtleveringRequests.Select(x => x.OrganisasjonId).ToList());
            await _validator.VektgrupperIsValidAsync(registerUtleveringRequests.Select(x => x.VektgruppeId).ToList(), lokasjonId);
            registerUtleveringRequests.ForEach(x =>
            {
                if (x.Notat is string notat && notat.Trim().Length == 0)
                {
                    x.Notat = null;
                }
            });
            var utleveringer = _mapper.Map<List<Utlevering>>(registerUtleveringRequests);
            utleveringer.Select(x => x.Nettovekt).ToList().ForEach(x => _validator.FloatIsPositive(x, "Nettovekt"));
            utleveringer.ForEach(x => x.LokasjonId = lokasjonId);

            utleveringer = await _database.AddRangeSaveAndGetAsync(_database.Utleveringer, utleveringer);
            _logger.LogInformation("Utleveringer lagt til " + string.Join(",", utleveringer.Select(x => x.Id).ToList()));
            return _mapper.Map<List<UtleveringResponse>>(utleveringer);
        }

        /// <summary>
        /// Update a given Utlevering
        /// Validates Lokasjon, Utlevering, Organisasjon, Nettovekt and Vektgruppe
        /// </summary>
        /// <param name="utleveringId">Id of Utlevering to be updated</param>
        /// <param name="lokasjonId">Lokasjon of the Utlevering</param>
        /// <param name="registerUtleveringRequest">Updated Utlevering request</param>
        /// <returns>The updated Utlevering</returns>
        public async Task<UtleveringResponse> UpdateAsync(int utleveringId, int lokasjonId, RegisterUtleveringRequest registerUtleveringRequest)
        {
            _logger.LogInformation($"Oppdaterer innlevering {utleveringId}");
            await _validator.UtleveringExistsOnLokasjonAsync(utleveringId, lokasjonId);
            await _validator.OrganisasjonExistsAsync(registerUtleveringRequest.OrganisasjonId);
            await _validator.VektgrupperIsValidAsync(new() { registerUtleveringRequest.VektgruppeId }, lokasjonId);
            if (registerUtleveringRequest.Notat is string notat && notat.Trim().Length == 0)
            {
                registerUtleveringRequest.Notat = null;
            }

            var utlevering = _mapper.Map<Utlevering>(registerUtleveringRequest);
            _validator.FloatIsPositive(utlevering.Nettovekt, "Nettovekt");
            utlevering.Id = utleveringId;
            utlevering.LokasjonId = lokasjonId;

            utlevering = await _database.UpdateSaveAndGetAsync(_database.Utleveringer, utlevering);
            _logger.LogInformation("Utlevering oppdatert");
            return _mapper.Map<UtleveringResponse>(utlevering);
        }

        /// <summary>
        /// Delete a given Utlevering
        /// Checks if Utlevering exists and that Lokasjon matches
        /// </summary>
        /// <param name="utleveringId">Id of the Utlevering to be deleted</param>
        /// <param name="lokasjonId">Lokasjon of Utlevering</param>
        /// <returns>Nothing</returns>
        /// <exception cref="ValidationException">Throws Valdiation exception if Utlevering does not exist</exception>
        public async Task DeleteAsync(int utleveringId, int lokasjonId)
        {
            _logger.LogInformation($"Sletter utlevering {utleveringId}");
            var utlevering = await _database.Utleveringer.FirstOrDefaultAsync(x => x.Id == utleveringId);
            if (utlevering == null || utlevering.LokasjonId != lokasjonId)
            {
                throw new ValidationException($"Invalid utlevering {utleveringId}");
            }
            _database.Utleveringer.Remove(utlevering);
            await _database.TrySaveChangesAsync("Kunne ikke slette utleveringen.");
            _logger.LogInformation("Utlevering slettet");
        }
    }
}
