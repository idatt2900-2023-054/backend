﻿namespace Backend.DTOs.Responses
{
    public class VektgruppeResponse
    {
        public int Id { get; set; }
        public string Navn { get; set; }
        public LokasjonResponse Lokasjon { get; set; }
        public float DefaultKassevekt { get; set; }
    }
}
