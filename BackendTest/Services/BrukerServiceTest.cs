﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Logging;
using Moq;
using Moq.EntityFrameworkCore;

namespace BackendTest.Services
{
    /// <summary>
    /// Unit tests for Bruker Service
    /// Test name format: "Method name" _ "What is expected to happen"
    /// Ex: Method_ThrowsException
    /// </summary>
    public class BrukerServiceTest
    {
        private readonly ILogger<BrukerService> _loggerMock;
        private readonly IMapper _mapper;
        private readonly IValidator _validator;

        public BrukerServiceTest()
        {
            _loggerMock = Utils.GetLogger<BrukerService>();
            _mapper = Utils.GetMapper();
            _validator = Utils.GetValidatorMock().Object;
        }

        [Fact]
        public async Task GetAllBrukereAsync_ReturnsBrukerResponses()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Brukere).ReturnsDbSet(
                new List<Bruker> {
                    new Bruker{ Id = 1 },
                    new Bruker{ Id = 2 },
                }
            );

            var brukerService = new BrukerService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var brukere = await brukerService.GetAllBrukereAsync();

            //Assert
            Assert.Equal(2, brukere.Count);
            Assert.True(brukere.Select(x => x.Id == 1).Any());
            Assert.True(brukere.Select(x => x.Id == 2).Any());
        }

        [Fact]
        public async Task GetAllBrukereAsync_ReturnsEmptyWhenEmpty()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.Brukere).ReturnsDbSet(
                new List<Bruker>()
            );

            var brukerService = new BrukerService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var brukere = await brukerService.GetAllBrukereAsync();

            //Assert
            Assert.Empty(brukere);
        }

        [Fact]
        public async Task AddAsync_RegistersAndReturnsBrukerResponse()
        {
            // Arrange
            var request = new RegisterBrukerRequest
            {
                Email = "bruker@matsentralen.no",
                LokasjonId = 1,
                Rolle = "Bruker"
            };
            var bruker = _mapper.Map<Bruker>(request);
            bruker.Lokasjon = new Lokasjon { Id = 1 };

            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.AddSaveAndGetAsync(It.IsAny<DbSet<Bruker>>(), It.IsAny<Bruker>()))
                .Callback(() => bruker.Id = 1)
                .ReturnsAsync(bruker);

            var brukerService = new BrukerService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var response = await brukerService.AddAsync(request);

            //Assert
            Assert.NotNull(response);
            Assert.Equal(bruker.Id, response.Id);
            Assert.Equal(bruker.Email, response.Email);
            Assert.Equal(bruker.Lokasjon.Id, response.Lokasjon.Id);
            Assert.Equal(bruker.Rolle.ToString(), response.Rolle);
        }

        [Fact]
        public async Task UpdateAsync_RegistersAndReturnsBrukerResponse()
        {
            // Arrange
            var request = new RegisterBrukerRequest
            {
                Email = "bruker@matsentralen.no",
                LokasjonId = 2,
                Rolle = "Admin"
            };
            var bruker = _mapper.Map<Bruker>(request);
            bruker.Lokasjon = new Lokasjon { Id = 2 };

            var entityEntryMock = new Mock<EntityEntry<Bruker>>(null);
            entityEntryMock.Setup(x => x.Entity).Returns(bruker);

            var databaseMock = Utils.GetDatabaseContextMock();
            databaseMock.Setup(x => x.UpdateSaveAndGetAsync(It.IsAny<DbSet<Bruker>>(), It.IsAny<Bruker>()))
                .ReturnsAsync(bruker);

            var brukerService = new BrukerService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            var response = await brukerService.UpdateAsync(1, request);

            //Assert
            Assert.NotNull(response);
            Assert.Equal(bruker.Id, response.Id);
            Assert.Equal(bruker.Email, response.Email);
            Assert.Equal(bruker.Lokasjon.Id, response.Lokasjon.Id);
            Assert.Equal(bruker.Rolle.ToString(), response.Rolle);
        }

        [Fact]
        public async Task DeleteAsync_DeletesAndReturnsNothing()
        {
            // Arrange
            var databaseMock = Utils.GetDatabaseContextMock();
            var brukere = new List<Bruker>
            {
                new Bruker{ Id = 1, LokasjonId = 1},
            };
            databaseMock.Setup(x => x.Brukere).ReturnsDbSet(brukere);
            databaseMock.Setup(x => x.Brukere.Remove(It.IsAny<Bruker>())).Callback(brukere.Clear);
            var brukerService = new BrukerService(_loggerMock, databaseMock.Object, _mapper, _validator);

            //Act
            await brukerService.DeleteAsync(1);

            // Assert
            Assert.Empty(brukere);
        }
    }
}
