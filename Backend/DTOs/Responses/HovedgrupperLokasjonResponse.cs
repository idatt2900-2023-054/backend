﻿namespace Backend.DTOs.Responses
{
    public class HovedgrupperLokasjonResponse
    {
        public int Id { get; set; }
        public string Navn { get; set; }
        public int? HovedgruppeId { get; set; }
        public List<HovedgrupperLokasjonResponse> Undergrupper { get; set; }
        public bool Aktiv { get; set; }
        public float DefaultKassevekt { get; set; }
    }
}
