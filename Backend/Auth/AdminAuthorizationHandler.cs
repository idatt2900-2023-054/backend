﻿using Backend.Models;
using Microsoft.AspNetCore.Authorization;

namespace Backend.Auth
{
    public class AdminRequirement : IAuthorizationRequirement { }

    public class AdminAuthorizationHandler : AuthorizationHandler<AdminRequirement>
    {
        private readonly AuthorizationHandlerHelper _authorizationHandlerHelper;

        public AdminAuthorizationHandler(AuthorizationHandlerHelper authorizationHandlerHelper)
        {
            _authorizationHandlerHelper = authorizationHandlerHelper;
        }

        /// <summary>
        /// Check whether the credentials are Admin or Superadmin
        /// </summary>
        /// <param name="authorizationHandlerContext">Context</param>
        /// <param name="requirement">Requirement</param>
        /// <returns>Nothing</returns>
        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext authorizationHandlerContext, AdminRequirement requirement)
        {
            var claimedRolle = _authorizationHandlerHelper.GetClaimedRolle(authorizationHandlerContext);

            if (
                claimedRolle == Rolle.SuperAdmin ||
                claimedRolle == Rolle.Admin
            )
            {
                authorizationHandlerContext.Succeed(requirement);
            }
            else
            {
                authorizationHandlerContext.Fail();
            }
        }
    }
}